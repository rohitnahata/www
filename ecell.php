<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title> Entrepreneurship Cell </title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/imageslider.css">
      <link rel="stylesheet" type="text/css" href="css/mystyle.css">
      <!-- Include scripts -->
      <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
      <script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>
      <!-- Include styles -->
      <link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>
      <!-- Include media queries -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
      <style>
         .hr1 {
         display: block;
         margin: 0.5em auto;
         border: 2px inset #FF4500;
         }
         .head {
         font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
         font-size: 2em;
         }
         .head_comm{
         font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
         font-size: 1.5em;
         }
         .caption
         {
         border: 1px solid #ddd;
         margin-bottom: 20px;
         }
         li.recentEv:before {
         content: "\f061";
         font-family: FontAwesome;
         display: inline-block;
         margin-left: -1.3em;
         width: 1.3em;
         }
         .ecell_img
         {
         max-width: 20%;
         height: auto;
         display: block;
         margin: auto;
         padding-top: 40px;
         padding-bottom: 30px;
         }
         .description{
         padding-top: 20px;
         padding-left: 2em;
         padding-right: 2em;
         padding-bottom: 35px;
         }
         #recent_hr{
         height: 10px;
         border: 0;
         box-shadow: 0 10px 10px -10px #8c8b8b inset;
         }
         #events_hr
         {
         border: 0; 
         height: 1px; 
         background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); 
         }
      </style>
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
   </head>
   <body>
      <?php include 'responsiveHeader/header.html'; ?><br><br>
      <div class="row">
         <div class="container">
            <div class="col-sm-9 caption">
               <h3 class="head" align="center">E - CELL</h3>
               <hr class="hr1">
               <p style="font-size: 1.2em; padding-top: 10px;" align="center"><strong>E-cell is about making job creators and not job seekers</strong></p>
               <img src="img\committee\ecell\ecell_logo.jpg" class="ecell_img"></img>
               <h4 class="head_comm" align="center">Mission</h4>
               <hr class="hr1" width="25%">
               <p class="description">We are here to promote entrepreneurship. To Show you the path to Entrepreneurship 
                  by hosting various events for aspiring entrepreneurs and support them with by providing necessary resources 
                  such as seed funding, mentoring, consultancy and networking.
               </p>
               <h4 class="head_comm" align="center">Overview</h4>
               <hr class="hr1" width="25%">
               <p class="description">S.P.I.T. E-Cell is over here with a mission. A mission to inspire, promote and help people
                  to not only be entrepreneurs but to be high growth entrepreneurs. Such entrepreneurs drive our country's economy
                  and bring a fresh leaf of business to us. And we believe that this has to start at grass root level.
               </p>
               <h4 class="head_comm" align="center">Initiatives</h4>
               <hr class="hr1" width="25%">
               <p class="description">Along with the regular events E-cell has undertook many new Initiatives intended to help 
                  students. Blog Blogs staright from the E-cell core team about the latest news and trends of the entreprenuer world. 
                  Inspire E-cell Initiative inteded to inspire students.
               </p>
               <h4 class="head_comm" align="center">Committee 2016-17</h4>
               <hr class="hr1" width="25%">
               <div style="padding-top: 20px; padding-bottom: 50px;">
                  <img src="img\committee\ecell\ecell.jpg"  style="height: 50%; width: auto;display: block; margin: auto; 
                     box-shadow: 10px 10px 5px #ccc; 
                     -moz-box-shadow: 10px 10px 5px #ccc; -webkit-box-shadow: 10px 10px 5px #ccc; -khtml-box-shadow: 10px 10px 5px #ccc;"></img>
               </div>
            </div>
            <div class="col-sm-3 caption">
               <h4 class="head" align="center">Recent events</h4>
               <hr id="recent_hr" width="100%">
               <ul id="event">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <ul>
            </div>
         </div>
         <!-- /.row -->
      </div>
      <?php include 'includes/footer.php'; ?>
   </body>
</html>