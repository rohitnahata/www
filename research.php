<?php
/*
 * Created by PhpStorm.
 * User: Rohit
 * Date: 06/10/2016
 * Time: 14:08
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Department of Computer Engineering | S.P.I.T. Mumbai</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
 <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
</head>

<body data-offset="40" id="body">
       <?php include 'responsiveHeader/header.html'; ?><br><br>


<div class="container">
    <div class="headerGrad"></div>

        <div class="container">

            <div class="row">


                <div>

                    <div class="thumbnail">
                        <div class="caption">


                            <h3>Faculty Research Publications</h3>
                            <br/>

                            <img src="img/Faculty_Publications_All.jpg" class="img-responsive" />

                        </div>	<!-- /.caption -->
                    </div>	<!-- /.thumbnail -->
                </div>	<!-- /.span9 -->

            </div>	<!-- /.row -->




        </div>	<!-- /container -->
    </div>


    <?php include 'includes/footer.php'; ?>


</div>

</body>
</html>
