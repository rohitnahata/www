<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>    <meta name="description" content="">
    <meta name="author" content="">

    <title> Vision&Mission </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <style>
        body {
            overflow-x: hidden;

        }
        .thumbnail {
        }

        .thumbnails {
        }

        .thumbnails h4 {
            text-align: center;
            color: #333333;
        }

        .thumbnails li {
            margin: 2px;
        }

        .thumbnails a {
            text-decoration: none !important;
        }

        .thumbnail li {
            list-style-type: square;
        }

        .h3{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;

        }
		h4{
			font-weight:bold;
			
		}

        .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }


    </style>
    <script>
        $(document).ready(function () {
            //Examples of how to assign the Colorbox event to elements
            $(".group1").colorbox({rel: 'group1', height: '80%'});


        });
    </script>
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>
<h1 class="h3" align="center">Under Graduate</h1>
<hr class="hr" width="25%">
<div class="container">
    <img src="img/ug.png" style="width:100%;height:1500px;align:center;">
</div>
<div class="container" style="margin-bottom: 50px">
    <ul class="thumbnails" style="list-style: none">
        <a href="pdf/SE3-4-COMP-2013.pdf" target="_blank">
            <li class="span5">

                <div class="thumbnail">
                    <h4>SE CMPN - Sem 3[CBSGS]</h4>
					<hr class="hr" width="30%">


                    <ul>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=6" target="_blank">
                            <li>Applied Mathematics III</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=9" target="_blank">
                            <li>Object Oriented Programming Methodolgy</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=12" target="_blank">
                            <li>Data Structures</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=16" target="_blank">
                            <li>Digital Logic Design and Analysis</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=18" target="_blank">
                            <li>Discrete Structures</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=21" target="_blank">
                            <li>Electronic Circuits and Communication Fundamentals</li>
                        </a>
                    </ul>
                </div>

            </li>

            <li class="span5">
                <div class="thumbnail">
                    <h4>SE CMPN - Sem 4[CBSGS]</h4>
					<hr class="hr" width="30%">


                    <ul>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=24" target="_blank">
                            <li>Applied Mathematics IV</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=27" target="_blank">
                            <li>Analysis of Algorithms</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=31" target="_blank">
                            <li>Computer Organization and Architecture</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=35" target="_blank">
                            <li>Data Base Management systems</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=38" target="_blank">
                            <li>Theoretical Computer Science</li>
                        </a>
                        <a href="pdf/SE3-4-COMP-2013.pdf#page=41" target="_blank">
                            <li>Computer Graphics</li>
                        </a>
                    </ul>
                    <!--                </div>-->
                    <!--            </li>-->
                    <!---->
                    <!--    </ul>   <!-- /.thumbnails -->
                    <!---->
                    <!--    <ul class="thumbnails">-->
                    <!---->
                    <!--        <li class="span5">-->
            <li class="span5">
                <div class="thumbnail">
                    <h4>TE CMPN - Sem 5[CBGS]</h4>
					<hr class="hr" width="30%">

                    <ul>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=13" target="_blank">
                            <li>Operating Systems</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=16" target="_blank">
                            <li>Structured and Object Oriented Analysis and
                                Design
                            </li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=10" target="_blank">
                            <li>Microprocessor</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=24" target="_blank">
                            <li>Web Technologies Laboratory</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=18" target="_blank">
                            <li>Computer network</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=21" target="_blank">
                            <li>Business Communication and Ethics*</li>
                        </a>
                    </ul>
                </div>
            </li>

            <li class="span5">
                <div class="thumbnail">
                    <h4>TE CMPN(CBGS) - Sem 6[CBGS]</h4>
					<hr class="hr" width="30%">


                    <ul>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=26" target="_blank">
                            <li>System Programming and Compiler
                                Constructionk
                            </li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=30" target="_blank">
                            <li>Software Engineering</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=33" target="_blank">
                            <li>Distributed Databases</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=36" target="_blank">
                            <li>Mobile Communication and Computing</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=39" target="_blank">
                            <li>Elective-I</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=42" target="_blank">
                            <li>Network Programming Laboratory</li>
                        </a>
                    </ul>
                </div>
            </li>

            <li class="span5">
                <div class="thumbnail">
                    <h4>BE CMPN - Sem 7[CBGS]</h4>
					<hr class="hr" width="30%">


                    <ul>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=49" target="_blank">
                            <li>Digital Signal Processing</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=56" target="_blank">
                            <li>Cryptography and System Security</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=59" target="_blank">
                            <li>Artificial Intelligence</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=79" target="_blank">
                            <li>Network Threats and Attacks Laboratory</li>
                        </a>

                        <a href="pdf/TE5-6-COMP-2014.pdf#page=62" target="_blank">
                            <li>Elective-II</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=113" target="_blank">
                            <li>Project-I</li>
                        </a>
                    </ul>
                </div>
            </li>

            <li class="span5">
                <div class="thumbnail">
                    <h4>BE CMPN(CBGS) - Sem 8[CBGS]</h4>
					<hr class="hr" width="30%">


                    <ul>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=82" target="_blank">
                            <li>Data Warehouse and Mining</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=86" target="_blank">
                            <li>Human Machine Interaction</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=90" target="_blank">
                            <li>Parallel and distributed Systems</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=93" target="_blank">
                            <li>Elective-III</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=113" target="_blank">
                            <li>Project-II</li>
                        </a>
                        <a href="pdf/TE5-6-COMP-2014.pdf#page=109" target="_blank">
                            <li>Cloud Computing Laboratory</li>
                        </a>
                    </ul>
                </div>
            </li>

    </ul>   <!-- /.thumbnails -->


</div>  <!-- /.span12 -->

<?php include 'includes/footer.php'; ?>

</body>
</html>