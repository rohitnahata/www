<?php

require_once './includes/session.php';
require_once './includes/database.php';


$sql = "select s.title,s.content,s.year,t.name,s.id FROM student_achievements  as s inner join team_members  as t on s.id=t.students_achievement_id ";
$achievement_result = $database->query($sql);
$row = $database->fetch_array($achievement_result);

$sql = "SELECT * FROM team_members ORDER BY students_achievement_id";
$mem_result = $database->query($sql);
$row2 = $database->fetch_array($mem_result);

?>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Achievements 2014-15 </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

    <style>
        .space-top-lg {
            padding-top: 30px;
        }
        .space-top-sm {padding-top: 20px;
            padding-left: 100px;
        }

        .space-top-md {
            padding-top: 30px;
            padding-left: 150px;
            padding-right: 50px;
        }
        .space-bottom-sm {padding-bottom: 20px;
            padding-left: 100px;
        }
        .space-bottom-md {padding-bottom: 30px
            padding-left: 100px;
        }
        .proj {
            background-color: #e5e5e5;
            border-radius: 10px;
            padding-left: 20px;

        }
        .projhead {
            font-family: Georgia, Times, "Times New Roman", serif;
            color: #000;
            text-align: center;
        }
        a:hover {
            color: #000;
        }
        .h3{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;

        }

        .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }
		.head {
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
            font-size: 2em;
        }
		


    </style>
</head>
<body>
<?php include 'responsiveHeader/header.html'; ?><br><br>
<div class="container">
    <h3 class="head" align="center">Achievements 2014-15</h3>
	<hr class="hr">

    

<?php
$i = 0;
do {
    if ($row['year']==2014) {
        $temp_id[$i] = $row['id'];
        $temp_name[$i] = $row['name'];
        $i++;
    }
} while ($row = $achievement_result->fetch_assoc());

$temp_id[$i] = $temp_id[$i - 1] + 1;
$j = 0;

$sql = "select s.title,s.content,s.year,t.name,s.id FROM student_achievements  as s inner join team_members  as t on s.id=t.students_achievement_id ";
$achievement_result = $database->query($sql);
$row = $database->fetch_array($achievement_result);

$sql = "SELECT title,content,year FROM student_achievements ";
$result = $database->query($sql);
$row2 = $database->fetch_array($result);


$tempr = $row['id'];

do {
    if ($row2['year'] == 2014) {
        $incr = $tempr + 1;
        ?>
        <div class="container space-top-lg">

            <div class="proj space-top-sm space-bottom-sm">
                <h3 class="projhead"><?php echo "{$row2['title']}" ?></h3>
                <!-- Name of coding competition -->
                <h4>Team:</h4>
                <ul>
                    <?php
                    while ($tempr < $incr) {
                        ?>
                        <li><?php echo "{$temp_name[$j]}" ?></li>
                        <?php
                        if ($j == $i - 1) {
                            break;
                        }
                        $tempr = $temp_id[++$j];
                    }
                    ?>
                </ul>
                <p style="padding-right: 10px"> <?php echo "{$row2['content']}" ?> </p><!--Description-->
            </div>

        </div>
        <?php
    }
}while ($row2 = $result->fetch_assoc());
?>
<div style="margin: 100px"></div>
<div>
<?php include 'includes/footer.php'; ?>

</body>
</html>