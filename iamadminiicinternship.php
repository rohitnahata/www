<?php
require_once './includes/database.php';
require_once './includes/session.php';
require_once './includes/functions.php';

//print_r($_POST);


if(isset($_SESSION['aid'])){

    if (isset($_POST['jobtitle']) && isset($_POST['studentname']) && isset($_POST['year']) && isset($_POST['companydesc']) && isset($_POST['company_id'])) {
        if (!empty($_POST['jobtitle']) && !empty($_POST['studentname'])) {
            $sql = "INSERT INTO internship(job_title,company_id,description) VALUES('{$database->escape_value($_POST['jobtitle'])}',".$_POST['company_id'].",'{$database->escape_value($_POST['companydesc'])}')";
            $result=$database->query($sql);
            $length = sizeof($_POST['studentname']);
            $sql = "SELECT max(id) FROM internship";
            $result = $database->query($sql);
            $result = $database->fetch_array($result);
            $internship_id = $result['max(id)'];
            //echo "Hellohellohello{$internship_id}";
            for ($i = 0; $i < $length; $i++) {
                if (!empty($_POST['studentname'][$i])) {
                    $sql = "INSERT INTO iic_students_selected(name,year,internship_id) VALUES('{$database->escape_value($_POST['studentname'][$i])}','{$database->escape_value($_POST['year'][$i])}','{$database->escape_value($internship_id)}')";
                    $result = $database->query($sql);

                }
            }
        } else {
            echo "Enter all fieds";
        }
    }
}  else {
    redirect_to('index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <title>Department of Computer Engineering | Faculty</title>
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">
    <style>
        .addMore {
            height: 30px;
            padding: 5px;
            width: auto !important;
            float: right !important;
            margin-top: -40px !important;
        }
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include('includes/control_header.php'); ?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <?php include('includes/admin_sidebar.php'); ?>
        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addFaculty">
                <!-- for adding a new faculty -->
                <div class="error">
                    <?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?>
                </div>
                <!-- print error here -->
                <!-- print error here -->
                <h3>Add IIC Internship</h3>
                <ul class="nav">
                    <form method="POST" action="iamadminiicinternship.php">
                        <input type="text" placeholder="Job Title" class="form-control" name="jobtitle">
                        <div id="addIntern">
                            <input type="text" placeholder="Student Name" class="form-control" name="studentname[]"
                                   style="width:250px;">
                            <select class="form-control" style="width:250px;" name="year">
                                <?php for ($i = 2008; $i
                                < 2020; $i++) { ?>
                                    <option>
                                        <?php echo "{$i}"; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="btn btn-md btn-primary btn-block addMore" onclick="internship_addmore()">Add More
                            Students
                        </div>
                        <textarea placeholder="Internship Description" class="form-control"
                                  name="companydesc"></textarea>
                        <br>
                        <select class="form-control" name="company_id">
                            <!-- for company names -->
                            <?php
                            $sql = "SELECT * FROM company";
                            $result = $database->query($sql);
                            while ($row = $result->fetch_array()) {


                                ?>
                                <option value="
																										<?php echo "{$row['id']}"; ?>">
                                    <?php echo "{$row['name']}"; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type="submit" value="Add" class="btn btn-sm btn-primary btn-block faculty-account"/>
                    </form>
                </ul>
            </div>
            <!--/jumbotron for adding a new faculty -->
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <hr>
    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>
</div>
<!--/.container-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
<?php
$database->close_connection();
?>