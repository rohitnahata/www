<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="">
      <meta name="author" content="">
      <title> About </title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
	  <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	  
      <link
         href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
         rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
      <style>
         .hr {
         display: block;
         margin: 0.5em auto;
         border: 2px inset #FF4500;
         }
      </style>
	  
   </head>
   <body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>

      <div class="container">
         <div class="panel panel-default">
            <div class="panel-body">
               <p>
               <h2 style ="text-align:center;">About The Department</h2>
               <hr class="hr" width="45%"><br><br>
               The Department of Computer Engineering was established in 1995. It has excellent infrastructure
               and enthusiastic young faculty. The department has conducted several value-added courses in the
               area of Multimedia systems,Neural Network and Fuzzy Logic,Programming languages, Application
               Development Certificate Program using Microsoft Dot Net and Service Oriented Architecture.
               Faculty actively participates in research and development activities by publishing papers in
               various National and International Conferences and Journals. The Department has established a
               good Industry interaction. For betterment of teaching and learning process,department
               coordinated with Wipro Technologies Ltd. and has conducted a program 'Mission10X and Post
               mission10X'
               </p>
               <p>
                  The Department has been conducting a Post-Graduate (M.E.) Program in Computer Engineering since
                  2011 and Ph.D. in Technology Program in Computer Engineering from the year 2012-13.
               </p>
               <p>
                  The Department receives a Research Grant from Department of Science and Technology(DST),
                  Government of India,New Delhi and Mumbai University for Innovative Projects and Minor Research
                  Grant Projects respectively.
               </p>
               <p>
                  The Department has an exclusive library for students with 400+ books and advanced reference
                  books. It also has a TV setup for video courses where advanced recorded courses are available.
                  Computer based training (CBT) CDs from IITs are available in the library.
               </p>
               <p>
                  The Department has started Industry Institute Interaction Cell(IIC-cell) for providing
                  internship from the year 2013-14.The Department has also started Consultancy Work in the area of
                  application development using web technologies and android O.S.
               </p>
               <br>
               <p>
                  <br>
               </p>
			   <a href="face.php">
               <div class="col-xs-6 col-md-3 ">
                  <img class="img-responsive" src="img/committee/face/FACE_logo.jpg"/>
               </div>
			   </a>
			   <a href="csi.php">
               <div class="col-xs-6 col-md-3">
                  <img class="img-responsive" src="img/committee/csi/CSI_logo.jpg"/>
               </div>
			   </a>
			   <a href="pi.php">
               <div class="col-xs-6 col-md-3">
                  <img class="img-responsive" src="img/committee/pi/PICLUB_logo.jpg"/>
               </div>
			   </a>
			   <a href="ecell.php">
               <div class="col-xs-6 col-md-3">
                  <img class="img-responsive" src="img/committee/ecell/ecell_logo.jpg"/>
               </div>
			   </a>
            </div>
         </div>
      </div>
      <?php include 'includes/footer.php'; ?>
   </body>
</html>