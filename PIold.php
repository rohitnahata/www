<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/includes/functions.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/includes/database.php';

?>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> PI CLUB </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    
   <style>

      .imgContainer{
    
    margin-right: 50px;
}
   h5
   {
    color:#22316c;

       }
   .caption
   {
    margin-left:160px;
    margin-right: 280px;
    border: 1px solid #ddd;
    margin-top: 20px;
   }
   
   .span9   {
            width:auto !important;
            text-align:justify;
        }
        .thumbnail  {
            line-height:22px !important;    
        }

#events
 {
color:#000000;
    font-family:"Times New Roman";
width: 20%;
    height: 100%;
    position:absolute; 
    overflow: auto;
padding:0;
margin:0;
margin-left: 1100px;
top: 210px;
    font-family:"Times New Roman";
}  

#events h2{
    font-family:"Times New Roman";
} 

#events ul li a{
text-decoration :none;
color:#000000;
}

#events ul li a :hover{
color:#000000;
}

   </style>

     <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
  </head>
  <body>
<div class="containerOut">

  
     <?php include 'includes/header.php'; ?> 
        
        <div class="row">
        
                
            <div class="span9">

                <div class="thumbnail">
<div id="events">
<h2> Recent Events</h2>
    <ul>
        <?php
        $sql="SELECT * FROM seminar_workshop where committee_id = '2'  ORDER BY date DESC LIMIT 10";
        $result=$database->query($sql);
        while($row=$database->fetch_array($result)){
            ?>
            <li>
                <?php echo $row['content'];?>   <!-- Title of the project -->
            </li>
        <?php }?>
    </ul>
</div>

                    <div class="caption">
                    
            
                    <h3>PI CLUB</h3>
                    <h4>Incharge: Prof. Nataasha Raul</h4>
                    <div align="center" >
                <img u="image" src="img/committee/PICLUB_logo.jpg" height="220px"/>
                
         
                </div>
                <br>
                    <p>
                    PI club is an activity club for all tech activities @ SPIT !! Come , be a part and enjoy activities at their best !! 
                    </p>
                    <br>
                    <div id ="PI_committee" align = "center">
                    <p>
                        <h4>Committee 2016-2017</h4>
                    </p>
                    <img u="image" src="img/committee/PICLUB_logo.jpg" height="300px"/>
                    </div>
                    
                    </div>  <!-- /.caption -->
                </div>  <!-- /.thumbnail -->
            </div>  <!-- /.span9 -->
            
        </div>  <!-- /.row -->     
    
</div> 

<?php include 'includes/footer.php'; ?> 
</body>
      </html>