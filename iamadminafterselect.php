
<?php
//echo print_r($_POST);
require_once 'includes/database.php';
require_once 'includes/session.php';
require_once 'includes/functions.php';
require_once 'includes/uploading_image.php';


if (isset($_SESSION['aid'])) {

    $sql = "SELECT id from faculty WHERE fname ='{$database->escape_value($_POST['fullname'])}'";

    $f_id = $database->query($sql);
    $f_id = $database->fetch_array($f_id);
    //echo $sql;
    $id = $f_id['id'];   //id is the id of the faculty!

    
    //updates

    if(isset($_POST['position']) && isset($_POST['fullname']) && isset($_POST['qualification']) && isset($_POST['email']) &&  isset($_POST['tel']) && isset($_POST['ext']) && isset($_POST['room'])){
        if (!empty($_POST['position']) && !empty($_POST['fullname']) && !empty($_POST['qualification']) && !empty($_POST['email']) && !empty($_POST['tel']) && !empty($_POST['ext']) && !empty($_POST['room'])) {
                $id = $_POST["id"];

            if (isset($_SESSION['ssz']) && $_SESSION['ssz'] != "" && isset($_POST['image'])) {
                $sql = "UPDATE faculty SET fname='{$database->escape_value($_POST['fullname'])}',qualification='{$database->escape_value($_POST['qualification'])}',email='{$database->escape_value($_POST['email'])}',extension='{$database->escape_value($_POST['ext'])}',
                room_number='{$database->escape_value($_POST['room'])}', telephone='{$database->escape_value($_POST['tel'])}',image_path='{$database->escape_value($_SESSION['ssz'])}',position='{$database->escape_value($_POST['position'])}' where id={$id}";
               // echo "ID: ".$_POST["id"]."\n".$sql;
            } else if (isset($_SESSION['ssz'])) {
                $sql = "UPDATE faculty SET fname='{$database->escape_value($_POST['fullname'])}',qualification='{$database->escape_value($_POST['qualification'])}',email='{$database->escape_value($_POST['email'])}',extension='{$database->escape_value($_POST['ext'])}',
                room_number='{$database->escape_value($_POST['room'])}', telephone='{$database->escape_value($_POST['tel'])}',position='{$database->escape_value($_POST['position'])}' where id={$id}";
                //echo "ID: ".$_POST["id"]."\n".$sql;

            }
            //echo "{$sql}";
            $database->query($sql);

            //$sql = "SELECT id from faculty WHERE fname ='{$database->escape_value($_POST['fullname'])}' AND email = '{$database->escape_value($_POST['email'])}' ";
            //$f_id = $database->query($sql);
            //$f_id = $database->fetch_array($f_id);
            //echo $sql;
            //$id = $f_id['id'];

            $message = "Updated";


            if (isset($_POST['aoi']) && !empty($_POST['aoi'][0])) {

                foreach ($_POST['aoi'] as $value) {
                    if (!empty($value)) {
                        $sql = "insert into area_of_interest(description,faculty_id) values('{$value}','{$id}')";
                        //echo "Hello";
                        echo $sql;
                        $result = $database->query($sql);
                        $message = "Updated";
                    }
                }
            }


            //This is for the professional part
            if (isset($_POST['pa_title']) && !empty($_POST['pa_title'][0])) {
                $length = sizeof($_POST['pa_title']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['pa_title'][$i]))
                        $sql = "insert into professional_affiliation(description,faculty_id) values('{$database->escape_value($_POST['pa_title'][$i])}','{$id}')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message = "Updated";
            }


            if (isset($_POST['nj_title']) && !empty($_POST['nj_title'][0])) {
                $length = sizeof($_POST['nj_title']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['nj_title'][$i]))
                        $sql = "insert into research_publication(title,faculty_id,type) values('{$database->escape_value($_POST['nj_title'][$i])}','{$id}','N')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message = "Updated";


            }
            if (isset($_POST['ij_title']) && !empty($_POST['ij_title'][0])) {
                $length = sizeof($_POST['ij_title']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['ij_title'][$i]))
                        $sql = "insert into research_publication(title,faculty_id,type) values('{$database->escape_value($_POST['ij_title'][$i])}','{$id}','I')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message = "Updated";
            }
            if (isset($_POST['nc_title']) && !empty($_POST['nc_title'][0])) {
                $length = sizeof($_POST['nc_title']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['nc_title'][$i]))
                        $sql = "insert into conference(title,type,faculty_id) values('{$database->escape_value($_POST['nc_title'][$i])}','N','{$id}')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message = "Updated";
            }
            if (isset($_POST['ic_title']) && !empty($_POST['ic_title'][0])) {
                $length = sizeof($_POST['ic_title']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['ic_title'][$i]))
                        $sql = "insert into conference(title,type,faculty_id) values('{$database->escape_value($_POST['ic_title'][$i])}','I','{$id}')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message = "Updated";
            }
            if (isset($_POST['bp_title']) && !empty($_POST['bp_title'][0])) {
                $length = sizeof($_POST['bp']);

                for ($i = 0; $i < $length; $i++) {
                    if (!empty($_POST['bp_title'][$i]))
                        $sql = "insert into books_published(title,faculty_id) values('{$database->escape_value($_POST['bp_title'][$i])}','{$id}')";
                    $result = $database->query($sql);
                }

                //$database->query($sql);
                $message="Updated";
            }
            /* if(isset($_POST['course'])){
                 foreach ($_POST['bp'] as $value) {
                     $sql="insert into books_published values('{$database->escape_value($value)}',{$_SESSION['aid']})";
                     $database->query($sql);
                 }
             }*/
            if (isset($_POST['other']) && !empty($_POST['other'][0])) {
                foreach ($_POST['other'] as $value) {
                    if (!empty($value)) {
                        $sql = "insert into others values('{$id}','{$database->escape_value($value)}')";
                        $database->query($sql);
                        $message = "Updated";
                    }
                }
            }

        } else $message = "All Personal Information required!";
    }

} else {
    redirect_to('index.php');
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Department of Computer Engineering | Faculty</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include('includes/control_header.php'); ?>

<div class="container">


    <div class="row row-offcanvas row-offcanvas-right">

        <?php include('includes/admin_sidebar.php'); ?>

        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addFaculty">    <!-- for adding a new faculty -->

                <div class="error"><?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?></div>    <!-- print error here -->
                <h3>Personal Information</h3>
                <!--  <img src="<?php //echo $_SESSION['image'];?>">-->
                <?php

                $query = "Select * from faculty where id = {$id}";
                $run = $database->query($query);
                $result = $database->fetch_array($run);
                //print_r($result);
                // echo "{$result['fname']}";
                ?>
                <ul class="nav">
                    <form action="iamadminafterselect.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $result['id'];?>" >
                        <li>
                            <input type="file" name="image" class="form-control" style="height:auto"
                                   placeholder="Picture">
                        </li>

                        <li>
                            <input name="fullname" type="text" class="form-control" placeholder="Full Name"
                                   value="<?php echo "{$result['fname']}" ?>">

                        </li>

                        <li>
                            <input type="text" name="position" class="form-control" placeholder="Postion in Department"
                                   value="<?php echo "{$result['position']}" ?> ">
                        </li>

                        <li>
                            <input type="text" name="qualification" class="form-control" placeholder="Qualification"
                                   value="<?php echo "{$result['qualification']}"; ?>">
                        </li>

                        <li>
                            <input type="text" name="email" class="form-control" placeholder="Email"
                                   value="<?php echo "{$result['email']}" ?>">
                        </li>

                        <li>
                            <input type="text" name="tel" class="form-control" placeholder="Telephone"
                                   style="width:300px" <?php echo 'value=' . $result['telephone'] ?> >
                            <input type="text" name="ext" class="form-control" placeholder="Ext"
                                   style="width:100px" <?php echo 'value=' . $result['extension'] ?> >
                            <input type="text" name="room" class="form-control" placeholder="Room Num"
                                   style="width:100px" <?php echo 'value=' . $result['room_number'] ?> >
                        </li>
                        <!-- <button class="btn btn-sm btn-primary btn-block faculty-account" style="float:left">Update</button> -->


                        <!-- added by ssz-->
                        <h3>Area Of Interest</h3>
                        <?php

                        $query = "select * from area_of_interest where faculty_id = {$id}";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){

                            ?>
                            <li>
                                <div><input type="text" class="form-control" placeholder="Area of Interest"
                                            value="<?php echo "{$result['description']}" ?>" disabled="true"/></div>
                            </li>

                        <?php } ?>


                        <li class="pInfo">
                            <div id="areaofinterest">
                                <input type="text" class="form-control" placeholder="Area of Interest" name="aoi[]"/>
                            </div>

                            <div class="btn btn-sm btn-primary btn-block addMore" onclick="areaofinterest_addmore()">Add
                                More
                            </div>
                        </li>
                        <hr>


                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="professionalaff">
                                <!--  <input type="text" class="form-control" placeholder="National Journal"> -->
                                <h3>Professional Affiliations</h3>
                                <?php
                                $sql = "Select * from professional_affiliation";
                                $result = $database->query($sql);
                                while ($row = $database->fetch_array($result)) {


                                    ?>
                                    <input type="text" class="form-control" placeholder="Description"
                                           value="<?php echo "{$row['description']}"; ?>"/>

                                <?php } ?>

                                <input type="text" class="form-control" placeholder="Description" name="pa_title[]"/>
                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore" name="pa[]"
                                 onclick="professional_affiliation_addmore()">Add More
                            </div>
                        </li>
                        <hr>


                        <h3>National Jounal</h3>
                        <?php

                        $query = "select * from research_publication where faculty_id = {$id} AND type='N'";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){
                            ?>
                            <li><div><input type="text" class="form-control" placeholder="nationaljournal" value=" <?php "{$result['title']}" ?>"   disabled = "true" /></div></li>

                        <?php }?>


                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="nationaljournal">
                                <!-- <input type="text" class="form-control" placeholder="National Journal"> -->


                                <input type="text" class="form-control" placeholder="Description" name="nj_title[]"/>

                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore" name="nj[]"
                                 onclick="nationaljournal_addmore()">Add More
                            </div>
                        </li>
                        <hr>

                        <h3>International Jounal</h3>
                        <?php

                        $query = "select * from research_publication where faculty_id = {$id} AND type='I'";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){
                            ?>
                            <li><div><input type="text" class="form-control" placeholder="internationaljournal" value="<?php echo"{$result['title']}"?>"  disabled = "true" /></div></li>

                        <?php }

                        ?>
                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="internationaljournal">
                                <!-- <input type="text" class="form-control" placeholder="Internation Journal" name="ij[]"> -->


                                <input type="text" class="form-control" placeholder="Description" name="ij_title[]"/>

                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore"
                                 onclick="internationaljournal_addmore()">Add More
                            </div>
                        </li>
                        <hr>

                        <h3>National Conference</h3>
                        <?php

                        $query = "select * from conference where faculty_id = {$id} AND type='N'";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){
                            ?>
                            <li><div><input type="text" class="form-control" placeholder="nationalconference" value="<?php echo"{$result['title']}"?>" disabled = "true" /></div></li>

                        <?php }

                        ?>

                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="nationalconference">
                                <!-- <input type="text" class="form-control" placeholder="National Conference" name="nc[]"> -->


                                <input type="text" class="form-control" placeholder="Description" name="nc_title[]"/>

                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore"
                                 onclick="nationalconference_addmore()">Add More
                            </div>
                        </li>
                        <hr>

                        <h3>International Conference</h3>
                        <?php

                        $query = "select * from conference where faculty_id = {$id} AND type='I'";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){
                            ?>
                            <li><div><input type="text" class="form-control" placeholder="internationaljournal" value="<?php echo"{$result['title']}"?>"  disabled = "true" /></div></li>

                        <?php }

                        ?>
                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="internationalconference">
                                <!-- <input type="text" class="form-control" placeholder="Internation Conference" name="ic[]"> -->


                                <input type="text" class="form-control" placeholder="Description" name="ic_title[]"/>

                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore"
                                 onclick="internationalconference_addmore()">Add More
                            </div>
                        </li>
                        <hr>

                        <h3>Book Published</h3>
                        <?php

                        $query = "select * from books_published where faculty_id = {$id}";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){
                            ?>
                            <li>
                                <div><input type="text" class="form-control" placeholder="Books Published"
                                            value="<?php echo "{$result['title']}" ?>" disabled="true"/></div>
                            </li>

                        <?php }

                        ?>
                        <li class="pInfo">
                            <!-- added by ssz-->
                            <div id="bookspublished">
                                <!-- <input type="text" class="form-control" placeholder="Book Published" name="bp[]"> -->


                                <input type="text" class="form-control" placeholder="Description" name="bp_title[]"/>

                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore" onclick="bookspublished_addmore()">Add
                                More
                            </div>
                        </li>
                        <hr>


                        <?php

                        $query = "select * from research_publication where faculty_id = {$id} AND type='O'";
                        $run = $database->query($query);

                        while( $result = $database->fetch_array($run) ){

                            echo '<li><div><input type="text" class="form-control" placeholder="Others" value="'.$research_publicationt['title'].'" disabled = "true" /></div></li>';

                        }

                        ?>
                        <li class="pInfo">
                            <?php

                            $query = "select * from others where faculty_id = {$id}";
                            $run = $database->query($query);

                            while ($result = $database->fetch_array($run)) {

                                echo '<li><div><input type="text" class="form-control" placeholder="Others" value="' . $result['description'] . '" disabled = "true" /></div></li>';

                            }

                            ?>
                            <!-- added by ssz-->
                            <div id="others">

                                <input type="text" class="form-control" placeholder="Others" name="other[]">
                            </div>
                            <div class="btn btn-sm btn-primary btn-block addMore" onclick="others_addmore()">Add More
                            </div>
                        </li>
                        <hr>

                        <input type="submit" name="ADD" class="btn btn-sm btn-primary btn-block"
                               value="Update Faculty"/>
                    </form>
                </ul>
            </div>    <!--/jumbotron for adding a new faculty -->

        </div><!--/span-->

    </div><!--/row-->

    <hr>

    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
<?php
$database->close_connection();
?>