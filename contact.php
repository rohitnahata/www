<html>

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="">
      <meta name="author" content="">
      <title> About </title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
	  <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	  
      <link
         href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
         rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
      <style>
         .hr {
         display: block;
         margin: 0.5em auto;
         border: 2px inset #FF4500;
         }
      </style>
	  
   </head>
<body>


       <?php include 'responsiveHeader/header.html'; ?><br><br>
	   
	   <div class="container">
         <div class="panel panel-default">
            <div class="panel-body">
               <div class="h3"><p align="center">Department Contact Info</p></div>
                    <hr class="hr" WIDTH="50%">
                    <br/>
                    <br/>
                    <div class="h4"><h4><b>Dr. Dhananjay Kalbande</b></h4></div>
                    <p style="padding-left: 20px">
                        Professor and Head<br>
                        Department of Computer Enginnering<br><br>

                        Room No. 604,<br>
                        6th Floor, Department of Computer Engineering,<br>
                        Sardar Patel Institute of Technology,<br>
                        Bhavan’s Campus, Munshi Nagar,<br>
                        Andheri (West), Mumbai 400 058<br><br>

                        <strong>Phone:</strong> 022 – 26707440, 26287250 Ext: 366<br><br>
                        <strong>Email:</strong><a href="mailto:drkalbande@spit.ac.in"> drkalbande@spit.ac.in</a><br>
                    <div class="h3"><h3 align="center">Locate Us</h3></div>
                    <hr class="hr" WIDTH="150px" COLOR="#FF0000">
                    <br>
                    <br>
                    <div style="text-align: center;">
					<div class="col-md-12">
      <iframe src="https://maps.google.co.in/maps?ie=UTF8&amp;q=Sardar+Patel+Institute+of+Technology&amp;fb=1&amp;gl=in&amp;hq=sardar+patel+institute+of+technology&amp;hnear=0x3be7c6306644edc1:0x5da4ed8f8d648c69,Mumbai,+Maharashtra&amp;cid=0,0,12945796962899063765&amp;t=m&amp;ll=19.125869,72.836115&amp;spn=0.007096,0.00912&amp;z=16&amp;iwloc=A&amp;output=embed" width="100%" height="450" frameborder="0" style="border:0"></iframe>
          
          
      </div>
					<!--
						<div class="col-md-6">
                        <iframe src="https://maps.google.co.in/maps?ie=UTF8&amp;q=Sardar+Patel+Institute+of+Technology&amp;fb=1&amp;gl=in&amp;hq=sardar+patel+institute+of+technology&amp;hnear=0x3be7c6306644edc1:0x5da4ed8f8d648c69,Mumbai,+Maharashtra&amp;cid=0,0,12945796962899063765&amp;t=m&amp;ll=19.125869,72.836115&amp;spn=0.007096,0.00912&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe>
						</div>-->
                       
                    </div>
            </div>
         </div>
      </div>

<?php include 'includes/footer.php'; ?>
</body>
</html>
