<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Department of Computer Engineering | S.P.I.T. Mumbai</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="css/mystyle.css">

    
    <style>


   
   .thumbnail  {
       line-height: 22px !important;
       border: 0;
       margin-bottom: 100px;
        }
   .h3{
       color: black;
       font-family: Georgia, Times, "Times New Roman", serif;
       font-size:250%;

   }

   .hr {
       display: block;
       margin: 0.5em auto;
       border: 2px inset #FF4500;
   }
    .tag_h4
    {
        color: black;
        font-family: optima, serif;
        font-size:250%;
    }

   p
    {
        font-family: optima, serif;
    }
        #body
        {
    background-color: #ffffff;
    }
    .fContainer img { box-shadow:0 0 10px 0 #222; }

    .fContainer {
    height:auto;
    border:1px solid #999;
    border-radius:5px;
    margin:10px;

}

   .thumbnail	{
			line-height:22px !important;	
		}
		
		.hr {
         display: block;
         margin: 0.5em auto;
         border: 2px inset #FF4500;
         }
    </style>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	  <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	  
      <link
         href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
         rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
    <script src="js/jquery.colorbox.js"></script>
	<script>
        $(document).ready(function(){
            //Examples of how to assign the Colorbox event to elements
            $(".group1").colorbox({rel:'group1',height:'80%'});
            
            //Example of preserving a JavaScript event for inline calls.
            /*$("#click").click(function(){ 
                $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
                return false;
            });*/
        });
    </script>
</head>

<body data-offset="40" id="body">
    	<?php include 'responsiveHeader/header.html'; ?><br><br>
				<h2 style ="text-align:center;">Facilities</h2>
               <hr class="hr" width="45%"><br><br>
<div class="container">
<div class="panel panel-default">
    <div class="panel-body">
	
	<div class="row">
                <div class="col-md-6">
                    <div class="fContainer">
                    
                    	<div class="tag_h4"><h4 align="center"><b>Project Lab</b></h4></div>
						<hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 602</h5>
                    	<p align="center"><a class="group1" href="img/facilities/ProjectLab.jpg" title="Room No 602 - Project Lab"><img src="img/facilities/thumbnails/ProjectLab.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Anand Godbole</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Computer network</li>
                            <li>System Security</li>
                            <li>Soft Computing</li>
                            <li>Mobile Computing</li>
                        </ul>
                        </div>
                        
                        </div><!-- /.fContainer -->
                  </div>	
                <div class="col-md-6">
                    <div class="fContainer">
                        
                    	<div class="tag_h4"><h4 align="center"><b>Networking Lab</b></h4></div>
						<hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 602</h5>
                    	<p align="center"><a class="group1" href="img/facilities/NetworkingLab.jpg" title="Room No 602 - Networking Lab"><img src="img/facilities/thumbnails/NetworkingLab.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Sudhir Dhage</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Theory of computing science</li>
                            <li>Robotics &amp; artificial intelligence</li>
                            <li>System security</li>
                            <li>Mobile Computing</li>
                        </ul>
                        </div>
                        </div><!-- /.fContainer -->
                </div>	
                            </div>
							
							
							
							
							 <div class="row">
                <div class="col-md-6">
                    <div class="fContainer">
                        
                        <div class="tag_h4" ><h4 align="center"><b>Algorithm Lab</b></h4></div>
						<hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 603</h5>
                    	<p align="center"><a class="group1" href="img/facilities/Algorithm.jpg" title="Room No 603 - Algorithm Lab"><img src="img/facilities/thumbnails/Algorithm.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Nataasha Raul</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Computer Organization and Architecture</li>
                            <li>Data Structure and Files</li>
                            <li>Theory of Computer Science</li>
                            <br>
                        </ul>
                        </div>
                        </div>	<!-- /.fContainer -->
                    </div>
                <div class="col-md-6">
                    <div class="fContainer">
                        
                    	<div class="tag_h4"><h4 align="center"><b>Database System Lab</b></h4></div>
						<hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 606</h5>
                    	<p align="center"><a class="group1" href="img/facilities/DatabaseSystem.jpg" title="Room No 606 - Database System Lab"><img src="img/facilities/thumbnails/DatabaseSystem.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Jyoti Ramteke</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Discrete Structure &amp; Graph Theory</li>
                            <li>Data Structure and Files</li>
                            <li>Advanced database System</li>
                            <li>Soft Computing</li>
                        </ul>
                        </div>
                        </div>	<!-- /.fContainer -->
                    </div>
                            </div>
                            <div class="row">
                <div class="col-md-6">
                    	<div class="fContainer">
                       <div class="tag_h4"><h4 align="center"><b>Computer Graphics</b></h4></div>
					   <hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 606</h5>
                    	<p align="center"><a class="group1" href="img/facilities/ComputerGraphics.jpg" title="Room No 606 - Computer Graphics &amp; Multimedia Lab"><img src="img/facilities/thumbnails/ComputerGraphics.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Kiran Gawande</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Microprocessors</li>
                            <li>Computer Graphics</li>
                            <li>Multimedia System Design</li>
                            <br>
                        </ul>
                        </div>
                        </div>	<!-- /.fContainer -->
                    </div>
                <div class="col-md-6">
                    	<div class="fContainer">
                       <div class="tag_h4"> <h4 align="center"><b>Systems Programming Lab</b></h4></div>
					   <hr class="hr" width="45%">
                        <h5 style="text-align: center; color:#22316c;">Room no 608</h5>
                    	<p align="center"><a class="group1" href="img/facilities/SystemProgramming.jpg" title="Room No 608 - Systems Programming Lab"><img src="img/facilities/thumbnails/SystemProgramming.jpg" class="img-responsive"></a></p>
                        <strong><p align="center">Lab Incharge: Prof. Surekha Dholay</p></strong>
                        <div style="text-align: center;">
                        <ul style="list-style-type:none">
                        	<li>Microprocessors</li>
                            <li>Digital Signal &amp; Image Processing</li>
                            <li>Computer Vision</li>
                            <li>Operating System</li>
                        </ul>
                        </div>
                 
                               
                           </div><!-- /.fContainer -->
                </div> 
	
	
	
	</div>
  </div>
</div>

        
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>