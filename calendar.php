<html>

<head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="">
      <meta name="author" content="">
      <title> CALENDER </title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
	  <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	  
      <link
         href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
         rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
    <style>
  .thumbnails {

            margin-left: 50px !important;

        }

       
        .span12 {
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-color: #FF4500;
            border-width: 2px;
        }
        .fContainer {
    height:auto;
    border:1px solid #999;
    border-radius:5px;
    margin:10px;
        }
            h4 {
            font-family: optima;
        }
    </style>
</head>
<body>
   <?php include 'responsiveHeader/header.html'; ?><br><br>


<div class="container">

<div class="panel panel-default">
    <div class="panel-body">


    <div class="row">


        <div class="span12">
            <br>
            <h3 style="font-family: optima;" align="center">ODD Semester 2015-2016</h3>
            <hr WIDTH="30%">
            <br>

            <center>
                <div class="container">
                    
         <div class="row">
<div class="col-xs-12 col-md-6">
    <div class="fContainer">
                                        <h4 align="center">SE Computers - Sem III 2016-17</h4>

                                        <p><a class="group1" href="img/timetable/se15.jpg" title="SE CMPN - Sem 3"> <img
                                                    u="image" src="img/timetable/se15.jpg" title="SE CMPN - Sem 3"
                                                    height="200px" width="200px"></a></p>
    </div>  </div>
             <div class="col-xs-12 col-md-6">
    <div class="fContainer">
                 
                                        <h4 align="center">TE Computers - Sem V 2016-17</h4>

                                        <p><a class="group1" href="img/timetable/te15.jpg" title="TE CMPN - Sem 5"><img
                                                    u="image" src="img/timetable/te15.jpg" title="TE CMPN - Sem 3"
                                                    height="200px" width="200px"></a></p>
                 </div> </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
    <div class="fContainer">
                                                   <h4 align="center">BE Computers - Sem VII 2016-17</h4>

                                        <p><a class="group1" href="img/timetable/be15.jpg" title="BE CMPN - Sem 7"> <img
                                                    u="image" src="img/timetable/be15.jpg" height="200px" width="200px"></a>
                                        </p>
                            </div>    </div>
                        <div class="col-xs-12 col-md-6">
    <div class="fContainer">
                  
                                        <h4 align="center">ME Computers Sem I 2016-17</h4>

                                        <p><a class="group1" href="img/timetable/me15.jpg" title="ME CMPN"> <img
                                                    u="image" src="img/timetable/me15.jpg" height="200px" width="200px"></a>
                                        </p>
                            </div> </div>
                    </div>    
                    </div>
            </center>

        </div>    <!-- /.span12 -->

    </div>    <!-- /.row -->
	
	  </div>
</div>

</div>    <!-- /container -->

<?php include 'includes/footer.php'; ?>
</body>
</html>