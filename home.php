<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .foot {
            float: left;
            width: 100%;
        }

        #body {
            background-color: white;
        }
    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>    <meta name="description" content="">
    <meta name="author" content="">

    <title> Home </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">


    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
          rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic"
          rel="stylesheet" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript" src="js/jssor.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script>

        jQuery(document).ready(function ($) {


            var _CaptionTransitions = [
                //CLIP|LR
                {$Duration: 900, $Clip: 3, $Easing: $JssorEasing$.$EaseInOutCubic},
                //CLIP|TB
                {$Duration: 900, $Clip: 12, $Easing: $JssorEasing$.$EaseInOutCubic},

                //ZMF|10
                {
                    $Duration: 600,
                    $Zoom: 11,
                    $Easing: {$Zoom: $JssorEasing$.$EaseInExpo, $Opacity: $JssorEasing$.$EaseLinear},
                    $Opacity: 2
                },

                //ZML|R
                {
                    $Duration: 600,
                    x: -0.6,
                    $Zoom: 11,
                    $Easing: {$Left: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic},
                    $Opacity: 2
                },
                //ZML|B
                {
                    $Duration: 600,
                    y: -0.6,
                    $Zoom: 11,
                    $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic},
                    $Opacity: 2
                },

                //ZMS|B
                {
                    $Duration: 700,
                    y: -0.6,
                    $Zoom: 1,
                    $Easing: {$Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic},
                    $Opacity: 2
                },

                //RTT|10
                {
                    $Duration: 700,
                    $Zoom: 11,
                    $Rotate: 1,
                    $Easing: {
                        $Zoom: $JssorEasing$.$EaseInExpo,
                        $Opacity: $JssorEasing$.$EaseLinear,
                        $Rotate: $JssorEasing$.$EaseInExpo
                    },
                    $Opacity: 2,
                    $Round: {$Rotate: 0.8}
                },

                //RTTL|R
                {
                    $Duration: 700,
                    x: -0.6,
                    $Zoom: 11,
                    $Rotate: 1,
                    $Easing: {
                        $Left: $JssorEasing$.$EaseInCubic,
                        $Zoom: $JssorEasing$.$EaseInCubic,
                        $Opacity: $JssorEasing$.$EaseLinear,
                        $Rotate: $JssorEasing$.$EaseInCubic
                    },
                    $Opacity: 2,
                    $Round: {$Rotate: 0.8}
                },
                //RTTL|B
                {
                    $Duration: 700,
                    y: -0.6,
                    $Zoom: 11,
                    $Rotate: 1,
                    $Easing: {
                        $Top: $JssorEasing$.$EaseInCubic,
                        $Zoom: $JssorEasing$.$EaseInCubic,
                        $Opacity: $JssorEasing$.$EaseLinear,
                        $Rotate: $JssorEasing$.$EaseInCubic
                    },
                    $Opacity: 2,
                    $Round: {$Rotate: 0.8}
                },

                //RTTS|R
                {
                    $Duration: 700,
                    x: -0.6,
                    $Zoom: 1,
                    $Rotate: 1,
                    $Easing: {
                        $Left: $JssorEasing$.$EaseInQuad,
                        $Zoom: $JssorEasing$.$EaseInQuad,
                        $Rotate: $JssorEasing$.$EaseInQuad,
                        $Opacity: $JssorEasing$.$EaseOutQuad
                    },
                    $Opacity: 2,
                    $Round: {$Rotate: 1.2}
                },
                //RTTS|B
                {
                    $Duration: 700,
                    y: -0.6,
                    $Zoom: 1,
                    $Rotate: 1,
                    $Easing: {
                        $Top: $JssorEasing$.$EaseInQuad,
                        $Zoom: $JssorEasing$.$EaseInQuad,
                        $Rotate: $JssorEasing$.$EaseInQuad,
                        $Opacity: $JssorEasing$.$EaseOutQuad
                    },
                    $Opacity: 2,
                    $Round: {$Rotate: 1.2}
                },

                //R|IB
                {$Duration: 900, x: -0.6, $Easing: {$Left: $JssorEasing$.$EaseInOutBack}, $Opacity: 2},
                //B|IB
                {$Duration: 900, y: -0.6, $Easing: {$Top: $JssorEasing$.$EaseInOutBack}, $Opacity: 2}

            ];

            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,                    //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0,                           //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
                    $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
                    $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                    $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                    $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$ScaleWidth(Math.min(parentWidth, 850));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
</head>
<body id="body">


       <?php include 'responsiveHeader/header.html'; ?><br><br>

<div id="" style="position : relative ;margin-left:21% ;margin-top:2%"> <!--here you can change the carousel height -->


    <!-- Jssor Slider Begin -->
    <!-- To move inline styles to css file/block, please specify a class name for each element. -->
    <div id="slider1_container"
         style="position: relative; top: 0; left: 0; width: 850px; height: 350px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div class="loading" style="position: absolute; top: 0; left: 0;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0; left: 0;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(/img/loading.gif) no-repeat center;
                top: 0; left: 0;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides"
             style="cursor: move; position: absolute; left: 0px; top: 0px; width: 850px; height: 350px; overflow: hidden;">
            <div>
                <img u="image" src="img/banner02.jpg"/>
                <div u=caption t="*" class="captionOrange"
                     style="position:absolute; left:20px; top: 30px; width:300px; height:30px;">
                    Students of 2014
                </div>
            </div>
            <div>
                <img u="image" src="img/banner01.jpg"/>
                <div u=caption t="*" class="captionOrange"
                     style="position:absolute; left:20px; top: 30px; width:300px; height:30px;">
                    Students of 2014
                </div>
            </div>
            <div>
                <img u="image" src="img/banner03.jpg"/>
                <div u=caption t="*" class="captionOrange"
                     style="position:absolute; left:20px; top: 30px; width:300px; height:30px;">
                    Students of 2014
                </div>
            </div>
            <div>
                <img u="image" src="img/banner04.jpg"/>
                <div u=caption t="*" class="captionOrange"
                     style="position:absolute; left:20px; top: 30px; width:300px; height:30px;">
                    Students of 2014
                </div>
            </div>

        </div>

        <!--#region Bullet Navigator Skin Begin -->
        <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-jquery.html -->


        <!-- bullet navigator container -->
        <div u="navigator" class="jssorb01" style="bottom: 16px; right: 10px;">
            <!-- bullet navigator item prototype -->
            <div u="prototype"></div>
        </div>

        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="top: 123px; right: 8px;">
        </span>


    </div>
</div>

<div class="container">
    <br>
    <br>
    <br>
    <div class="row">

        <div class="col-md-2">

        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3> News </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="http://www.iitb.ac.in/sites/default/files/styles/news_images/public/news/2015-12/IMG_0300_0.JPG?itok=THaw6PJt">
                        </div>
                        <div class="col-md-8">
                            <a href="#">IITB Celebrate its Alumni Day</a>
                            <ul>
                                <li>Four alumni members were presented with Distinguished Service Awards</li>
                                <li>The Silver Jubilee batch of 1990, pledged Rs. 6.5 crore towards the Legacy project
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <img src="http://www.iitb.ac.in/sites/default/files/styles/news_images/public/news/2015-12/IMG_0300_0.JPG?itok=THaw6PJt">
                        </div>
                        <div class="col-md-8">
                            <a href="#">IITB Celebrate its Alumni Day</a>
                            <ul>
                                <li>Four alumni members were presented with Distinguished Service Awards</li>
                                <li>The Silver Jubilee batch of 1990, pledged Rs. 6.5 crore towards the Legacy project
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                            <img src="http://www.iitb.ac.in/sites/default/files/styles/news_images/public/news/2015-12/IMG_0300_0.JPG?itok=THaw6PJt">
                        </div>
                        <div class="col-md-8">
                            <a href="#">IITB Celebrate its Alumni Day</a>
                            <ul>
                                <li>Four alumni members were presented with Distinguished Service Awards</li>
                                <li>The Silver Jubilee batch of 1990, pledged Rs. 6.5 crore towards the Legacy project
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br>


                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3> Events </h3>
                </div>
                <a href="#" class="list-group-item">57th Foundation Day Celebration </a>

                <a href="#" class="list-group-item">Sixth International Congress on Computational Mechanics and
                    Simulation</a>
                <a href="ProductUrology3.html" class="list-group-item">3rd International Conference on MIPS 2016</a>
                <a href="ProductUrology4.html" class="list-group-item">12th TMPDC Conference</a>

            </div>

        </div>

        <!--        <div class="row">-->
        <!--            <div class="col-lg-12">-->
        <!--                <h1 class="page-header">-->
        <!--                    Notice-->
        <!--                </h1>-->
        <!--            </div>-->
        <!--            <div class="col-md-3">-->
        <!--                <div class="panel panel-default">-->
        <!--                    <div class="panel-heading">-->
        <!--                        <h4><i class="fa fa-fw fa-check"></i> FE Comps</h4>-->
        <!--                    </div>-->
        <!--                    <div class="panel-body">-->
        <!--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla-->
        <!--                            aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus-->
        <!--                            eveniet incidunt dicta nostrum quod?</p>-->
        <!--                        <a href="#" class="btn btn-default">See</a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="col-md-3">-->
        <!--                <div class="panel panel-default">-->
        <!--                    <div class="panel-heading">-->
        <!--                        <h4><i class="fa fa-fw fa-check"></i> SE Comps</h4>-->
        <!--                    </div>-->
        <!--                    <div class="panel-body">-->
        <!--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla-->
        <!--                            aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus-->
        <!--                            eveniet incidunt dicta nostrum quod?</p>-->
        <!--                        <a href="#" class="btn btn-default">See</a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-md-3">-->
        <!--                <div class="panel panel-default">-->
        <!--                    <div class="panel-heading">-->
        <!--                        <h4><i class="fa fa-fw fa-check"></i> TE Comps</h4>-->
        <!--                    </div>-->
        <!--                    <div class="panel-body">-->
        <!--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla-->
        <!--                            aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus-->
        <!--                            eveniet incidunt dicta nostrum quod?</p>-->
        <!--                        <a href="#" class="btn btn-default">See</a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!---->
        <!--            <div class="col-md-3">-->
        <!--                <div class="panel panel-default">-->
        <!--                    <div class="panel-heading">-->
        <!--                        <h4><i class="fa fa-fw fa-check"></i> BE Comps</h4>-->
        <!--                    </div>-->
        <!--                    <div class="panel-body">-->
        <!--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla-->
        <!--                            aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus-->
        <!--                            eveniet incidunt dicta nostrum quod?</p>-->
        <!--                        <a href="#" class="btn btn-default">See</a>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!-- Footer -->
        <!--<footer>
            <div class="row">
                <div class="col-lg-12">
                    <p align="center">Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.

+91 22 26707440,26287250</p>
                </div>
            </div>
        </footer>-->

    </div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
</div>
<?php include 'includes/footer.php'; ?>
</body>


</html>

