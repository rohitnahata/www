<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> About </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    
   <style>

      .imgContainer{
    
    margin-right: 50px;
}
   
   .caption
   {
    margin-left:260px;
    margin-right: 160px;
    border: 1px solid #ddd;
    margin-top: 20px;
   }
   
   .span9   {
            width:auto !important;
            text-align:justify;
        }
        .thumbnail  {
            line-height:22px !important;    
        }


   </style>

     <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
  </head>
  <body data-offset="40">
<div class="containerOut">

  
     <?php include 'includes/header.php'; ?> 
        
        <div class="row">
        
                
            <div class="span12">

                <div class="thumbnail">
                    <div class="caption">
                    
                    <h3>Contact</h3>
                    <br/>
                    <p>
                      <h4><b>Dr. Dhananjay Kalbande</b></h4>
                        Professor and Head<br>
                        Department of Computer Enginnering<br><br>
                        
                        Room No. 604,<br>
                        6th Floor, Department of Computer Engineering,<br>
                        Sardar Patel Institute of Technology,<br>
                        Bhavan’s Campus, Munshi Nagar,<br>
                        Andheri (West), Mumbai 400 058<br><br>
                        
                        <strong>Phone:</strong> 022 – 26707440, 26287250 Ext: 366<br><br>
                        <strong>Email:</strong> drkalbande@spit.ac.in<br>              
                    
                    </p>
                    <center>
                    <iframe width="725" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?ie=UTF8&amp;q=Sardar+Patel+Institute+of+Technology&amp;fb=1&amp;gl=in&amp;hq=sardar+patel+institute+of+technology&amp;hnear=0x3be7c6306644edc1:0x5da4ed8f8d648c69,Mumbai,+Maharashtra&amp;cid=0,0,12945796962899063765&amp;t=m&amp;ll=19.125869,72.836115&amp;spn=0.007096,0.00912&amp;z=16&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.in/maps?ie=UTF8&amp;q=Sardar+Patel+Institute+of+Technology&amp;fb=1&amp;gl=in&amp;hq=sardar+patel+institute+of+technology&amp;hnear=0x3be7c6306644edc1:0x5da4ed8f8d648c69,Mumbai,+Maharashtra&amp;cid=0,0,12945796962899063765&amp;t=m&amp;ll=19.125869,72.836115&amp;spn=0.007096,0.00912&amp;z=16&amp;iwloc=A&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
                    </center>
                    
                    </div>  <!-- /.caption -->
                </div>  <!-- /.thumbnail -->
            </div>  <!-- /.span9 -->
            
        </div>  <!-- /.row -->     
    
</div> 

<?php include 'includes/footer.php'; ?> 
</body>
      </html>
