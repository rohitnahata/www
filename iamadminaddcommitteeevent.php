<?php

require_once 'includes/database.php';
require_once 'includes/session.php';
require_once 'includes/uploading_image.php';
if (!isset($_SESSION['aid'])) {
    redirect_to("index.php");
}
if (isset($_POST['mm']) && isset($_SESSION['ssz']) && $_SESSION['ssz'] != "") {
    $sql = "INSERT INTO committee_workshop( content, date, image, committee_id) VALUES ('{$_POST['desc']}','{$_POST['dd']}-{$_POST['mm']}-{$_POST['yyyy']}','{$_SESSION['ssz']}',{$_POST['committee']})";
    $database->query($sql);
} else if (isset($_POST['mm'])) {
    $sql = "INSERT INTO committee_workshop( content, date,  committee_id) VALUES ('{$_POST['desc']}','{$_POST['dd']}-{$_POST['mm']}-{$_POST['yyyy']}',{$_POST['committee']})";
    $database->query($sql);
}

$sql = "select name,id from committee";
$committee_resultset = $database->query($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <title>Department of Computer Engineering | Faculty</title>
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include('includes/control_header.php'); ?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <?php include('includes/admin_sidebar.php'); ?>
        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addFaculty">
                <!-- for adding a new faculty -->
                <div class="error">
                    <?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?>
                </div>
                <!-- print error here -->
                <!-- print error here -->
                <h3>Add Event</h3>
                <ul class="nav">
                    <form id="addFaculty" method="POST" enctype="multipart/form-data">
                        <select name="committee" class="form-control">
                            <?php
                            while ($row = $database->fetch_array($committee_resultset)) {
                                ?>
                                <option value="
																						<?php echo $row['id']; ?>">
                                    <?php echo $row['name']; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        <br>
                        <input type="text" name="desc" placeholder="Event Description" class="form-control" required>
                        <input type="text" name="dd" placeholder="DD" class="form-control" style="width:150px;"
                               required>
                        <input type="text" name="mm" placeholder="MM" class="form-control" style="width:150px;"
                               required>
                        <input type="text" name="yyyy" placeholder="YYYY" class="form-control" style="width:150px;"
                               required>
                        <input type="file" name="image" class="form-control" style="height:auto;"/>
                        <button type="submit" name="submit" class="btn btn-sm btn-primary btn-block faculty-account">
                            Update
                        </button>
                    </form>
                </ul>
            </div>
            <!--/jumbotron for adding a new faculty -->
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <hr>
    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>
</div>
<!--/.container-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
<?php


?>
<?php $database -> close_connection();?>