<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/includes/functions.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/includes/database.php';

?>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> CSI </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    
   <style>



      .imgContainer{
    
    margin-right: 50px;
}
   h5
   {
    color:#22316c;

       }
   .caption
   {
    margin-left:100px;
    margin-right: 300px;
    border: 1px solid #ddd;
    margin-top: 20px;
   }
   
   .span9   {
            width:auto !important;
            text-align:justify;
        }
        .thumbnail  {
            line-height:22px !important;    
        }

#events
 {
color:#000000;
    font-family: "Times New Roman", serif;
width: 20%;
    height: 100%;
    position:absolute; 
    overflow: auto;
padding:0;
margin:0;
margin-left: 1100px;
top: 210px;
} 

#events h2{
     font-family: "Times New Roman", serif;
} 

#events ul li a{
text-decoration :none;
color:#000000;
}

#events ul li a :hover{
color:#000000;
}

   </style>

     <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
  </head>
  <body>


<div class="containerOut">

  
     <?php include 'includes/header.php'; ?>
        
        <div class="row">
        
                
            <div class="span12">


 <div id="events">
<h2> Recent Events</h2>


     <ul>
         <?php
         $sql="SELECT * FROM seminar_workshop where committee_id = '4'  ORDER BY date DESC LIMIT 10";
         $result=$database->query($sql);
         while($row=$database->fetch_array($result)){
             ?>
             <li>
                 <?php echo $row['content'];?>   <!-- Title of the project -->
             </li>
         <?php }?>
     </ul>
</div>

                    <div class="caption">


                    
                               <h3>CSI</h3>
 
                    <h4>Incharge: Prof. Sudhir N. Dhage</h4>
                    <div align="center" >
                <img u="image" src="img/committee/CSI_logo.jpg" height="220px"/>
                
         
                </div>
                <br>
                <h4><p>What is C.S.I. ?</p></h4>

                <p>The seed for the Computer Society of India (CSI) was first sown in the year 1965 with a handful of IT enthusiasts who were a computer user group and felt the need to organize their activities. They also wanted to share their knowledge and exchange ideas on what they felt was a fast emerging sector. Today the CSI takes pride in being the largest and most professionally managed association of and for IT professionals in India. The purposes of the Society are scientific and educational directed towards the advancement of the theory and practice of computer science & IT.</p>

                <br>

                <h4><p>So who are we?</p></h4>

                    <p>
                    We are the C.S.I. Student Branch of Sardar Patel Institute of Technology. Formed in 2009, CSI-SPIT is an altruistic society in college initiating workshops, events and seminars to explore the cornucopia of information other than the regular curriculum offered by the university. As part of our endeavour to bring together and assimilate various aspects of technical and non-technical education, number of seminars and workshops are conducted by professionals imparting knowledge to the students of the college. We also encourage member students to organize events by themselves so that it imbibes in them the skills of management, self-confidence and helps to exchange views and information, learn and share ideas. An annual 10 day industrial visit cum trip is organized to different parts of the country.This facilitates the students' interaction with the industries in different parts of the country and gives them the valuable experience of witnessing the ground realities of the work environment intertwined with the pleasures of visiting the most beautiful places in India. Every person on this planet is blessed with some or the other quality and is dextrous at something. We at CSI-SPIT aim at exploring the acme within every individual so that they can realize their dreams in reality. . 
                    </p>

                    <br>

                    <h4><p>C.S.I. - Mumbai</p></h4>
                    <p>The Computer Society of India is a non-Profit professional meet to exchange views and information learn and share ideas. The wide spectrum of members is committed to the advancement of the theory and practice of Computer Engineering and Technology Systems, Science and Engineering, Information Processing and related Arts and Sciences. The Society also encourages and assists professionals to maintain the integrity and competence of the profession and fosters a sense of partnership amongst members. Besides the activities held at the Chapter and Student Branches, the Society also conducts periodic conferences, seminars at Regional and Divisional levels. </p>

                    <br>
                    <div id ="csi committee" align = "center">
                    <p>
                        <h4>Committee 2016-2017</h4>
                    </p>
                    <img u="image" src="img/csi_2016.jpg" height="300px"/>
                    </div>
                    
                    </div>  <!-- /.caption -->
                </div>  <!-- /.thumbnail -->
            </div>  <!-- /.span9 -->
            
        </div>  <!-- /.row -->
    
</div>

<?php include 'includes/footer.php'; ?>
</body>
      </html>