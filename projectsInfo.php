<?php
require_once 'includes/functions.php';
require_once 'includes/database.php';
//echo $_GET['pid'];
$sql = "SELECT a.*,b.* FROM projects AS a INNER JOIN faculty AS b ON a.faculty_id=b.id WHERE a.id={$_GET['pid']}";
$result = $database->query($sql);
$row = $database->fetch_array($result);
$sql = "SELECT * FROM student WHERE project_id={$_GET['pid']}";
$result1 = $database->query($sql);
?>


<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Department of Computer Engineering | S.P.I.T. Mumbai</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/other.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">

    <style>
        .thumbnail {
            line-height: 22px !important;
        }
		
		#projectContainer
		{
			padding-top: 50px;
		
		}
		#abstract_hr
		{
			width: 15%;
			
		}
		
    </style>
	
</head>

<body data-offset="40">
<div class="containerOut">
    <?php include 'includes/header.php'; ?>

        <div class="row">

            <div class="span12">


                <div id="projectContainer">

                    <div id="displayFrame">
                        <h3 align="center"><?php echo $row['topic']; ?></h3>
						<hr>
                        <span id="student"><strong>Students: </strong><?php
                            while ($row1 = $database->fetch_array($result1))
								echo "{$row1['first_name']} {$row1['last_name']}&nbsp;&nbsp;&nbsp;";
                            ?></span>
                        <span id="guide"><strong>Guide: </strong><?php echo $row['fname']; ?></span>
                      <br><br>
                        
                        <h4 align="center"><strong id="abs">Abstract</strong></h4>
						<hr id="abstract_hr">
                        <p>
                            <?php echo $row['content']; ?>
                        </p>
                        <div style="padding-bottom: 20px"></div>
                    </div>


                </div>    <!-- /#projectContainer -->


            </div>    <!-- /.span12 -->

        </div>    <!-- /.row -->
    <div style="padding-bottom: 50px"></div>


    <?php include 'includes/footer.php'; ?>

</div>

</body>
</html>