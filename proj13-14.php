<?php

require_once './includes/session.php';
require_once './includes/database.php';

//$sql= "select * FROM faculty as A inner join area_of_interest as B ON A.id = B.faculty_id
// inner join professional_affiliation as C on A.id = C.faculty_id
// inner join research_publication as D on A.id = D.faculty_id
// inner join conference as E on A.id = E.faculty_id
// inner join books_published as F on A.id = F.faculty_id
// inner join software_projects as G on A.id = G.faculty_id
// inner join research_grant_project as H on A.id = H.faculty_id
//inner join seminar_workshop as I on A.id = I.faculty_id
//";

// $name = $_GET['username'];

$sql = "select pro.topic , fac.fname ,fac.id  AS fid , pro.id  AS pid from projects AS pro INNER JOIN faculty AS fac ON pro.faculty_id = fac.id where pro.year=2013";
//echo $sql;
$project_result = $database->query($sql);
?>

    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title> 2013-2014 </title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/imageslider.css">
        <link rel="stylesheet" type="text/css" href="css/mystyle.css">
        <style>
            .space-top-lg {
                padding-top: 50px;
                padding-left: 150px;
                padding-right: 50px;
            }

            .space-top-sm {
                padding-top: 20px;
            }

            .space-bottom-sm {
                padding-bottom: 20px;
            }

            .proj {
                background-color: #e5e5e5;
                border-radius: 10px;
                padding-left: 15px;

            }

        </style>
    </head>
    <body>
    <?php include 'includes/header.php'; ?>
    <?php while ($row2 = $database->fetch_array($project_result)) {

        $sql = "SELECT * FROM student WHERE project_id = " . $row2["pid"];
        $student_result = $database->query($sql);
//$row3 = $database->fetch_array($student_result) ;
        ?>
        <div class="container space-top-lg">
            <div class="proj space-top-sm space-bottom-sm">
                <a href="projectsInfo.php?pid=<?php echo $row2['pid']; ?>">
                    <h2><?php echo "{$row2['topic']}" ?></h2>
                </a>
                <!-- Title of the project -->
                <a href="facultyInfo.php?id=<?php echo $row2['fid']; ?>">
                    <h4> Guide: <?php echo $row2["fname"]; ?></h4>
                </a>
                <!-- Name of Project Guide -->
                <ul style="padding-left: 15px">
                    <?php while ($row3 = $database->fetch_array($student_result)) {
                        ?>

                        <!-- student name will be displayed -->
                        <li><?php echo "{$row3['first_name']}" ?> </li>


                    <?php } ?>
                </ul>
            </div>
        </div>


    <?php } ?>

    <hr>


    <?php include 'includes/footer.php'; ?>
    </body>
    </html>

<?php $database->close_connection(); ?>