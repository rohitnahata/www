<?php

require_once './includes/session.php';
require_once './includes/database.php';


?>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>    <meta name="description" content="">
    <meta name="author" content="">


    <title> Placements </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/03.css">

    <style>
        .space-top-lg {
            padding-top: 50px;
        }

        .space-top-sm {
            padding-top: 20px;
        }

        .space-bottom-sm {
            padding-bottom: 20px;
        }

        .space-bottom-md {
            padding-bottom: 30px;
            padding-left: 10px;
        }

        .space-top-md {
            padding-top: 30px
        }

        .placements, .recruiters {
            border: 1px solid;
            padding-left: 10px;
        }

        .plaHead, .recrHead {

            font-size: 2.5em;
            text-align: center;
        }

        3
        .graph {
            text-align: center;

        }

        .container_placement {
            padding-right: 15px;
            padding-left: 25px;
            margin-right: auto;
            margin-left: 240px;
        }

        .h3 {
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size: 300%;

        }

        .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }

    </style>
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>

<div class="containerOut">

    <div class="row" id="row">
        <div class="span12">
            <div class="container space-top-lg space-bottom-lg">
                <div class="thumbnail">
                    <div class="caption">
                        <p class="h3" align="center">Placement Details</p>
                        <hr width="35%" class="hr">
                        <div class="graph space-top-md space-bottom-md">Lorem ipsum dolor sit amet, eos at commodo
                            meliore
                            democritum, nonumy neglegentur ei sit. Ex probatus constituam qui, pri et ferri iuvaret, ne
                            cum
                            quodsi consequat liberavisse. Cu vix inciderint necessitatibus, sint explicari no eam,
                            admodum
                            delectus rationibus ea cum. Ne usu nibh discere iracundia. Ea maluisset consulatu
                            ullamcorper
                            mel, eos et delenit reformidans disputationi.

                        </div>
                        <br>
                        <div id="wrapper">
                            <div class="chart">
                                <table id="data-table" border="1" cellpadding="10" cellspacing="0"
                                       summary="Placements of students from 2010-11 to 2014-15">
                                    <caption>Number of Students</caption>
                                    <thead>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <?php
                                        $sql = "SELECT * FROM placements ORDER BY year DESC";
                                        $result = $database->query($sql);
                                        $row = $database->fetch_array($result);
                                        $count = 0;
                                        // output data of each row
                                        do { ?>
                                            <th scope="col"><?php echo "{$row['year']}" ?></th>
                                            <?php
                                            $count = $count + 1;
                                            if ($count > 4) {
                                                break;
                                            }
                                        } while ($row = $result->fetch_assoc());
                                        ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Appeared</th>

                                        <?php
                                        $sql = "SELECT * FROM placements ORDER BY year DESC";
                                        $result = $database->query($sql);
                                        $row = $database->fetch_array($result);
                                        $count = 0;
                                        // output data of each row
                                        do { ?>
                                            <td><?php echo "{$row['number_of_students_appeared']}" ?></td>
                                            <?php
                                            $count = $count + 1;
                                            if ($count > 4) {
                                                break;
                                            }
                                        } while ($row = $result->fetch_assoc())
                                        ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Placed</th>

                                        <?php
                                        $sql = "SELECT * FROM placements ORDER BY year DESC";
                                        $result = $database->query($sql);
                                        $row = $database->fetch_array($result);
                                        $count = 0;
                                        // output data of each row
                                        do { ?>
                                            <td><?php echo "{$row['number_of_students_placed']}" ?></td>
                                            <?php
                                            $count = $count + 1;
                                            if ($count > 4) {
                                                break;
                                            }
                                        } while ($row = $result->fetch_assoc())
                                        ?>
                                    </tr>
                                    <tr>
                                        <th scope="row">Dream Placement</th>
                                        <?php
                                        $sql = "SELECT * FROM placements ORDER BY year DESC";
                                        $result = $database->query($sql);
                                        $row = $database->fetch_array($result);
                                        $count = 0;
                                        // output data of each row
                                        do { ?>
                                            <td><?php echo "{$row['dream_placed']}" ?></td>
                                            <?php
                                            $count = $count + 1;
                                            if ($count > 4) {
                                                break;
                                            }
                                        } while ($row = $result->fetch_assoc())
                                        ?>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- JavaScript at the bottom for fast page loading -->

                        <!-- Grab jQuery from Google -->
                        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

                        <!-- Example JavaScript -->
                        <script src="js/03.js"></script>

                    </div>
                </div>
            </div>


            <div class="container space-top-lg space-bottom-lg">
                <div class="thumbnail">
                    <div class="caption">
                        <h3 class="h3" align="center">Our Recruiters</h3>
                        <hr width="35%" class="hr">
                        <div class="recrImages space-top-md space-bottom-md">
                            <div class="row space-top-small space-bottom-small"
                                 style="padding-left: 5px; padding-right: 5px; ">

                                <?php
                                $dirname = "img/recruiters/";
                                $images = glob($dirname . "*.png");
                                $count = 0;

                                foreach ($images as $image) {

                                    if ($count % 6 == 0) {
                                        echo /** @lang text */
                                        '<div class="row space-top-small space-bottom-small" style="padding-left: 5px; padding-right: 5px;">';
                                    }
                                    echo /** @lang text */
                                        '<div class = "col-sm-2"><img class="rImg" src="' . $image . '"width="100%" height="auto" /></div>';
                                    if ($count % 6 == 5) {
                                        echo /** @lang text */
                                        '</div>';
                                    }
                                    $count++;
                                }
                                ?>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>

</html>
