<?php

require_once './includes/session.php';
require_once './includes/database.php';


$sql = "select * FROM faculty where id = {$_GET['id']}";
$faculty_result = $database->query($sql);
$row = $database->fetch_array($faculty_result);
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Include scripts -->
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
        <script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>
        <!-- Include styles -->
        <link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>
        <!-- Include media queries -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo "{$row['fname']}" ?></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <style>
        #facultyBody {
            /*margin-left: 120px;
            margin-right: 20px; */
            margin-left: 40px;
            margin-right: 40px;
            border: 1px solid #ddd;
            margin-top: 10px;
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif; /*padding-top: 20px;*/
        }

        .caption {
            margin-right: 50px;

            color: black;
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
            font-size: 150%;
        }

        .thumbnail {
            line-height: 22px !important;
        }

        .h3 {
            color: black;
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
            font-size: 250%;
        }

        .h4 {
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size: 240%;
        }

        .h5 {
            color: black;
            font-size: 85%;

        }

        .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }

        .pad {
            padding-left: 30px;
            font-size: 200%;
            top: 0;
        }

        .Info {
            color: black;
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
            font-size: 65%;
			margin-right:20px;
        }

    </style>
    <link
            href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
            rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic"
          rel="stylesheet" type="text/css">
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>

<div class="containerOut">
    <div class="row">
        <div id="facultyBody">
            <div>
                <div class="thumbnail">
                    <div class="h3">
                        <p align="center">
                            <?php echo "{$row['fname']}" ?>
                            <br>
                        </p>
                    </div>
                    <HR class="hr" WIDTH="40%">
                    <br/>
                    <div class="caption">
                        <img src="<?php echo "{$row['image_path']}"; ?>"
                             style="height: 200px; position: relative; float: left;padding-right:10px;">
                        <!--<div align="center" ><img u="image" src="" height="220px"/></div> -->
                    </div>

                    <div class="Info" >
                        <div class="pad">
                            <?php echo "{$row['position']}" ?>
                            <br>
                            <?php echo "{$row['qualification']}" ?>
                            <br>
                            <P>
                               <span class="contactInfo">Address: Room No.
                                   <?php echo "{$row['room_number']}" ?>, 6th Floor,
                               </span>
                                <!-- Room number and floor -->
                                Department of Computer Engineering,
                                <br>
                                Sardar Patel Institute of Technology,
                                <br>
                                Andheri(W), Mumbai 400058.
                                <br>
                                <span class="contactInfo">
                                    Email: <a
                                            href="mailto: <?php echo "{$row['email']}" ?>"><?php echo "{$row['email']}" ?></a>
                                </span>
                                <!-- Email ID -->
                                <br>
                                <span class="contactInfo">
                                    Phone No.: <?php echo "{$row['telephone']}" ?>
                                    Ext:<?php echo "{$row['extension']}" ?>
                                </span>
                                <!-- Phone Number with extention -->

                                <?php if ($row['cv']) { ?>
                                    <br>
                                    <a href="<?php echo "{$row['cv']}" ?>" target="_blank">View My CV</a>
                                <?php } ?>
                                <br>
                                <br>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /#facultyHeadInfo -->
            </div>    <!-- /#facultyHead -->
            <br>


            <?php if ($row['id'] == '1') { ?>
                <div class="pad">
                    <div class="h5">
                        <p>Post Ph.D Collaborative Research In Progress :</p>
                    </div>

                    <ul>
                        <div class="Info">
                            <li>
                                Research work Title : Skin Image Analysis Software to recognize skin diseases:- A
                                clinical
                                study to recognize skin diseases.
                                Principal Investigator and Guide: Dr.Uday Khopkar,Professor & Head, Dept.of
                                Dermatology,Seth
                                G.S.Medical College and KEM Hospital.
                            </li>
                            <li>
                                Co-Investigators : Dr.Dhananjay Kalbande,Dr.Mithali Jage
                            </li>
                        </div>
                    </ul>

                </div>
                <br>
                <div class="pad">
                    <div class="h5">
                        <p>Certification Done :</p>
                    </div>
                    <ul>
                        <div class="Info">
                            <li>GCP: Good Clinical Practices : Course Completion Date: 22 May 2015 ,CTN Expiration Date:
                                22
                                May 2018.
                            </li>
                        </div>
                    </ul>
                </div>
                <br>
            <?php } ?>

            <?php $sql = "select * FROM area_of_interest where faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <div class="pad">
                    <div class="h5">
                        <p>Area of Interest:</p>
                    </div>
                    <ul>
                        <div class="Info">
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['description']; ?>
                                </li>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            <?php } ?>
            <br>
            <?php $sql = "select * FROM professional_affiliation where faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <div class="pad">
                    <div class="h5">
                        <p>Professional Affiliations: </p>
                    </div>
                    <ul>
                        <div class="Info">
                            <!-- li will repeat in loop -->
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['description']; ?>
                                </li>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            <?php } ?>
            <br>
            <?php $sql = "select * FROM research_publication where type = 'N' AND faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <!--                <div class="h4">-->
                <!--                    <p>PUBLICATIONS</p>-->
                <!--                </div>-->
                <!--                <hr class="hr" WIDTH="25%" COLOR="#FF0000">-->
                <!--                <br>-->
                <div class="pad">
                    <div class="h5">
                        <p>National Journal:</p>
                    </div>
                    <ul>
                        <div class="Info">
                            <!-- li will repeat in loop -->
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['title']; ?>
                                </li>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            <?php } ?>
            <?php $sql = "select * FROM research_publication where type = 'I' AND faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <div class="pad">
                    <div class="h5">
                        <p>International Journal:</p>
                    </div>
                    <ul>
                        <div class="Info">
                            <!-- li will repeat in loop -->
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['title']; ?>
                                </li>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            <?php } ?>
            <?php $sql = "select * FROM conference where type = 'N' AND faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <div class="pad">
                    <div class="h5">
                        <p>National Conference:</p>
                    </div>
                    <div class="Info">
                        <ul>
                            <!-- li will repeat in loop -->
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['title']; ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <?php $sql = "select * FROM conference where type = 'I' AND faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) { ?>
                <div class="pad">
                    <div class="h5">
                        <p>International Conference:</p>
                    </div>
                    <ul>
                        <div class="Info">
                            <!-- li will repeat in loop -->
                            <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                                <li>
                                    <?php echo $row['title']; ?>
                                </li>
                            <?php } ?>
                        </div>
                    </ul>
                </div>

            <?php } ?>
            <?php $sql = "select * FROM others where  faculty_id = {$_GET['id']}";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)){ ?>

            <div class="pad">

                <div class="h5">
                    <p>Miscellaneous:</p>
                </div>
                <ul>
                    <div class="Info">
                        <!-- li will repeat in loop -->
                        <?php while ($row = $database->fetch_array($faculty_result)) { ?>
                            <li>
                                <?php echo $row['description']; ?>
                            </li>
                        <?php } ?>
                    </div>
                </ul>
                <?php } ?>
            </div>
            <br>
            <br>
        </div>


    </div>

</div>

<br>
<br>

<?php include 'includes/footer.php'; ?>
</body>
</html>