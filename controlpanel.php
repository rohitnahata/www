<?php
    require_once 'includes/functions.php';
    require_once 'includes/database.php';
    
    if(isset($_POST['username']) && isset($_POST['password'])){
        if(!empty($_POST['username']) && !empty($_POST['password'])){
            $type=$_POST['typeofuser'];
            $username=$_POST['username'];
            $password=hash("sha512",$_POST['password']);
            
            
                $sql="SELECT id,username,password FROM admin WHERE username='{$username}' AND password='{$password}'";
                    //echo $sql;
                    $result=$database->query($sql);
                    if(mysqli_num_rows($result)==1){
                        require_once 'includes/session.php';
                        $result=$database->fetch_array($result);
                        $_SESSION['aid']=$result['id'];
                        redirect_to ("iamadminadd.php");
                    }
                    else{
               
                        $message= "Invalid username/password combination";
                        //redirect_to("projects.php");
                    }
            }
            
        }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Department of Computer Engineering | S.P.I.T</title>


    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
	<link href="css/admin_signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#">Administrator Panel : <strong>Department of Computer Engineering</strong> |
                <span
                    style="font-size: smaller; ">Sardar Patel Institute of Technology</span></a>
        </div>
      </div>
    </div>

    <div class="container">
        
        
        <form class="form-signin" action="controlpanel.php" method="POST">
            <br>
            <div style="text-align: center;">
                <div class="error"><?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?></div>
            </div>    <!-- print error here -->
            <h2 class="form-signin-heading">Login</h2>
        <input type="text" class="form-control" placeholder="Username" required autofocus name="username" autocomplete="off">
        <input type="password" class="form-control" placeholder="Password" required name="password" autocomplete="off">
  <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
