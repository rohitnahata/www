<?php
//echo print_r($_POST);
//isset($_SESSION['aid'])
require_once 'includes/database.php';
require_once 'includes/session.php';
require_once $_SERVER['DOCUMENT_ROOT'] . 'includes/functions.php';
require_once 'includes/uploading_image.php';
if (isset($_SESSION['aid'])) {//why this line?? harsh
    if (isset($_POST['companyname']) && isset($_POST['companywebsite']) && isset($_POST['companydesc'])) {
        if (!empty($_POST['companyname']) && !empty($_POST['companywebsite']) && !empty($_POST['companydesc'])) {
            $sql = "SELECT name FROM iic_company WHERE name='{$_POST['companyname']}'";
            $result = $database->query($sql);
            if ($database->number_of_rows($result) == 0) {
                $sql = "INSERT INTO iic_company(name,url,description,logo) VALUES('{$database->escape_value($_POST['companyname'])}','{$database->escape_value($_POST['companywebsite'])}','{$database->escape_value($_POST['companydesc'])}','{$database->escape_value($_SESSION['ssz'])}')";
                $result=$database->query($sql);
                $message = 'Added';
            }else{
                $message = 'Company already exists';
            }
        } else {
            $message = "Please enter all details";
        }
    }
} else {
    redirect_to("index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <title>Department of Computer Engineering | Faculty</title>
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include('includes/control_header.php'); ?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <?php include('includes/admin_sidebar.php'); ?>
        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addFaculty">
                <!-- for adding a new faculty -->
                <div class="error">
                    <?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?>
                </div>
                <!-- print error here -->
                <!-- print error here -->
                <h3>Add IIC Company</h3>
                <ul class="nav">
                    <form id="addFaculty" method="POST" action="" enctype="multipart/form-data">
                        <input type="text" placeholder="Company Name" class="form-control" name="companyname">
                        <input type="text" placeholder="Company Website" class="form-control" name="companywebsite">
                        <textarea placeholder="Company Description" class="form-control" name="companydesc"></textarea>
                        <br>
                        <input type="file" class="form-control" name="image" style="height:auto;">
                        <input type="submit" value="Add" class="btn btn-sm btn-primary btn-block faculty-account"/>
                    </form>
                </ul>
            </div>
            <!--/jumbotron for adding a new faculty -->
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <hr>
    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>
</div>
<!--/.container-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
			