<?php
//echo print_r($_POST);
require_once '/includes/database.php';
require_once '/includes/session.php';
require_once '/includes/functions.php';


if (isset($_SESSION['aid'])) {

    $sql="select * from faculty;";
    $all_faculty=$database->query($sql);

    if(isset($_POST['title'])&&isset($_POST['teacher'])&&isset($_POST['title'])&&isset($_POST['students'])&&!empty($_POST['students'][0])){
        $sql="insert into projects (topic,faculty_id,content,year) values ('{$database->escape_value($_POST['title'])}',".$_POST['teacher'].",'{$database->escape_value($_POST['abstract'])}','{$database->escape_value($_POST['year'])}')";
        //echo $sql;
        if($database->query($sql));
            //echo done;
        $sql="select max(id) from projects";
        $id=$database->query($sql);
        $id=$database->fetch_array($id);
        $id = $id[0];
        foreach ($_POST['students'] as $value) {
            if(!empty($value)){
                $sql="insert into student (first_name,last_name,project_id) values ('{$database->escape_value($value)}','',{$id});";
                $database->query($sql);
            }
        }

        //echo "done";
        $message="Added";
    }else if(isset($_POST['year'])){
        $message="Please Enter All Inputs";
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <title>Department of Computer Engineering | Faculty</title>
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include('includes/control_header.php'); ?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <?php include('includes/admin_sidebar.php'); ?>
        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addProject">
                <!-- for adding new projects -->
                <div class="error">
                    <?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?>
                </div>
                <!-- print error here -->
                <h3>Add Project</h3>
                <ul class="nav">
                    <form method="post" action="iamadminproject.php">
                        <li class="pInfo">
                            <div id="addStudent">
                                <input type="text" class="form-control" placeholder="Title" name="title">
                                <select class="form-control" name="teacher">
                                    <?php while ($row = $database->fetch_array($all_faculty)) {

                                        ?>
                                        <option value="
																									<?php echo $row['id']; ?>">
                                            <?php echo $row['fname']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <hr>
                                <input type="text" class="form-control" placeholder="Student" name="students[]">
                            </div>
                            <select class="form-control" name="year">
                                <option value="2009">2009-10</option>
                                <option value="2010">2010-11</option>
                                <option value="2011">2011-12</option>
                                <option value="2012">2012-13</option>
                                <option selected value="2013">2013-14</option>
                            </select>
                            <div class="btn btn-sm btn-primary btn-block addMore" onclick="students_addmore()">Add
                                Student
                            </div>
                            <textarea class="form-control" placeholder="Abstract" name="abstract"></textarea>
                        </li>
                        <button class="btn btn-sm btn-primary btn-block faculty-account" style="float:left">Add
                            Project
                        </button>
                    </form>
                </ul>
            </div>
            <!--/jumbotron for adding new projects -->
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <hr>
    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>
</div>
<!--/.container-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
			