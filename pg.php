<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Vision&Mission </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <style>
		h4{
			font-size:150%;
			font-weight:bold;
			
		}
        body {
            overflow-x: hidden;

        }
        .thumbnail {
            height: auto;
			margin-right:20px;
        }

        .thumbnails {
			margin-right:20px;
        }

        .thumbnails h4 {
            text-align: center;
            color: #333333;
        }

        .thumbnails li {
            margin: 2px;
        }

        .thumbnails a {
            text-decoration: none !important;
        }

        .thumbnail li {
            list-style-type: square;
        }

        .h3{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;

        }

        .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }


    </style>
    <script>
        $(document).ready(function () {
            //Examples of how to assign the Colorbox event to elements
            $(".group1").colorbox({rel: 'group1', height: '80%'});


        });
    </script>
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>

<p class = "h3" align="center">Post Graduate</p>
<hr width="50%" class="hr">
<div class="container">
    <ul class="thumbnails" style="list-style: none">

        <li class="span5">

            <div class="thumbnail">

                <a href="pdf/mecourse.pdf#page=5"><h4 style="margin-left:10px;"> ME CMPN - Sem 1</h4></a>
				<hr width="30%" class="hr">


                <ul>
                    <a href="pdf/mecourse.pdf#page=10">
                        <li>Advanced Algorithms and Complexity</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=8">
                        <li>Parallel Computing</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=8">
                        <li>Network Design and Management</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=12">
                        <li>Elective I</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=22">
                        <li>Elective II</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=28">
                        <li>Laboratory I - Open Source #</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=29">
                        <li>Laboratory II - Advanced Algorithms and Complexity Lab</li>
                    </a>

                </ul>
            </div>

        </li>

        <br>


        <li class="span5">
            <div class="thumbnail">

                <a href="pdf/mecourse.pdf#page=30"><h4 style="margin-left:10px;"> ME CMPN - Sem 2</h4></a>
				<hr width="30%" class="hr">


                <ul>
                    <a href="pdf/mecourse.pdf#page=31">
                        <li>Advanced Operating System</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=33">
                        <li>Cyber Security</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=35">
                        <li>Decision Making and Adaptive Business Intelligence</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=37">
                        <li>Machine Learning</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=39">
                        <li>Elective III</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=45">
                        <li>Elective IV</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=51">
                        <li>Laboratory I - Open Source #</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=52">
                        <li>Laboratory II - Cyber Security and Decision Making and Adaptive Business Intelligence</li>
                    </a>
                </ul>
            </div>

        </li>

        <br>

        <li class="span5">
            <div class="thumbnail">

                <a href="pdf/mecourse.pdf#page=4"><h4 style="margin-left:10px;"> ME CMPN - Sem 3</h4></a>
				<hr width="30%" class="hr">


                <ul>
                    <a href="pdf/mecourse.pdf#page=53">
                        <li>Seminar</li>
                    </a>
                    <a href="pdf/mecourse.pdf#page=54">
                        <li>Dissertation I</li>
                    </a>

                </ul>
            </div>

        </li>
        <br>


        <li class="span5">
            <div class="thumbnail">

                <a href="pdf/mecourse.pdf#page=4"><h4 style="margin-left:10px;"> ME CMPN - Sem 4</h4></a>
				<hr width="30%" class="hr">


                <ul>
                    <a href="pdf/mecourse.pdf#page=54">
                        <li>DissertationII</li>
                        <br></a>

                </ul>
            </div>

        </li>
    </ul>
</div>
<br>
<br>

<?php include 'includes/footer.php'; ?>

</body>
</html>