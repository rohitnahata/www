<?php
    require_once './includes/session.php';
    require_once './includes/database.php';
    
    //$sql= "select * FROM faculty as A inner join area_of_interest as B ON A.id = B.faculty_id 
    // inner join professional_affiliation as C on A.id = C.faculty_id
    // inner join research_publication as D on A.id = D.faculty_id
    // inner join conference as E on A.id = E.faculty_id
    // inner join books_published as F on A.id = F.faculty_id
    // inner join software_projects as G on A.id = G.faculty_id
    // inner join research_grant_project as H on A.id = H.faculty_id
    //inner join seminar_workshop as I on A.id = I.faculty_id
    //";
    
    // $name = $_GET['username'];
    
    $sql = "SELECT * FROM faculty WHERE visibility=1 ORDER BY PRIORITY";
    $faculty_result = $database->query($sql);
    
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            #row {
            /*margin-left: 120px;
            margin-right: 60px; */
            border: 1px solid #ddd;
            margin-top: 10px;
			margin-left:10px;
            /*padding-top: 20px;*/
            }
            }
            #team {
            height: 350px;
            width: 320px;
            }
            #body {
            background-color: white;
            }
            .h2 {
            text-align: center;
            font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
            }
            #h3{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;
            }
            #hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
            }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Include scripts -->
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
        <script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>
        <!-- Include styles -->
        <link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>
        <!-- Include media queries -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <meta name="description" content="">
        <meta name="author" content="">
        <title> Faculty </title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/imageslider.css">
        <link rel="stylesheet" type="text/css" href="css/mystyle.css">
        <link
            href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
            rel="stylesheet" type="text/css">
        <link
            href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic"
            rel="stylesheet" type="text/css">
    </head>
    <body id="body">
        <!-- Header part starts -->
        <?php include 'responsiveHeader/header.html'; ?><br><br>
        <!-- Header parts end -->
        <!-- Page Content -->
        <div class="container">
            <!-- Team Members -->
            <div class="row" id="row">
                <!-- <div class="col-lg-12">
                    <h2 class="page-header">Our Team</h2>
                    </div>  -->
                <p class="h3" id="h3" align="center">Faculty </p>
				<hr style="width:50%;">
                <br>
                <?php while ($row = $database->fetch_array($faculty_result)) {
                    ?>
                <div class="col-md-4 text-center" id="team">
                    <div class="thumbnail">
                        <!-- gets the image location from the database and makes it clickable which returns to the page"facultyInfo.php?id="given id" -->
                        <a href="facultyInfo.php?id=<?php echo $row['id']; ?>"><img class="img-responsive"
                            src="<?php echo "{$row['image_path']}"; ?>"
                            alt="<?php echo "{$row['fname']}" ?>"
                            style="height:200px; width:270px"></a>
                        <div class="caption">
                            <!-- gets the   image locatipon from the database and makes it clickable which returns to the page"facultyInfo.php?id="given id" -->
                            <a href="facultyInfo.php?id=<?php echo $row['id']; ?>">
                                <h4><?php echo "{$row['fname']}" ?></h4>
                            </a>
                            <p><?php echo "{$row['position']}" ?><br>
                                <b>Tel:</b> +91 (22) – <?php echo "{$row['telephone']}" ?>
                                Ext:<?php echo "{$row['extension']}" ?> <br> <b>
                                Email:</b> <?php echo "{$row['email']}" ?> 
                            </p>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <!-- /.row -->
            <!-- Footer -->
        </div>
        <!-- /.container -->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
    <?php include 'includes/footer.php'; ?>
</html>
<?php $database->close_connection(); ?>