<footer>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <style>
        .foot_up {
            background-color: #3d3d3d;
            color: #fff;
        }

        #logo1 {
            height: 80px;
            width: 80px;

        }

        .marker, .call {
            text-align: center;
            padding-bottom: 10px;
        }

        .imglogo {
            width: 80px;
            margin: 0 auto;

        }

        a:hover {
            text-decoration: none;
        }

        #footer_hr {
            border-color: #fff;
            border-width: 1px;
        }

    </style>

    <div class="container-fluid foot_up" style="padding-top: 10px;">

        <div style="width: 100%">
            <div class="col-lg-4">
                <div class="marker">
                    <i class="fa fa-map-marker" style="font-size: 2.5em;" aria-hidden="true"></i>
                </div>
                <p align="center" style="font-size: 1.5em; font-weight: bold;">Address:</p>
                <a href="https://goo.gl/maps/5kWLqq3BCCE2">
                    <p align="center" style="color: #fff;">
                        Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology,<br>
                        Bhavans Campus, Munshi Nagar,<br>
                        Andheri (West), Mumbai, Maharashtra 400058</p>
                </a>
            </div>

            <div class="col-lg-4">
                <a href="http://www.spit.ac.in/">
                    <div class="spit">
                        <div class="imglogo">
                            <img id="logo1" src="images\logo_png.png" style="opacity: 1.0;"/>
                        </div>
                        <p align="center" style="color: #fff; padding-top: 10px; font-size: 1.5em; font-weight: bold;">
                            Sardar Patel Institute of Technology
                        </p>
                    </div>
                </a>
            </div>


            <div class="col-lg-4">

                <div class="call">
                    <i class="fa fa-phone" style="font-size: 2.5em;" aria-hidden="true"></i>
                </div>
                <p align="center" style="font-size: 1.5em; font-weight:bold;">Contact us:</p>
                <a href="tel: (91)-(022)-26707440"><p align="center" style="color: #fff;"> (91)-(022)-26707440</p></a>
                <a href="tel: (91)-(022)-26287250"><p align="center" style="color: #fff;"> (91)-(022)-26287250</p></a>
            </div>
            <div class="col-lg-4" style="width: 100%">
                <hr id="footer_hr" width="100%" style="position: relative;"><br>
                <p style="margin-top: -10px; text-align: center; ">Copyright &copy; 2017 Bharatiya Vidya
                    Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
            </div>
        </div>
    </div>

</footer>