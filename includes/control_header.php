    <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand" style="color:#FFF; cursor:default">Welcome Faculty | <strong>Department of Computer Engineering</strong> | <font size="-1">Sardar Patel Institute of Technology</font></div>
        </div>
      </div><!-- /.container -->
    </div><!-- /.navbar -->