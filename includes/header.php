<div class="top" >
    <div class="row">

        <div class="col-md-3">


            <a href="http://www.spit.ac.in/" id="logo">
                <img src="images/logo_png.png" height="120px" width="120px"
                     style="position:relative;float:left; margin:10px 0 10px 250px; opacity: 1.0;"
                     ondragstart="return false">
            </a>

        </div>
        <div class="col-md-9" style=" position: relative; float: right; padding-left: 70px">
            <h1 style="margin-top: 1.2em">Department of Computer Engineering</h1>
            <h3 style="margin-top: -0.1em">Sardar Patel Institute Of Technology</h3>
        </div>
    </div>


    <div id='cssmenu'>
        <ul>
            <li class='has-sub' style="z-index: 1000;"><a href='home.php'><span>Home</span></a>
                <ul>
                    <li><a href='about.php'><span>About us</span></a></li>
                    <li class='dropdown-menu-right' style="z-index: 50;"><a
                            href='ug.php'><span>Programs</span></a>
                        <ul>
                            <li><a href='ug.php'><span>Under Graduate</span></a></li>
                            <li><a href='pg.php'><span>Post Graduate</span></a></li>
                            <li><a href='phd.php'><span>Ph.D</span></a></li>

                        </ul>
                    </li>
                    <li><a href='contact.php'><span>Contact us</span></a></li>
                </ul>
            </li>


            <li class='has-sub' style="z-index: 900;"><a href='faculty.php'><span>Department</span></a>
                <ul>
                    <li><a href='faculty.php'><span>Faculty</span></a></li>
                    <li><a href='facilities.php'><span>Facilties</span></a></li>
                    <li><a href="calendar.php"><span>Timetable</span></a></li>

                </ul>
            </li>
            <li class='has-sub' style="z-index: 800;"><a href='iic_about.php'><span>IIC</span></a>
                <ul>
                    <li><a href='iic_about.php'><span>About IIC</span></a></li>
                    <li><a href='iic_companies.php'><span>Companies</span></a>
                </ul>
            </li>
            <li class='has-sub' style="z-index: 700;"><a href='consultancy.php'><span>Industry</span></a>
                <ul>
                    <li><a href='consultancy.php'>Consultancy<span></span></a>
                    <li><a href='placement.php'><span>Placement</span></a></li>
                    <li><a href='research.php'>Research<span></span></a></li>
                </ul>
            </li>

            <li class='has-sub' style="z-index: 600;"><a href='proj15-16.php'><span>Projects</span></a>
                <ul>
                    <li><a href='proj16-17.php'><span>2016-17</span></a></li>
                    <li><a href='proj15-16.php'><span>2015-16</span></a></li>
                    <li><a href='proj14-15.php'><span>2014-15</span></a></li>
                    <li><a href='proj13-14.php'><span>2013-14</span></a></li>
                    <li><a href='proj12-13.php'><span>2012-13</span></a></li>
                    <li><a href='proj11-12.php'><span>2011-12</span></a></li>
                </ul>
            </li>

            <li class='has-sub' style="z-index: 500;"><a href='achievements16-17.php'><span>Achievements</span></a>
                <ul>
                    <li><a href='achievements16-17.php'><span>2016-17</span></a></li>
                    <li><a href='achievements15-16.pho'><span>2015-16</span></a></li>
                    <li><a href='achievements14-15.php'><span>2014-15</span></a></li>
                    <li><a href='achievements13-14.php'><span>2013-14</span></a></li>
                </ul>
            </li>

            <!--            <li class='has-sub' style="z-index: 200;"><a href='#'><span>Survey</span></a>-->
            <!---->
            <!--                        <ul>-->
            <!--                         <li><a href='#'><span>Alumni Feedback</span></a>-->
            <!--                -->
            <!--                         </li>-->
            <!--                         <li><a href='#'>Employer Survey<span>-->
            <!--                         </span></a>-->
            <!--                -->
            <!--                -->
            <!--                         <li><a href='#'><span>Student Mentor</span></a>-->
            <!--                -->
            <!--                         </li>-->
            <!--                       <li><a href='#'><span>Parent Feedback</span></a>-->
            <!--                -->
            <!--                         </li>-->
            <!--                         <li><a href='#'><span>Program Exit Survey</span></a>-->
            <!--                -->
            <!--                         </li>-->
            <!--                -->
            <!--                -->
            <!--                               </ul>-->
            <!---->
            <!---->
            <!--            </li>-->

            <li class='has-sub' style="z-index: 400;"><a href='face.php'><span>Committee</span></a>
                <ul>
                    <li><a href='face.php'><span>FACE</span></a></li>
                    <li><a href='pi.php'><span>PI Club</span></a></li>
                    <li><a href='csi.php'><span>CSI</span></a></li>
                    <li><a href='ecell.php'><span>E Cell</span></a></li>

                </ul>
            </li>
        </ul>
    </div>
</div>