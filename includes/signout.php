<?php
    require_once './session.php';
    require_once './functions.php';
    if(isset($_SESSION)){
        unset($_SESSION);
        session_destroy();
    }
    redirect_to("../home.php");
?>
