<?php
//echo print_r($_POST);
require_once '/includes/database.php';
require_once '/includes/session.php';
require_once '/includes/functions.php';
require_once '/includes/uploading_image.php';
if (isset($_SESSION['aid'])) {
    if (isset($_POST['description'])) {
        if ($_POST['type'] == "Latest Announcement") {
            $sql = "INSERT INTO latest_announcements(image,content) VALUES('{$database->escape_value($_SESSION['ssz'])}','{$database->escape_value($_POST['description'])}')";
            $database->query($sql);

        } else {
            $sql = "INSERT INTO other_workshop(image,content) VALUES('{$database->escape_value($_SESSION['ssz'])}','{$database->escape_value($_POST['description'])}')";
            $database->query($sql);

        }
    }
} else {
    redirect_to("index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Department of Computer Engineering | Faculty</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include('includes/control_header.php'); ?>

<div class="container">


    <div class="row row-offcanvas row-offcanvas-right">

        <?php include('includes/admin_sidebar.php'); ?>

        <div class="col-xs-12 col-sm-9">

            <div class="jumbotron" id="deleteFaculty">    <!-- for deleting a faculty -->

                <div class="error"></div>    <!-- print error here -->

                <h3>Announcements</h3>
                <ul class="nav">
                    <form method="POST" action="iamadminannouncements.php" enctype="multipart/form-data">
                        <input type="file" class="form-control" style="height:auto" name="image">
                        <textarea class="form-control" placeholder="Description" name="description"></textarea>
                        <select class="form-control" name="type">
                            <option>Latest Announcement</option>
                            <option>Seminar/Workshop</option>
                        </select>
                        <input type="submit" class="btn btn-sm btn-primary btn-block faculty-account"
                               placeholder="Description" value="Add">
                    </form>
                </ul>
            </div>    <!--/jumbotron for deleting a faculty -->

        </div><!--/span-->

    </div><!--/row-->

    <hr>

    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
