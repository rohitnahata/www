<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>    <meta name="description" content="">
    <meta name="author" content="">

    <title> Vision&Mission </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <style>
        body {
            overflow-x: hidden;

        }

        .space-top-lg {
            padding-top: 50px;
        }

        .space-bottom-lg {
            padding-bottom: 50px;
        }

        .space-top-sm {
            padding-top: 20px;
        }

        .space-bottom-sm {
            padding-bottom: 20px;
        }

        .space-bottom-md {
            padding-bottom: 30px
        }

        .space-top-md {
            padding-top: 30px
        }

        .imgAbout {
            width: 100%;
            height: auto;
            border: 2px solid #fff;

        }

        .about {
            text-align: center;
            border: 2px solid;
            border-radius: 10px;
        }

        #imgDescription {
            font-style: italic;
            font-size: 1.2em;
        }

        .thumbnail {
            height: 180px;
        }

        .thumbnails {
            margin-left: 50px !important;
        }

        .thumbnails h4 {
            text-align: center;
            color: #333333;
        }

        .thumbnails li {
            margin: 2px;
        }

        .thumbnails a {
            text-decoration: none !important;
        }

        .thumbnail li {
            list-style-type: square;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-color: #FF4500;
            border-width: 2px;
        }
		


    </style>
    <script>
        $(document).ready(function () {
            //Examples of how to assign the Colorbox event to elements
            $(".group1").colorbox({rel: 'group1', height: '80%'});


        });
    </script>
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>
         <div class="panel panel-default">
            <div class="panel-body">
    <div class="row">
        <center><h2 style=" font-family: Georgia;">IIR - Industry Relation</h2>
            <hr style="width:40%;">
            <br><img src="img/iic.jpg" alt="Committee Image"
                     style="width:500px; height:auto; margin:0 auto; box-shadow:0 5px 10px #333;"></hr></center>
        <h3 style="text-align:center;">ABOUT</h3>
        <hr style="width:40%;">
        <div class="container" style="text-align:center; font-family: Optima;"><h5 style="text-align:center">Address:
                IIC, Room No. 604, 6th Floor, Department of Computer Engineering</h5>
            <h4>IIC-Chair - IIC</h4>

            <h5>- Dr.D.R.Kalbande</h5>
            <br>
            <h4>Faculty Coordinator</h4>

            <h5>- Prof. Radha Shankarmani (Head of IT department)</h5>

            <br> <h4>Student Coordinator </h4>
            <h5> - Mr. Rushabh Shah ,TE Computer, Sem V</h5>
            <h5> - Mr. Jay Bhatt ,SE Computer, Sem III</h5>
            <h5> - Ms. Rheya Vithlani ,SE Computer, Sem III</h5>
            <h5> - Ms. Shivani Inamdar ,SE Computer, Sem III</h5>
            <h5> - Ms. Trisha Surve ,SE Computer, Sem III</h5>

            <br/>
            <br/>
            <h4>Objectives:</h4>
            <hr style="width:40%;">

            <ol>

                <h5>To enhance the employability of the Computer Engineering branch students.</h5>
                <h5>To guide and motivate the students for undertaking Industry oriented projects.</h5>
                <h5>To provide internship to students of Computer Engineering.</h5>
            </ol>
            </p>
        </div>


    </div>
	</div>
	</div>
<br>

<br>
<?php include 'includes/footer.php'; ?>

</body>
</html>