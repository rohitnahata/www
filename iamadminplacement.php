<?php

require_once '/includes/session.php';
require_once '/includes/database.php';
require_once '/includes/functions.php';


if(isset($_SESSION['aid'])){

    if (isset($_POST['submit'])) {
        // echo "{$_POST['numstupla']}";
        if (isset($_POST['numstuapp']) && isset($_POST['numstupla'])) {
            $sql = "select * FROM placements where year = '{$_POST['year']}'";
            $faculty_result = $database->query($sql);
            if ($numrows = $database->number_of_rows($faculty_result)) {
                $sql = "update placements set number_of_students_appeared = {$_POST['numstuapp']} , number_of_students_placed = {$_POST['numstupla']} where year = {$_POST['year']}";
                $result = $database->query($sql);
                $message = "Data Updated";


            } else {
                $sql = "insert into placements (number_of_students_appeared,number_of_students_placed,year) values ({$_POST['numstuapp']},{$_POST['numstupla']},{$_POST['year']})";
                $result = $database->query($sql);
                $message = "Data Inserted";
            }


        } else {
            $message = "Please Enter Valid Data";
        }
    }

}  else {
    redirect_to('index.php');
}

?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>Department of Computer Engineering | Faculty</title>

        <script src="js/jquery-1.10.2.min.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap_admin.css" rel="stylesheet">
        <link href="css/admin_template.css" rel="stylesheet">
        <link href="css/admin_faculty.css" rel="stylesheet">
        <link href="css/faculty_need.css" rel="stylesheet" type="text/css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

    <?php include('includes/control_header.php'); ?>

    <div class="container">


        <div class="row row-offcanvas row-offcanvas-right">

            <?php include('includes/admin_sidebar.php'); ?>

            <div class="col-xs-12 col-sm-9">
                <div class="jumbotron" id="addFaculty">    <!-- for adding a new faculty -->

                    <div class="error"><?php if (isset($message)) {
                            echo $message;
                            unset($message);
                        } ?></div>    <!-- print error here -->    <!-- print error here -->

                    <h3>Placement Update</h3>
                    <ul class="nav">
                        <form id="addFaculty" method="POST">
                            <input type="text" name="numstuapp" placeholder="Number of stundents appeared"
                                   class="form-control" required>
                            <input type="text" name="numstupla" placeholder="Number of stundents placed"
                                   class="form-control" required>
                            <select name="year" class="form-control">
                                <option>2014</option>
                                <option>2015</option>
                                <option>2016</option>
                                <option>2017</option>
                            </select>
                            <button type="submit" name="submit"
                                    class="btn btn-sm btn-primary btn-block faculty-account">Update
                            </button>
                        </form>
                    </ul>
                </div>    <!--/jumbotron for adding a new faculty -->

            </div><!--/span-->

        </div><!--/row-->

        <hr>

        <footer style="font-size:12px; text-align:center;">
            <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights
                Reserved.</p>
        </footer>

    </div><!--/.container-->




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/addmore.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/admin_faculty.js"></script>
    </body>
    </html>

<?php


?>



<?php $database -> close_connection();?>