<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title> Computer Society of India </title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/imageslider.css">
      <link rel="stylesheet" type="text/css" href="css/mystyle.css">
      <!-- Include scripts -->
      <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
      <script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>
      <!-- Include styles -->
      <link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>
      <!-- Include media queries -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
      <style>
         .hr1 {
         display: block;
         margin: 0.5em auto;
         border: 2px inset #FF4500;
         }
         .head {
         font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
         font-size: 2em;
         }
         .head_comm{
         font-family: Optima, Segoe, "Segoe UI", Candara, Calibri, Arial, sans-serif;
         font-size: 1.5em;
         }
         .caption
         {
         border: 1px solid #ddd;
         margin-bottom: 20px;
         }
         li.recentEv:before {
         content: "\f061";
         font-family: FontAwesome;
         display: inline-block;
         margin-left: -1.3em;
         width: 1.3em;
         }
         .csi_img
         {
         max-width: 20%;
         height: auto;
         display: block;
         margin: auto;
         padding-top: 40px;
         padding-bottom: 30px;
         }
         .description{
         padding-top: 20px;
         padding-left: 2em;
         padding-right: 2em;
         padding-bottom: 35px;
         }
         #recent_hr{
         height: 10px;
         border: 0;
         box-shadow: 0 10px 10px -10px #8c8b8b inset;
         }
         #events_hr
         {
         border: 0; 
         height: 1px; 
         background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
         background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); 
         }
      </style>
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
      <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
   </head>
   <body>
      <?php include 'responsiveHeader/header.html'; ?><br><br>
      <div class="row">
         <div class="container">
            <div class="col-sm-9 caption">
               <h3 class="head" align="center">COMPUTER SOCIETY OF INDIA</h3>
               <hr class="hr1">
               <p style="font-size: 1.2em; padding-top: 10px;" align="center"><strong>Professor in-charge: Prof. Sunil N Dhage</strong></p>
               <img src="img\committee\csi\CSI_logo.jpg" class="csi_img"></img>
               <h4 class="head_comm" align="center">What is CSI?</h4>
               <hr class="hr1" width="25%">
               <p class="description">The seed for the Computer Society of India (CSI) was first sown in the year 1965 
                  with a handful of IT enthusiasts who were a computer user group and felt the need to organize their activities. 
                  They also wanted to share their knowledge and exchange ideas on what they felt was a fast emerging sector. 
                  Today the CSI takes pride in being the largest and most professionally managed association of and for IT professionals in India.
                  The purposes of the Society are scientific and educational directed towards the advancement of the theory and 
                  practice of computer science & IT.
               </p>
               <h4 class="head_comm" align="center">So who are we?</h4>
               <hr class="hr1" width="25%">
               <p class="description">We are the C.S.I. Student Branch of Sardar Patel Institute of Technology. 
                  Formed in 2009, CSI-SPIT is an altruistic society in college initiating workshops, events and seminars
                  to explore the cornucopia of information other than the regular curriculum offered by the university. 
                  As part of our endeavour to bring together and assimilate various aspects of technical and non-technical education, 
                  number of seminars and workshops are conducted by professionals imparting knowledge to the students of the college.
                  We also encourage member students to organize events by themselves so that it imbibes in them the skills of management,
                  self-confidence and helps to exchange views and information, learn and share ideas. An annual 10 day industrial visit 
                  cum trip is organized to different parts of the country.This facilitates the students' interaction with the industries
                  in different parts of the country and gives them the valuable experience of witnessing the ground realities of the work
                  environment intertwined with the pleasures of visiting the most beautiful places in India. Every person on this planet 
                  is blessed with some or the other quality and is dextrous at something. We at CSI-SPIT aim at exploring the acme within
                  every individual so that they can realize their dreams in reality.
               </p>
               <h4 class="head_comm" align="center">CSI - Mumbai</h4>
               <hr class="hr1" width="25%">
               <p class="description">The Computer Society of India is a non-Profit professional meet to exchange views and information 
                  learn and share ideas. The wide spectrum of members is committed to the advancement of the theory and practice of Computer
                  Engineering and Technology Systems, Science and Engineering, Information Processing and related Arts and Sciences. 
                  The Society also encourages and assists professionals to maintain the integrity and competence of the profession and 
                  fosters a sense of partnership amongst members. Besides the activities held at the Chapter and Student Branches, the 
                  Society also conducts periodic conferences, seminars at Regional and Divisional levels.
               </p>
               <h4 class="head_comm" align="center">Committee 2016-17</h4>
               <hr class="hr1" width="25%">
               <div style="padding-top: 20px; padding-bottom: 50px;">
                  <img class="img-responsive" src="img\committee\csi\csi1617.jpg" id="committee_img" style="height: 50%; width: auto;display: block; margin: auto; 
                     box-shadow: 10px 10px 5px #ccc; 
                     -moz-box-shadow: 10px 10px 5px #ccc; -webkit-box-shadow: 10px 10px 5px #ccc; -khtml-box-shadow: 10px 10px 5px #ccc;"></img>
               </div>
            </div>
            <div class="col-sm-3 caption">
               <h4 class="head" align="center">Recent events</h4>
               <hr id="recent_hr" width="100%">
               <ul id="event">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <li class="recentEv">kefkjnejfn</li>
               <!--- Insert event --->
               <hr id="events_hr">
               <ul>
            </div>
         </div>
         <!-- /.row -->
      </div>
      <?php include 'includes/footer.php'; ?>
   </body>
</html>