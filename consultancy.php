<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Include scripts -->
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
        <script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>
        <!-- Include styles -->
        <link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>
        <!-- Include media queries -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <meta name="description" content="">
        <meta name="author" content="">
        <title> Consultancy </title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/imageslider.css">
        <link rel="stylesheet" type="text/css" href="css/mystyle.css">
        <style>
            .space-top-lg {
            padding-top: 30px;
            }
            .space-top-sm {padding-top: 20px;
            padding-left: 100px;
            }
            .space-top-md {
            padding-top: 30px;
            padding-left: 150px;
            padding-right: 50px;
            }
            .space-bottom-sm {padding-bottom: 20px;
            padding-left: 100px;
            }
            .space-bottom-md {padding-bottom: 30px
            padding-left: 100px;
            }
            .proj {
            background-color: #e5e5e5;
            border-radius: 10px;
            padding-left: 20px;
            }
            .projhead {
            font-family: Georgia, Times, "Times New Roman", serif;
            color: #000;
            }
            .projguide {
            color: #000;
            }
            a:hover {
            color: #000;
            }
            .h3{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;
            }
            .hr  {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
            }
        </style>
    </head>
    <body>
        <?php include 'responsiveHeader/header.html'; ?><br><br>
		<div class="panel panel-default">
        <div class="panel-body">
        <div class="container space-top-lg">

                    <div class = "h3">
                        <p align="center">Projects Undertaken</p>
                    </div>
                    <hr width="45%" class="hr">
                </div>
                <div class="container space-top-lg">
                    <div class="proj space-top-sm space-bottom-sm">
                        <h3 class="projhead">Traveller Website</h3>
                        <!-- Title of the project -->
                        <p style="padding-right: 10px">This work is started as a small initiative by Chaitanya (client) to create a
                            global community
                            of people who are willing to share their cultures and cuisines by sharing a meal with travellers. 
                            The goal is to make the world a better place where people respect and value each other’s cultures over a delicious meal.
                        </p>
                        <h5>Guide: Dr.Dhananjay Kalbande</h5>
                        <ul>
                            <li>Aakash Thakur</li>
                            <li>Ananya Jain</li>
                            <li>Harsh Chheda</li>
                            <li>Shreyas Zagade</li>
                            <li>Sunny Shah</li>
                        </ul>
                        <p><strong>Status : This consultancy Project has been completed on 10th June 2014</strong></p>
                    </div>
                </div>
                <div class="container space-top-lg">
                    <div class="proj space-top-sm space-bottom-sm">
                        <a href="#">
                            <h3 class="projhead">LMS</h3>
                        </a>
                        <!-- Title of the project -->
                        <p style="padding-right: 10px;">Learning Management System(LMS) for Organization.</p>
                        <h5>Guide: Dr.Dhananjay Kalbande</h5>
                        <ul>
                            <li>Aaswad Satpute</li>
                            <li>Sheetal Pandrekar</li>
                            <li>Sumit Gouthaman</li>
                            <li>Sagar Kamat</li>
                            <li>Ritika Nevatia</li>
                        </ul>
                        <p><strong>Status : This consultancy Project is in the process of completion</strong></p>
                    </div>
                </div>
                <div class="container space-top-lg space-bottom-md">
                    <div class="proj space-top-sm space-bottom-sm">
                        <a href="#">
                            <h3 class="projhead">Construction Website</h3>
                        </a>
                        <!-- Title of the project -->
                        <p style="padding-right: 10px">Construction services System(CSS) for Organization.</p>
                        <h5>Guide: Dr.Dhananjay Kalbande</h5>
                        <ul>
                            <li>Aaswad Satpute</li>
                            <li>Rishab Shah</li>
                            <li>Himani Choudhary</li>
                        </ul>
                        <p><strong>Status : This consultancy Project is started in June 2014</strong></p>
                    </div>
                </div>
				<br>
            </div>
        </div>

        <?php include 'includes/footer.php'; ?> 
    </body>
</html>