<?php
require_once 'includes/functions.php';
require_once 'includes/database.php';
require_once 'includes/session.php';

if (isset($_SESSION['aid'])) {
    if (isset($_POST['id']) && !empty($_POST['id'])) {
        $sql = "DELETE FROM faculty WHERE id={$_POST['id']}";
        $database->query($sql);
    }
} else {
    redirect_to("index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">
    <title>Department of Computer Engineering | Faculty</title>
    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include('includes/control_header.php'); ?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <?php include('includes/admin_sidebar.php'); ?>
        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="deleteFaculty">
                <!-- for deleting a faculty -->
                <div class="error">
                    <?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?>
                </div>
                <!-- print error here -->
                <!-- print error here -->
                <h3>Delete Faculty</h3>
                <ul class="nav">
                    <form action="iamadmindelete.php" method="POST">
                        <!-- serch -->
                        <select class="form-control" name="id">
                            <?php
                            $sql="SELECT * FROM faculty";
                            $result=$database->query($sql);
                            while($row=$database->fetch_array($result)){
                                ?>
                                <option value="
																						<?php echo "{$row['id']}"; ?>">
                                    <?php echo "{$row['fname']}"; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <input type="submit" class="btn btn-sm btn-primary btn-block faculty-account"
                               value="Delete Faculty" name="search" /input>
                    </form>
                </ul>
            </div>
            <!--/jumbotron for deleting a faculty -->
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <hr>
    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>
</div>
<!--/.container-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
<?php
$database->close_connection();
?>