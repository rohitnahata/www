-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2017 at 06:56 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `comps`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `id`) VALUES
('drkalbande', 'AC15225A00C22707FFB127ACB3E5CE3867BF152A00E6FB1EFEEF760320A5DE92C4E1EBBA629828187ECB2FF76B39AA62C7C763882C9A0D6FCFB83F3959C9FC5E', 1),
('admin', 'f68620a934473a2815b046d2a8a35cfbcc26ece46e69929fa7d666624580fdf87213cd7b2485e49f3076a31c66472f2aa1563f5a4f4c27e62e5a746a96a5c1ed', 2);

-- --------------------------------------------------------

--
-- Table structure for table `area_of_interest`
--

CREATE TABLE IF NOT EXISTS `area_of_interest` (
  `description` text NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_of_interest`
--

INSERT INTO `area_of_interest` (`description`, `faculty_id`) VALUES
('Soft computing (Neural Networks, Fuzzy Logic)', 1),
('Computer Network', 1),
('Human Machine Interaction', 1),
('Decision making and business Intelligence', 1),
('Mobile application development for social cause.', 1),
('Computer graphics', 2),
('Digital Signal Processing', 2),
('Image Processing', 2),
('Computer Vision', 2),
('Robotics', 2),
('Computer Programming', 3),
('Data Structures', 3),
('Analysis of Algorithm', 3),
('Web Engineering', 3),
('Operating System', 3),
('Data Base Management Systems', 4),
('Discrete Structure and Graph Theory', 4),
('Image Processing', 4),
('Object Oriented Software Engineering', 5),
('Data Structures', 5),
('Analysis of Algorithms', 5),
('Software Architecture', 5),
('Database Management Systems', 5),
('Computer Programing', 6),
('Computer Graphics', 6),
('Multimedia Systems', 6),
('System Programming', 6),
('Data Structures', 6),
('Compiler Construction', 6),
('Mobile Computing,OOPM,OS', 11),
('Mobile Computing', 3),
('System Security', 3),
('Android Based Applications', 3),
('ICT for semi rural development for social cause', 1),
('Performance Evaluation of Computer Systems and Networks', 8),
('Distributed and Parallel Computing', 8),
('Security', 8),
('Cloud Computing', 8),
('Distributed Systems and Algorithms', 9),
('Information Security', 9),
('Network Virtualization and Security', 9),
('Computer Network and Security', 9),
('Cloud Computing and Security', 9),
('Internet of Things and Security', 9),
('Middleware & Enterprise Technology', 13);

-- --------------------------------------------------------

--
-- Table structure for table `books_published`
--

CREATE TABLE IF NOT EXISTS `books_published` (
  `title` varchar(10000) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `committee`
--

CREATE TABLE IF NOT EXISTS `committee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 4;

--
-- Dumping data for table `committee`
--

INSERT INTO `committee` (`id`, `name`) VALUES
(0, 'FACE'),
(1, 'PI-Club'),
(2, 'E-Cell'),
(3, 'CSI');

-- --------------------------------------------------------

--
-- Table structure for table `committee_workshop`
--

CREATE TABLE IF NOT EXISTS `committee_workshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text,
  `date` date NOT NULL,
  `content` text,
  `committee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `committee_workshop`
--

INSERT INTO `committee_workshop` (`id`, `image`, `date`, `content`, `committee_id`) VALUES
(10, NULL, '0000-00-00', 'effwef+1', 0),
(11, NULL, '2012-12-01', '12+1', 0),
(12, NULL, '2012-12-12', 'vghjkl', 0),
(13, NULL, '0000-00-00', 'fyuio', 0),
(14, NULL, '0000-00-00', 'fyuio', 2),
(15, NULL, '0000-00-00', 'hjkl', 3),
(16, NULL, '0000-00-00', 'a', 3),
(17, NULL, '0000-00-00', 's', 1),
(18, NULL, '0000-00-00', 'egf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `conference`
--

CREATE TABLE IF NOT EXISTS `conference` (
  `title` varchar(10000) NOT NULL,
  `type` varchar(1) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conference`
--

INSERT INTO `conference` (`title`, `type`, `faculty_id`) VALUES
(' A Study of Different TCP/IP Mail Access Protocols, National conference on Emerging Trends in Engineering & Technology organized by Shree L.R.Tiwari College of Engineering, Mira Road .', 'N', 3),
('An Approach to Multimedia Conferencing on Android Technology, National Conference on Emerging Trends in Engineering & Technology (VNCET'' 12-30 Mar''12) Vol:-II..', 'N', 3),
('STUDY OF LOAD BALANCING SCHEMES OVER A VIDEO ON DEMAND SYSTEM, International conference Computer Engineering and Technology, 3rd (ICCET 2011)', 'I', 3),
('Improving Malware Detection module by using Machine Learning Algorithm, International Conference on Advanced in Computer and Communication Technology(ACCT 2012) organized by IETE Mumbai Centre.', 'I', 3),
  ('An aid for hearing impaired. Published in National conference NCBME 2006.', 'N', 6),
  ('Finger Print Recognition Using Image Processing, International Conference ICDIP 2011 Chengdu, China, April 2010.', 'I', 2),
  ('Intrusion Detection System Using Visualization and Neural network, National Conference on Infrastructure Management Systems,pp- 32-34, 17-18, Dec-2010 VJTI, Mumbai-2010', 'N', 8),
  ('Implementing AI Driven Simulation Game, National Conference On Automation, Communication And Computing (NCACC 2010), pp- 197-199, 9-10, Dec-2010, Visakhapatnam.', 'N', 8),
  ('Intrusion Detection systems, National Conference on trends in advance computing at Mumbai, Dec-2007.', 'N', 8),
  ('Network Intrusion Detection System using Data Mining Technique, IEEE International Conference, Vol. No.5 pp-98-103, Feb 2008.', 'I', 8),
  ('GC in DSM System, IEEE Second International Conference on Advanced Computing & Comm. Technologies for High Performance Applications, pp-278-285, 7-9 Dec 2010.', 'I', 8),
  ('Cosmic Movement Detection Using Image Processing, 2010 International Conference on Graphics and Image Processing, (ICGIP 2010), pp-213-217, 4-5, Dec-2010, Manila, Philippines.', 'I', 8),
  ('Intrusion Detection System in Distributed Computing Environment, ACM International Conference and Workshop on Emerging treads in Technology-2011 (ICWET 2011),25-26, Feb 2011.', 'I', 8),
  ('A Comparative Analysis of Ajax, Struts and Spring,2011 2nd International Conference on e-Education, e-Business, e-Management and E-Learning, Mumbai, (IC4E 2011) under International Association o', 'I', 8),
  ('Parameters affecting Search Engine Rankings and Tactics for Holistic Web Development using SEO Techniques, 2011 2nd International Conference on e-Education, e-Business, e-Management and E-Learni', 'I', 8),
  ('Fault Tolerance in Cluster Computing System, The 17 IEEE international conference on parallel and Distributed Systems accepted in September 2011.', 'I', 8),
('Identifying the research areas in the realm of "Information technology in Manufacturing at JMI University, Delhi in Jan 2006.', 'I', 1),
('Neuro-Analytical Hierarchy Process (NAHP)Approach for Selecting ERP Technologies in a Ongoing Project Settings Presented and Published in proceedings of 2010 3rd IEEE International Conference on Computer Science and Information Technology,Chengdu,CHINA', 'I', 1),
('Lip Reading Using Neural Networks", Published in IEEE2010 International Conference on Graphic and Image Processing (ICGIP December 2010) Manila, Philippines.', 'I', 1),
('Energy Conservation using Face Detection", Published in IEEE 2010 International Conference on Graphic and Image Processing (ICGIP December 2010) Manila, Philippines.', 'I', 1),
('Total Productive Maintenance Accelerating OEE in a Manufacturing Industry leveraging pervasive technologies. Published in the Proceedings of IEEEInternational conference on Manufacturing Science & Technology (ICMST 2010), Kuala Lumpur, Malaysia 26-28 Nov 2010.', 'I', 1),
('Zip it Up SMS: A Path breaking Technology Model in the field of Mobile Messaging Presented and Published in proceedings of 2010 3rd IEEE International Conference on Computer Science and Information Technology,Chengdu,CHINA', 'I', 1),
('Neural Network Based Empirical Model Development for E-Business (Mobile) Application", Presented and published in the Proceedings of International Conference & workshop on Emerging Trends and Technology(ICWET -ACM MUMBAI 25-26 Feb 2011) ,ACM International Digital Library, SIGART, USA.ISBN: ISBN:978-1-4503-0449-8,DOI:10.1145/1980022.1980353', 'I', 1),
('Algorithm to access Office Files on Mobile Phones", Presented and published in the Proceedings of International Conference & workshop on Emerging Trends and Technology(ICWET -ACM MUMBAI 25-26 Feb 2011) , and ACM ,New York NY(USA), ISBN :978-1-4503-0449,DOI :10.1145/1980022.1980112', 'I', 1),
('Neuro Analytical Hierarchy Process (NAHP) Approach for CAD/CAM/CIM Tool Selection in the Context of Small Manufacturing Industries,presented and published in IEEE-2012, International Conference ,ICCICT-2012,Mumbai,India. Print ISBN: 978-1-4577-2077-2 DOI : 10.1109/ICCICT.2012.6398139.', 'I', 1),
('Satellite Positioning Simulation System,Presented and published in IEEE-2012, International Conference ,ICCICT-2012,Mumbai,India.DOI: 10.1109/ICCICT.2012.6398141 .', 'I', 1),
('.Demonstration of Signature Analysis using Intelligent Systems published and presented at 3rd International Conference on advances in Computing, communication and control , with Springer 18-19 Jan 2013', 'I', 1),
('Emotion Detection from "The SMS of the Internet" Accepted for presentation in IEEE International Conference Dec 19-21 2013,,IEEE RAICS-2013,Trivandrum,Kerala,India', 'I', 1),
('Voice enabled Android application for vehicular complaint system: Using GPS and GSM-SMS technology,Published in IEEE sponsored International Conf. on Information and Communication Technologies (WICT), Trivendrum Oct 30, 2012 DOI: 10.1109/WICT.2012.6409133 Publication Year: 2012 , Page(s): 520 - 524', 'I', 1),
('Machine Learning Applied To Human Learning presented in IEEE Conference IEEE INDICON 2013,13-15 Jan 2013,IIT Bombay,Mumbai,India', 'I', 1),
('MIS for Knowledge Management in the organizationat Bannari Amman Inst.of Technology, 2005.', 'N', 1),
('Network Intrusion and detection Mechanismin the National Conference at Govt.College Engg., Aibad, 2005.', 'N', 1),
('A Novel Idea to speed the customer business in the context of Indian railwaysat K.J.Somaiya College, Mumbai, 2006.', 'N', 1),
('Systolic Arrays and Recursion : A new approach for matrix multiplicationaccepted for publication in National Conference on "Recent Advances in Computer Vision and Information Technology, Government College of Technology, Coimbatore, March 2010.', 'N', 1),
('"Extended Visual Cryptography for color image using coding table",presented and published in IEEE-2012, International Conference ,ICCICT-2012,Mumbai,India.  ,Issue Date :   19-20 Oct. 2012.', 'I', 2),
('Gesture detection system using smart watch based motion sensors Authors :Sumit G.,O.Karande,A.Pandya,Dr.D.R.Kalbande Presented and published in IEEE Conference (CSCITA Approved by IEEE), April 4-5 2014,Mumbai.', 'I', 1),
('Research on Educative Games for Autistic Children Authors: Sridari Iyer,Dr.D.R.Kalbande Presented and published in IEEE Conference (CSCITA Approved by IEEE), April 4-5 2014,Mumbai.', 'I', 1),
('" Review of Mobile Data Offloading through Wi-Fi "Authors: Deepshikha H.,Dr.D.R.Kalbande Presented and published in IEEE Conference (CSCITA Approved by IEEE), April 4-5 2014,Mumbai', 'I', 1),
('"Web service selection based on QoS using Feedforward Neural Network". Authors: Aarti Karande,Dr.D.R.Kalbande Published in International Conference ICICT-2014,IEEE Delhi Section,in KIET,Gaziabad', 'I', 1),
('"A Hybrid System for Object Detection and Trackinh Combining Color And Intensity". International Conference on Engineering Confluence, Equinox''14', 'I', 3),
('Mr. Sunil Ghane, Mrs. Sujata Kolhe, Facilitating Document Annotation for Efficient User  Relevant Search&quot; International Conference MECIT-2015, at Jawaharlal Nehru University, New  Delhi.', 'I', 13);

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `fname` text NOT NULL,
  `date` date NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(100) DEFAULT NULL,
  `qualification` text NOT NULL,
  `email` text,
  `telephone` int(100) DEFAULT NULL,
  `extension` int(50) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  `image_path` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  `visibility` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`fname`, `date`, `id`, `position`, `qualification`, `email`, `telephone`, `extension`, `room_number`, `image_path`, `priority`, `visibility`) VALUES
('Dr. Dhananjay Kalbande', '0000-00-00', 1, 'Professor and Head of Department      ', 'Ph.D.,M.E.(Info.Tech.),B.E.(Comp.)<br>Post-Doctoral Fellow student,TISS, Mumbai.', 'drkalbande@spit.ac.in', 26707440, 366, 604, 'img/drk1.png', 0, 1),
  ('Prof. Surekha Dholay', '0000-00-00', 2, 'Associate Professor          ', 'M.E. (Computer Science & Engineering) <br> B.E. (Computer Science & Engineering)', 'surekha_dholay@spit.ac.in ', 26707440, 377, 608, 'img/Surekha_Dholay-300x262.jpg', 4, 1),
('Prof. Nataasha Raul', '0000-00-00', 3, 'Assistant Professor     ', 'M.E. (Information Technology) ,B.Tech. (Information Technology)\r\nPursuing Phd in Computer Engineering', 'nataasharaul@spit.ac.in', 26707440, 374, 607, 'img/nr.jpg', 7, 1),
('Prof. Kiran Gawande', '0000-00-00', 4, 'Assistant Professor', 'M.E. (Computer Science & Engineering)<br/> B.E. (Computer Science & Engineering)', 'kiran_gawande@spit.ac.in', 26707440, 374, 607, 'img/kiran_gawande.jpg', 5, 1),
('Prof. Reeta Koshy', '0000-00-00', 5, 'Assistant Professor       ', 'B.E. (Electronics and Communication)', 'reeta_koshy@spit.ac.in', 26707440, 374, 607, 'img/reeta-koshy.jpg', 8, 1),
('Prof. Anand Godbole', '0000-00-00', 6, 'Associate Professor     ', 'M.E. (Electronics) <br> B.E. (Computer)', 'anand_godbole@spit.ac.in ', 26707440, 373, 602, 'img/Anand_Godbole-300x260.jpg', 3, 1),
('Dr. Sudhir Dhage', '0000-00-00', 8, 'Professor   ', 'Ph.D.(Technology) in Computer Engineering <br/>M.E.(Computer Engineering), B.E.(Computer Engineering) ', 'sudhir_dhage@spit.ac.in', 26707440, 368, 602, 'img/S_Dhage-300x250.jpg', 1, 1),
('Dr. Anant Nimkar', '0000-00-00', 9, 'Associate Professor  ', 'Ph.D (Computer Science & Engg.), IIT Kharagpur <br>M.Tech.(I.T.) IIT Kharagpur, B.E.(CSE)', 'anantnimkar@spit.ac.in', 26707440, 369, 602, 'img/anant-nimkar.png', 2, 1),
('Dr.(Mrs.) Prachi Gharpure', '0000-00-00', 10, NULL, '', NULL, NULL, NULL, NULL, '', 9, 0),
('Prof.Soni Bhambar', '0000-00-00', 11, 'Asst.Professor', 'ME Computer', '', 22, 371, 607, 'img/Bhambar_s.jpg', 10, 1),
('Prof. Abhijeet Salunke', '0000-00-00', 12, 'Asst. Professor', 'M.E.(Comps),B.E.(Comps)', 'abhijeet_salunke@spit.ac.in', NULL, 371, 607, 'img/abhijeet_s.jpg', 11, 1),
('Prof. Sunil B. Ghane', '0000-00-00', 13, 'Assistant Professor ', 'M.E. Computer, B.E. I.T.', 'sunil_ghane@spit.ac.in', 26287250, 371, 607, 'img/2.jpg', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `iic_company`
--

CREATE TABLE IF NOT EXISTS `iic_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10000) NOT NULL,
  `logo` varchar(10000) NOT NULL,
  `url` varchar(10000) NOT NULL,
  `description` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 15;

--
-- Dumping data for table `iic_company`
--

INSERT INTO `iic_company` (`id`, `name`, `logo`, `url`, `description`) VALUES
(1, 'Vucentric Technologies', 'http://comp.spit.ac.in/images/vucentric_tech.jpg', 'www.vucentric.com', 'Android Application Development\nAuthority : Bharat Jain\nDate of Affiliation : 29th November 2013'),
(2, 'NetDoers Technologies', 'http://comp.spit.ac.in/images/netdoers.png', 'http://www.netdoers.com/', 'Android Application\r\nFounder : Dhaval Shah\r\nCo-Founder & CTO : Rajan Rawal\r\nDate of Affiliation : 5th December 2013'),
(3, 'Bizdom Learning Technologies Pvt. Ltd.', '', '', 'Bizdom Learning Technologies,is a learning company which is a joint venture to bring together the learning content expertise of communication consultant Anita Bhogle with the technical capability of Team Deltecs, a group of technopreneurs.'),
(4, 'NetTech India', 'http://comp.spit.ac.in/images/netTechIndia.jpg', 'http://www.nettechindia.com/', 'NetTech India offers a quality learning experience in the areas of IT training.NetTech India focus is on providing advanced training and certifications in complex networking technologies.'),
(5, 'Last-Bench Applied Education Pvt. Ltd.', '', 'http://last-bench.com/', 'Web Application Development'),
(6, 'BluBears Foundation', '', '', 'Web Application Development'),
(7, 'Gameeon Infotech Pvt Ltd', 'http://comp.spit.ac.in/images/game_eon.png', 'http://www.gameeon.in/', 'Android Game Development\r\nMr. Taral Patel'),
(8, 'Zoopky', 'http://comp.spit.ac.in/images/zoopky.jpg', 'http://www.zoopky.com/', 'You have an event that needs a high-quality artist. We have a network of handpicked, vetted artists who are ready to entertain. We match you up and then we make sure the process goes smoothly on both ends.\r\n\r\nThe best applicants. No agents. No worries.'),
  (9, 'D-Link Academy', 'http://comp.spit.ac.in/images/d_link.jpg', 'http://academy.dlink.com/',
   'Network Training Academy\r\nMr. Sudhanshu Ojha\r\nG.M SAARC\r\n\r\nDate of Affiliation : 18th September 2014'),
  (14, 'ty78', '', 'tyui', 'tyuio');

-- --------------------------------------------------------

--
-- Table structure for table `iic_students_selected`
--

CREATE TABLE IF NOT EXISTS `iic_students_selected` (
  `name` text NOT NULL,
  `year` int(4) NOT NULL,
  `internship_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`internship_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `iic_students_selected`
--

INSERT INTO `iic_students_selected` (`name`, `year`, `internship_id`) VALUES
('rohitnahata', 2, 4),
('crftgyhuj', 2, 5),
('rohitnahata', 2, 6),
('ewf', 2, 7),
('wef', 2, 8),
('dd', 2, 9),
('rgfg', 2, 10),
('regfrg', 2, 11),
('klmn', 2, 12),
('qqqq', 2, 13),
('axaxa', 2, 14),
('xdd', 2, 15),
('axaxaxa', 2, 16);

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE IF NOT EXISTS `internship` (
  `id` int(11) NOT NULL,
  `job_title` varchar(10000) NOT NULL,
  `company_id` int(10) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `student_selected` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`id`, `job_title`, `company_id`, `description`, `student_selected`) VALUES
(1, 'HMMM', 0, 'add', 'azxxcc'),
(2, 'ABCDDD', 1, 'ZZZZZZ', 'azxxcc'),
(3, 'eddddd', 2, 'asasaxx', 'azxxcc'),
(4, 'awssd', 1, 'qasdfgg', ''),
(5, 'awssd', 1, 'qasdfgg', ''),
(6, 'awssd', 1, 'qasdfgg', ''),
(7, 'awssd', 1, 'qasdfgg', ''),
(8, 'awssd', 1, 'qasdfgg', ''),
(9, 'awssd', 1, 'qasdfgg', ''),
(10, 'awssd', 1, 'qasdfgg', ''),
(11, 'awssd', 1, 'qasdfgg', ''),
(12, 'awssd', 1, 'qasdfgg', ''),
(13, 'WAA', 1, 'zcxvvv', ''),
(14, 'vb', 1, 'hhhh', ''),
(15, 'azz', 1, 'aaaaa', ''),
(16, 'aaddda', 1, 'vvvvv', '');

-- --------------------------------------------------------

--
-- Table structure for table `latest_announcements`
--

CREATE TABLE IF NOT EXISTS `latest_announcements` (
  `image` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `latest_announcements`
--

INSERT INTO `latest_announcements` (`image`, `timestamp`, `content`, `id`) VALUES
('img/banner03.jpg', '2013-12-19 12:28:37', '', 1),
('img/banner02.jpg', '2013-12-19 12:29:42', '', 2),
('img/banner01.jpg', '2013-12-19 12:35:06', '', 3),
('', '2013-12-19 13:07:00', 'Timetable for the Next Semester (Jan 2014 to Apr. 2014) is uploaded.', 4),
(NULL, '2014-01-13 07:00:23', 'Lecture by Mr.Alark Joshi of University of San Francisco for BE students on 15th Jan.2014.at 10.30 am in Room No.601/603.', 6),
(NULL, '2014-01-13 09:37:18', 'INTERNSHIP: SE Computers and TE Computers students can submit the resume to \r\nSE COMPUTER COORDINATOR:Bhairavi and Rishab\r\nTE COMPUTER COORDINATOR:Raj,Ritika and Suhani\r\nLAST DATE : 20th Jan 2014.\r\n', 7),
(NULL, '2014-01-21 12:27:57', 'LastBench.com and Netdoers :are coming to hire SECOMPS and TECOMPS students for Internship in the first or second week of February 2014.', 8),
(NULL, '2014-01-27 11:46:33', 'Internship Opportunity : Blubears Foundation is taking interviews on 28th Jan in IIC-Cell at 5.00 pm onwards.', 9),
(NULL, '2014-01-29 11:27:52', 'Internship Opportunity :Netdoers Technologies  is taking interviews on 31st Jan in IIC-Cell at 5.00 pm onwards. ', 10),
(NULL, '2014-04-01 13:12:00', 'Last-Bench Applied Education Pvt. Ltd.has selected the following students for Internship.\r\n\r\nCongratulations !!!!!\r\nMr.Shubham Jain  and Ms.Gayatri S.', 11),
(NULL, '2014-06-19 12:01:17', 'Congratulations M.E. Comps Sem II students !!!!!\r\nMr.Suchit Dhakate and Ms.Pooja Inamdar\r\n\r\nfor securing Internship at VMWare and INTEL Bangalore.', 12),
(NULL, '2014-06-20 06:57:10', '!!!New Administrative Team for Dept.website 2014-15 !!!\r\nMr.Aditya Rathi(Team Leader),Mr.Pratik Panjwani,Mr.Nikhil Hirve,Mr.Vatsal Vora,Ms.Tanvi C.', 13),
(NULL, '2014-06-20 07:02:44', '!!!Consultancy work:The following students are appointed to do consultancy work taken by the department since from 20/06/2014.\r\nMr.Rushabh Shah and Ms.Himani Choudhary!!!', 14),
(NULL, '2014-07-01 07:03:47', '!!!Welcomes all the new Faculty joining the Computer Engg.Dept.\r\n1.Mrs.Ashwini Galande\r\n2.Mrs.Harshada Sarode\r\n3.Mrs.Soni Bhambar\r\n4.Ms.Amber Hayat', 15),
(NULL, '2014-07-24 12:29:26', 'INTERNSHIPS!!!!!\r\nInterviews by Gameeon Software Pvt. Ltd. is scheduled on 24th July 2014 in Room No.604,IIC-cell,Computer Engg.Dept.', 16),
(NULL, '2014-07-28 11:36:13', '!!! Congratulations to the following students for securing internship at GAMEEON: Soumil Rao (TE Comps), Nishit Kagalwala (TE Comps), Abhinandan Deshpande (TE Comps) and Varun Joshi(SE Comps) !!! ', 17),
(NULL, '2014-08-08 14:18:06', 'M.E. lectures have already started from Tuesday 5th Oct 2014.', 18),
(NULL, '2014-08-11 11:54:05', 'All the SE Comps students who are interested to become IIC- Cell Student coordinators are requested to give their names to Mr.Rushabh of TE Comps by tomorrow upto 2:00 pm.\r\nThe interested students will be interviewed for selection.', 19),
(NULL, '2014-08-14 13:04:35', 'Congratulations to Apekshit Jotwani, Shraddha Sangtani, Abbas Cyclewala, Hinal Desai from T.E. Computers for securing internship in  Microsoft.', 20),
(NULL, '2014-08-14 13:06:24', 'Congratulations Mr. Sumit Gouthaman for declaring as the  Best Student of department for the year 2013-14', 21),
(NULL, '2014-08-14 13:08:28', 'Industry Institute Interaction Cell(IIC) interviews for student co-ordinators to be conducted tomorrow at 1:00 pm in Room No.604 ,IIC Cell.', 22),
(NULL, '2014-08-27 12:40:29', 'CONGRATULATIONS!!!!!!! TO BE COMP STUDENTS SELECTED FOR  IVP(INDUS VALLY PARTNERS)SPONSORED PROJECT:\r\nGroup 1:\r\nSuhani Singhal,\r\nRajvi Mehta,and \r\nPrateek Bonde\r\n\r\nGroup 2:\r\nSiddhant Doshi\r\nAayushi Acharya and\r\nKevin Desai\r\n  ', 24),
(NULL, '2014-09-16 11:31:48', 'ME Comps Term Test 1 Exam Timetable. Copy paste below link:\r\ncomp.spit.ac.in/pdf/ME_Test_timetable.pdf', 25),
(NULL, '2014-12-23 06:43:08', 'Congratulations to all who won prizes in Hackathon Pune and Hyderabad. \r\n\r\n*** Ritika Nevatiya For winning Ipod for the Best Female Participantat in Barclays Hackathon held in Pune.***\r\n\r\n***Anish,Soham and Ritika For Standing 1st In Hackathon Competition organized by Amazon in Hyderabad on 21st and 22nd December 2015.***', 26),
(NULL, '2014-12-24 06:53:11', 'Alumni Day Meet on Thursday, 25th December 2014 at 10:00 am in the SPJIMR auditorium followed by lunch at 12:30 pm.', 27),
(NULL, '2014-12-30 09:08:31', '****Time table for the next academic semester is uploaded.The classes will start from 5th January 2015.****\r\n', 28),
(NULL, '2015-02-01 14:17:57', '** Congratulations to all those selected for the Barclays Internship - Priyanka Adsul, Deep Sidhpura, Soumil Rao, Apoorva Deo, Aastha Jhunjhanuwala, Vatsal Vora, Tanvi Chandna, Kartik Killawala, Rushabh Shah, Aditya Rathi !! **', 29),
(NULL, '2015-07-08 18:30:00', '"2 days workshop on SOFT COMPUTING (NEURAL NETWORK & FUZZY LOGIC) AND ITS PRACTICES on 10th and 11th July 2015 at S.P.I.T."', 30),
(NULL, '2015-08-26 10:20:05', 'Term Test T-1 Time Table (Odd Sem) and for Direct Second Year/Branch Change Students 2015-16.', 31),
(NULL, '2015-10-12 07:31:19', 'CONGRATULATIONS TECOMPS  \r\nFor Standing 1st and 3rd  In Microsoft Hackathon Competition organized in Sardar Patel Institute of Technology on 10th  and 11th Oct. 2015 .\r\n1st Rank:  Varun Joshi,\r\n                Neelesh Jayaraman\r\n                Pratik Velhal\r\n                Ganesh Baleri\r\n3rd Rank: Varsha Shetty  \r\n                 Jaanvi Jatakia\r\n                 Chaitanya Bapat\r\n                 Prajwal Shimpi                    \r\n                 Anand Desai\r\n                  Madhuri Dhodi\r\n                  Nemil Shah\r\n                  Rohit Naik\r\n', 32),
(NULL, '2015-10-13 13:31:47', 'Term Test T-2 Time Table \r\n', 33),
(NULL, '2015-12-11 12:09:51', 'Hearty Congratulations,\r\nRohit Naik (TE COMPS) for his selection as a Microsoft Student Partner (MSP) for India for 2015-2016.', 34),
(NULL, '2015-12-11 12:14:23', 'Hearty Congratulations FE Students !!!\r\nAashish Nehete (FEIT)and Srijal Poojari(FEETRX)\r\nFor securing 1st and 2nd rank in CODECHAMPs competition conducted by IIC Cell and Computer Engineering Dept powered by WorkIndia.\r\nCongrats For their selection as Paid Interns in WorkIndia.', 35),
(NULL, '2016-01-04 17:50:11', 'Orientation program for the course ''Human Machine Interaction'' is scheduled on 6th Jan.2016 in Room No.008 at 10 am to 3 pm. ', 36),
(NULL, '2016-06-13 14:10:01', 'Congratulations to all Thirty Three (33) TE computers for securing Internships ', 37),
('img/Adroid_Workshop_Brochure_2016.pdf', '2016-08-19 02:25:38', 'Two days workshop on Android Application Development on 24th & 25th September 2016', 38),
('', '2017-01-28 09:38:36', 'Sfafs', 39),
('', '2017-01-28 20:28:24', '3e3e32r23r', 41);

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE IF NOT EXISTS `others` (
  `faculty_id` int(11) NOT NULL,
  `description` varchar(10000) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `others`
--

INSERT INTO `others` (`faculty_id`, `description`) VALUES
(8, 'Received Research grant from Mumbai University for Cloud security Project in Academic year 2011-2012'),
  (8, 'Institute Selected to receive the prestigious Best CSI Student Branch Award in Nov., 2011. (Founder)'),
(8, 'Student Counselor of CSI Mumbai Chapter.'),
(8, 'Coordinator of CSI-SPIT Student Branch.'),
  (8, 'Conducted Orientation Programme for the course Theoretical Computer Science held on 07/07/2009'),
  (8, 'Conducted one day Workshop on Distributed Computing organized by TIET on 09/02/2009.'),
(8, 'Coordinator of University syllabus revision committee appointed by Board Of studies in Computer Engi'),
(8, 'Coordinator of IGNOU Programme Study centre 1628P for the Programme MCA, BCA and CIC for 3 Years.'),
(8, 'Handled the responsibility of Head of Department (Computer and Information Technology) at KCE and AC'),
(8, 'Mumbai University PG recognized teacher in Computer Engineering and EXTC.'),
(8, 'Handled the responsibility of Examination In-charge for two years.'),
(8, 'Single handed done the new courses Proposal (MCA and M.E. EXTC) in academic year 2008-09.'),
(1, '  Worked as an Expert to evaluate the Result Processing Software for MSBTE, Mumbai, India.'),
(1, 'Worked as a Technical Expert in the Scrutiny Work assigned by the AICTE, New Delhi, India.'),
(1, 'Worked as a Convener and Member of Syllabus Revision Committee of Mumbai University.'),
(1, 'Worked as a Project Incharge for consultancy work received from Lunchex Online Pvt.Ltd and successfully delivered the product to Lunchex Online Pvt.Ltd in June 2014.'),
(1, 'Develop the Industry Institute Interaction Cell (IIC-Cell) in Department of Computer Engineering to provide the Internship and motivate the students for undertaking Industry oriented projects.Currently working a Chair of IIC-Cell in S.P.I.T.'),
(1, 'Working as a Chair of Entrepreneurship Cell(E-Cell) of S.P.I.T.'),
(3, 'Certified as "DCT (D-Link Certified Trainer) in Network Switching by D-Link'),
(1, 'Working as a Member (Advisory Board), of IEDC,S.P.I.T.'),
(1, 'Currently Working as a Member Adhoc Board of Studies Computer Engineering, University of Mumbai, Mumbai.'),
(1, 'Currently Working as a Chairman-Ad-hoc Board of Studies in Computer Application.(M.C.A.),University of Mumbai,Mumbai.from 5th July 2016.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `other_workshop`
--

CREATE TABLE IF NOT EXISTS `other_workshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text,
  `date` date NOT NULL,
  `content` text,
  `committee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `other_workshop`
--

INSERT INTO `other_workshop` (`id`, `image`, `date`, `content`, `committee_id`) VALUES
(10, NULL, '0000-00-00', 'effwef+1', 0),
(11, NULL, '2012-12-01', '12+1', 0),
(12, '', '0000-00-00', '23r32ee13e', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pg_projects`
--

CREATE TABLE IF NOT EXISTS `pg_projects` (
  `faculty_id` int(255) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `special_topic` varchar(10000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `year` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pg_projects_ibfk_1` (`faculty_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `pg_projects`
--

INSERT INTO `pg_projects` (`faculty_id`, `title`, `special_topic`, `name`, `id`, `year`) VALUES
(30, 'Emotion detection using facial expressions', 'Covert channels and countermeasures in  computer network protocols', '', 1, 2012),
(8, 'Intrusion Detection System in Wireless Sensor Network.', 'Forensic and Antiforensic Analysis in Cyber Security ', 'Badgujar Vishal Sahebrao   Ratna', 2, 2012),
(10, 'Performace Modelling using petrinets for various web  applications', 'Zettabyte File System (ZFS)', 'Gaur Nidhi Satnarain Raj', 4, 2012),
(1, 'Interfaces for mentally disabled children', 'DNA computing & its applications', 'Iyer Sridari Tulsiram  Vedanayaki', 5, 2012),
(34, 'Mitigating Distributed Denial Of Service Attack In Cloud Environment', 'Increasing productivity and solving data warehousing problem using HIVE', 'Deshmukh Rashmi Vinod   Sunanda', 3, 2012),
(6, 'An Intelligent Travelling Companion for Visually Impaired Pedestrian', 'An Adaptive E-learning Recommendor Based on Learners Behavior', 'Joseph Richard Frederick   Rita', 6, 2012),
(34, 'Identification of Potential Evidence in Cloud using Forensics for Cyber Crime', 'Hadoop Usage for Better Performance in Big Data Processing', 'Kamble Karan Madhukar   Anita', 7, 2012),
(1, 'Twitter based ad-hoc search engine (Twitilyzer)', 'Business intelligence tools for small and  medium size enterprises and their  comparative study', 'Kanakia Harshil Tarun  Panna', 8, 2012),
(10, 'Authoring Tool based on Learning Object Standards', 'Computer-aided Diagnosis', 'Kharat Prashant Prakash  Jaya', 9, 2012),
(32, 'Remediation for Data Loss in V center data collection  by VMWare Chargeback Manager ', 'NoSQL Database Case Study and its implementation on Mobile Devices for Faster and Better Response', 'Kohli Vaibhav Ashok  Kamni', 10, 2012),
(31, 'BYOD-secure integration of mobile device into company network infrastructure', 'Task allocation in distributed operating system', 'Mestry Suresh Ramesh  Sumitra', 11, 2012),
(2, 'Depth Consistent Composition in stereoscopic images', 'Network database security using different  techniques: A survey ', 'Naik Sunita Vinay Neelam', 12, 2012),
(30, 'Suspicious Movement Detection and Tracking using  Video Analytics', 'Exam Timetable Scheduling using Genetic  Algorithms', 'Patil Sandesh Vilas Megha', 13, 2012),
(33, 'Adaptive model for distributed inventory allocation', 'In Memory Computing', 'Rath Minati Radha Krishna  Sumitra', 14, 2012),
(33, 'Web Logs Predictive Analysis for Finding Brand Status', 'Agile Refactoring for Code Maintenance', 'Raut Aditi Bharat Kalpana', 15, 2012),
(2, 'Image Compression Analysis Using Efficient  Transforms', 'Remote Sensing Technology''s Research in  Hyperspectral Imaging', 'Raut Vinit Dilip Deepali', 16, 2012),
(31, 'Automated Dental Implantation', ' Fuzzy Logic for Embedded Systems', 'Rodrigues Brinzel Benjamin  Philomina', 17, 2012),
(32, 'SAEER:Security Aware Energy Efficient Routing Protocol For Wireless Sensor Network', 'Efficiently Tracking The Changing World', 'Salunke Abhijeet Vijay  Sneha', 18, 2012),
(10, 'Expert System For Diagnosis Of Neurological Disorder', '', 'Amrita Mathur', 19, 2011),
(1, 'Integration Of Mobile App. With Open Source Cloud  Services', '', 'Mahendra Mehra', 20, 2011),
(1, 'Risk Analysis For Agile Software- Development', '', 'Renuka Sahebrao Pawar', 22, 2011),
(2, 'Automated Text Summarizer', '', 'Pai Anusha Nitin', 23, 2011),
(10, 'Smart Search For Web', '', 'Amruta Rohit  Sankhe', 24, 2011),
(10, 'On Demand Information Retrieval', '', 'Chandsarkar Rupali Sachin', 25, 2011),
(1, 'Neuro-Genetic Based ECG Classification', '', 'Abhiruchi Saraf', 26, 2011),
(8, 'Performance Analysis Of Cloud Computing', '', 'Akshay Rele', 27, 2011),
(2, 'Machine Learning Model For Sign Language Classification  Using Web Cam Images', '', 'Kanchan Inas  Dabre', 28, 2011),
(1, 'Machine Learning Model For Efficient Product   Categorization', '', 'Khan Sumaiya  Mohammad Ismail', 29, 2011),
(10, 'An Efficient Approach For Regression Testing In Software   Life Cycle', '', 'Ahlam Shakeel Ahmed Ansari', 30, 2011),
(10, 'Dynamic Web Service Composition', '', 'Sharma Ruchi Kaushal', 31, 2011),
(10, 'Botnet Detection Using Neural Network', '', 'Mane Yogita Deepak', 32, 2011),
(8, 'Performance Model Of Interactive Video On Demand   System', '', 'Patil Smita Krishna', 33, 2011),
(6, '3D Motion Interpolation Using Quaternions', '', 'Umang Mansukhlal Gajera', 34, 2011),
(6, 'Reverse Engineering Of Analytic Geometry', '', 'Prathamesh  Rajendra Gharge', 35, 2011),
(6, 'Minkowski Sums Of 2D/3D Breps.', '', 'Siddharth Thaker', 36, 2011);

-- --------------------------------------------------------

--
-- Table structure for table `pg_student`
--

CREATE TABLE IF NOT EXISTS `pg_student` (
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `project_id` int(255) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `pg_student_ibfk_1` (`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `pg_student`
--

INSERT INTO `pg_student` (`first_name`, `last_name`, `project_id`, `id`) VALUES
('Alphonso Alvina Andrew  Anne', '', 1, 1),
('Badgujar Vishal Sahebrao   Ratna', '', 2, 2),
('Gaur Nidhi Satnarain Raj', '', 4, 4),
('Iyer Sridari Tulsiram  Vedanayaki', '', 5, 5),
('Deshmukh Rashmi Vinod   Sunanda', '', 3, 3),
('Joseph Richard Frederick   Rita', '', 6, 6),
('Kamble Karan Madhukar   Anita', '', 7, 7),
('Kanakia Harshil Tarun   Panna', '', 8, 8),
('Kharat Prashant Prakash  Jaya', '', 9, 9),
('Kohli Vaibhav Ashok  Kamni', '', 10, 10),
('Mestry Suresh Ramesh   Sumitra', '', 11, 11),
('Naik Sunita Vinay Neelam', '', 12, 12),
('Patil Sandesh Vilas Megha', '', 13, 13),
('Rath Minati Radha Krishna  Sumitra', '', 14, 14),
('Raut Aditi Bharat Kalpana', '', 15, 15),
('Raut Vinit Dilip Deepali', '', 16, 16),
('Rodrigues Brinzel Benjamin  Philomina', '', 17, 17),
('Salunke Abhijeet Vijay   Sneha', '', 18, 18);

-- --------------------------------------------------------

--
-- Table structure for table `placements`
--

CREATE TABLE IF NOT EXISTS `placements` (
  `number_of_students_appeared` int(11) NOT NULL,
  `number_of_students_placed` int(11) NOT NULL,
  `dream_placed` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `placements`
--

INSERT INTO `placements` (`number_of_students_appeared`, `number_of_students_placed`, `dream_placed`, `year`, `id`) VALUES
(67, 57, 25, 2009, 1),
(63, 53, 33, 2010, 2),
(67, 64, 32, 2011, 3),
(70, 64, 23, 2012, 4),
(73, 62, 32, 2013, 5),
(78, 59, 27, 2014, 6),
(79, 65, 29, 2015, 7);

-- --------------------------------------------------------

--
-- Table structure for table `professional_affiliation`
--

CREATE TABLE IF NOT EXISTS `professional_affiliation` (
  `description` text NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_affiliation`
--

INSERT INTO `professional_affiliation` (`description`, `faculty_id`) VALUES
('Recognized PG Teacher & Ph.D.Guide of Mumbai University in Computer Engineering and Information Technology. ', 1),
('Modelling software solutions leveraging soft computing techniques to improve business process agility.', 1),
('Seamless Integration of applications on Mobile Cloud using Hybrid Approach.', 1),
('Recognized PG Teacher of Mumbai University in Computer Engineering.', 3),
('Fast track training attended at Mikado Solutions, which is an IT staffing, training and consulting  company dealing with several MNCs, on Java, Advanced Java, J2EE', 13);

-- --------------------------------------------------------

--
-- Table structure for table `prof_seminar_workshop`
--

CREATE TABLE IF NOT EXISTS `prof_seminar_workshop` (
  `description` varchar(10000) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `prof_seminar_workshop_ibfk_1` (`faculty_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prof_seminar_workshop`
--

INSERT INTO `prof_seminar_workshop` (`description`, `faculty_id`) VALUES
(' Conducted one month training program on Office Automation in Bharati Vidyapeeth Institute of Technology, Navi Mumbai', 1),
('Conducted two days short term course on Data Mining & Warehousing in Sardar Patel college of Engineering, Mumbai.', 1),
('Coordinated & conducted the two-week Application development certificate programme (STTP) on VB.Net & ADO.Net from 22nd June to 4th July.2009.', 1),
('Coordinated & conducted the 8-days Application development certificate programme (STTP) on VB.Net & ADO.Net from 22nd Aug to 1st Sept.2009.', 1),
('Coordinated 1-day workshop on software testing on 12th march 2010', 1),
('Coordinated & Conducted the 2-days Application development certificate programme (STTP) on VB.Net & ADO.Net from 6th to 7th march 2010.', 1),
('Conducted a session on PC Assembly organized by CSI S.P.I.T. Chapter on 14th March 2010', 1),
('Coordinated and conducted the 2-days workshop on Neural Network & Fuzzy Logic (NNFL) from 27th to 28th March 2010.', 1),
('Coordinated and conducted one week STTP on VB. Net & ADO .Net from 7th to 15th Aug 2010.', 1),
('Conducted a session on Network Simulator 2.0 at ISTE approved workshop in THAKUR ENGG.COLLEGE, Kandivali,Mumbai.', 1),
('Conducted seminar on Designing Neural Network using Java Object Oriented Neural Engine (JOONE) and Neural Easy Tool for Pattern Recognition on 24th February 2010 under the national state level seminar on Artificial Intelligence and its Application organized by Modern College of Engineering with University of Pune under QIP.', 1),
('Coordinated & Conducted the 4-days Application development certificate programme (STTP) on VB.Net & ADO.Net from 7th Aug to 11th Aug 2012', 1),
('Coordinated 3-days Application development certificate programme (STTP) on Ruby on Rails from 15th Sept to 17th Sept 2012.', 1),
('Conducted and trainer for the two-days workshop on Neural network & fuzzy logic from 20th & 21st March.2009.', 1),
('Conducted Workshop on Network Simulator at Thakur College of Engineering And Technology, Mumbai.', 1),
('Coordinated & conducted 2-days workshop on Human Computer Interaction from 16th and 17th Feb. 2013.', 1),
('Coordinated and conducted  the 2-days workshop on Soft Computing and Neuro Morphic Engineering  from 7th and 8th  Feb. 2014. ', 1),
('Conducted laboratory session during One-Week Graded ISTE-STTP Course on "Object-Oriented Programming Methodology with Lab Excercises" approved by ISTE during 8th July 2013 to 12 July 2013 at Sardar Patel Institute of Technology, Mumbai.', 3),
('Coordinated and conducted the 2-days workshop on Soft Computing ( Neural Network & Fuzzy Logic) & its practices on 10th and 11th July 2015.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `topic` text NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `year` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `faculty_id` (`faculty_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 72;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`topic`, `faculty_id`, `content`, `year`, `id`) VALUES
('Sentiment Analysis', 1, ' "Reason creates science; sentiments and creeds shape history"<br>\r\n- Gustave Le Bon<br>\r\nSuch is the significance of sentiments! SentilyzeR performs Twitter sentiment analysis for current events, companies, products and people thus, leading a way to shape history. Sentiment analysis is the application of natural language processing, computational linguistics, and text analytics to identify and extract subjective information (based on emotion as opposed to reason) in source materials (unstructured texts). The sentiment found within comments, feedback or critiques provide useful indicators for many different purposes. These sentiments can be categorised either into two categories: positive and negative; or into an n-point scale, e.g., very good, good, satisfactory, bad, very bad. In this respect, a sentiment analysis task can be interpreted as a classification task where each category represents a sentiment. With the growing availability and popularity of opinion-rich resources such as online review sites and personal blogs, new opportunities and challenges arise as people now can, and do, actively use information technologies to seek out and understand the opinions of others. The sudden eruption of activity in the social networking domain has drawn the attention of analysts, social media as well as general public to area of sentiment analysis to extract invaluable information from public opinion. Twitter is a pioneer in the social networking domain. It is a popular online micro-blogging service launched in 2006. Twitter has specifically 517 million accounts as of July 1, 2012, positioning it as the second-biggest social networking site. Almost 300 million new tweets are generated every day (Paris-based analyst group Semiocast). Users on Twitter write tweets up to 140 characters to tell others about what they are doing and thinking. Because almost all tweets are public, these rich data offer new opportunities for doing research on data mining and natural language processing. Thus, SentilyzeR uses Twitter as the source of its opinionated data. One way to perform Twitter sentiment analysis is to directly exploit traditional Sentiment Analysis methods (Pang and Lee 2007). However, tweets are quite different from other text forms like product reviews and news articles. Firstly, tweets are often short and ambiguous because of the limitation of characters. Secondly, there are more misspelled words, slang, modal particles and acronyms on Twitter because of its casual form. Thirdly, a huge amount of unlabeled or noisy labelled data can be easily downloaded through Twitter API. Therefore, we have proposed a novel SA method using the Machine learning framework especially for Twitter Sentiment Analysis that combines aspects of traditional Sentiment Analysis along with additional micro-blogging features.', 2012, 1),
('Windows 7 Mobile Application for Smart Shopping', 10, 'An online mobile application that allows us to browse through the various products available in various stores. The features and the prices are also made available to the user. Thus by looking at the different products the user can make the decision to buy which product and from where. A decision making algorithm is used to determine which products are to be made available to the user for selection. The solution will be obtained by considering parameters like cost, memory, distance from his place to shop,etc. Thus the user can browse through the most widely used and the most popular products. Also the nearest location of the shop from which he decides to buy from can also be shown to him with respect to his current location.', 2012, 2),
('Enhanced moving K-means algorithm for image segmentation', 10, 'Image Segmentation is a challenging task that simplifies an image so that it can be represented in a way that makes it easier to analyze. Although there have been numerous algorithms to perform image segmentation, the difficult part has been to overcome the drawbacks of all these algorithms. The most efficient so far has been K-Means algorithm. The algorithm deals with clustering which are proposed with the objective to produce better segmentation. K-Means algorithm for image segmentation has been modified and improved to provide Moving K-Means algorithm, Adaptive Moving K-Means algorithm and finally Enhanced Moving K-Means (EMKM) algorithm. In this report we analyze the advantages and disadvantages of each of these algorithms along with their observed outputs. Further, we also go on to discuss EMKM-1 and EMKM-2 algorithms and the comparison between the two.', 2012, 3),
('Screen Sharing for Mobile Phones', 1, 'With the advent of smart phones, mobile phones underwent metamorphosis and became capable of handling high end applications. Almost all the software and applications that could be run on a personal computer can now be run on a mobile phone. Software that enables users to share their resources remotely with other users is one of the latest types of applications that are being developed for computers. We want to create an application that makes allow users to accomplish this using their smart phones.\r\n\r\nMost of the smart phones have touch screens that allow the user a high level of interactivity. All smart phones operating using the Android OS incorporate touch screens. Such phones utilize their screen to display information and conveniently manipulate it. The screen can basically be used a digital interface to enhance the experience existing applications and make them much more user friendly. By making the screen of one user available to other users, the interaction between two users can be taken to a whole new level. This is what we want to achieve through our application.\r\n\r\nThis application creates a connection between two users through which they can share their mobile phone screens. Thus a digital interface is available to the users, on which they can exchange visual information. We use the internet to establish this connection between the two users.\r\n\r\nWhile designing this application we have kept in mind, to ensure security for the data of the participants of a screen sharing session. Also, as this application is being developed for a mobile phone, we have also taken into account the limited scale of resources that will be available to this application.', 2012, 4),
('Voice based enquiry system', 1, 'Speech is a natural mode of communication for people. We learn all the relevant skills during early childhood, without instruction, and we continue to rely on speech communication throughout our lives. There is no communication which is understood more appropriately than voice. Voice-based technology provides an alternative to ''traditional'' technologies such as paper-based systems, Hand-Held Devices, etc. Voice Based Automated Transport Enquiry System is the enquiry system which operates on the basis of some input in the form of a voice given by the user. This voice input will then be analyzed using neural networks. We will be using trained neural networks (which are trained using back propagation) for better results. After analyzing the input the appropriate output is generated, which is also in the form of a voice command.The basic goal of the voice conversion system is to transform the speaker characteristics, while maintaining the message intact.', 2012, 5),
('Movie Recommendations using Social Data', 6, 'Recommendation Systems are gaining more and more importance as the primary mechanism for a company to ensure healthy revenues. This is evident from the fact that the world?s leading movie streaming provider, Netflix, gains about 70% of its revenues directly from recommendations put directly up by its engine. Thus it is imperative that businesses adopt a more pragmatic approach towards filtering using artificial intelligence to keep at par with current times and competitors.\r\n\r\nMany online movie recommendation engines such as Yahoo! Movies and MovieLens use collaborative filtering to generate movie recommendations. This approach works well given a large data set of the user''s movie viewing history and ratings, which often involves filling out lengthy surveys, tracking user activities and monitoring search engine keywords. Thus the approach implements rather impromptu user feedback rather than focused and aggregated data collected over a period of time. Thus integration of the user''s social presence gives access to data that truly reflects the user?s taste.\r\n\r\nThe approach presented uses a trust based social network in conjunction with collaborative filtering. We hope to develop an efficient movie recommendations engine that utilizes existing personal profile information available on social networks and user?s social inclinations without the need for any explicit user input.', 2012, 6),
('Data Security In Cloud Computing', 8, 'Innovations are necessary to ride the inevitable tide of change. Most of enterprises are striving to reduce their computing cost through the means of virtualization. This demand of reducing the computing cost has led to the innovation of Cloud Computing. Cloud Computing offers better computing through improved utilization and reduced administration and infrastructure costs. Cloud Computing is still at its infant stage and a very new technology for the enterprises. Therefore, most of the enterprises are not very confident to adopt it.\r\n\r\nOur project will tackle this issue for enterprises in terms of cost and security. We focus on research and development in the area of data security in a cloud. We propose a solution that deals with providing redundancy and error detection and correction of data that is stored on the cloud. The three main service models in cloud are:\r\n\r\n    Software as a Service (SaaS) “ the cloud user controls only application configurations.\r\n    Platform as a Service (PaaS) “ the cloud user also controls the hosting environment.\r\n    Infrastructure as a Service (IaaS) “ the cloud user controls everything except the data centre infrastructure.\r\n\r\nCommon Security Requirements:\r\n\r\n    Confidentiality: Ensure that information is not disclosed to unauthorized persons\r\n    Integrity: Ensuring that information held in a system is a proper representation of the information intended and that it has not been modified by an unauthorized person.\r\n    Availability: Ensuring that information processing resources are not made unavailable by malicious action.\r\n    Non-Repudiation: Ensuring that agreements made electronically can be proven to have been made.\r\n\r\nObjectives:\r\n\r\nWe plan to implement an algorithm that will take care of the following things in terms of data storage in a cloud:\r\n\r\n    Ensure data redundancy.\r\n    Effective data block storage.\r\n    Challenge-response protocol for error detection.\r\n    Efficient protocol for error correction.\r\n\r\n\r\nCurrently, there is no security solution that allows the user to check the data stored on the cloud remotely. Our aim is to provide security in cloud computing by ensuring data integrity using digital signatures.', 2011, 7),
('Pharma-ERP via Mobile Phones', 1, 'Indian pharmaceutical enterprises can optimize their business process execution in order to retain a competitive edge in market place by automating their business processes. Business processes includes gathering, distributing, analyzing and integrating data for the strategic decision making and processing of the company. Automation provides quality software for employees and managers to logically plan and execute complex workflows simply. The automation software will be developed in accordance with the requirements of a pharmaceutical company Leben Laboratory P.V.T. L.T.D., Maharashtra. They already have DOS based application system.\r\nDrawbacks of the existing system:\r\n\r\n    Its allows only Single User mode.\r\n    It does not supports Graphics.\r\n    It does not provide the feature of Networking.\r\n    MS-DOS is very old by computer standards and not compatible with most programs and applications now a days.\r\n\r\nCommon Security Requirements:\r\n\r\n    Confidentiality: Ensure that information is not disclosed to unauthorized persons\r\n    Integrity: Ensuring that information held in a system is a proper representation of the information intended and that it has not been modified by an unauthorized person.\r\n    Availability: Ensuring that information processing resources are not made unavailable by malicious action.\r\n    Non-Repudiation: Ensuring that agreements made electronically can be proven to have been made.\r\n\r\nProposed System:\r\n\r\nThe automation software with interactive GUI will be created using the Microsoft Windows development tools to overcome drawbacks of current system. The design of the software will be such that it provides quick access to the information or process that is needed and is easy to use. It will be a suite of programs including Creating ledgers, Inventory management, Sales order processing, Account management, Billing terms, and Generating reports. Automation permits remote deployment of automated business processes allowing employees and managers to streamline their processes across a network in addition to providing them an extra level of security through private user name and password. Mobile phone utility provides the application software to be used remotely via cell phones. The software will be used by the managers and employees on their cell phones as a mobile application. This will make it convenient and portable for them to automate the business processes and have an immediate access over the information for decision making.\r\nScope of Project:\r\n\r\nThe proposed system will give an interactive way to access the information so as to improve the performance of the company to a great extent as compared to current system. Bringing functionalities in mobile will help manager manage things in efficient way by remotely accessing the database.', 2011, 8),
('Fault Tolerance And Load Balancing In Cloud', 8, '\r\nIntroduction:\r\n\r\nCloud computing is the latest buzzword in the I.T. industry. In concept, it is a paradigm shift whereby details are abstracted from the users who no longer need knowledge of, expertise in, or control over the technology infrastructure in the cloud that supports them. It typically involves the provision of dynamically scalable and often virtualized resources as a service over the internet. These services are broadly divided into three categories: Infrastructure-as-a-Service (IaaS), Platform-as-a-Service (PaaS) and Software-as-a-Service (SaaS).\r\nFault tolerance:\r\n\r\nFault tolerance is an ability of system to continue operation in occurrence of failure of part of the system rather than failing completely. Occurrence of failure might restart the whole system. This partial failure may result in reduction in throughput or increase in response time and leaves the system barren for considerable amount of time. The major aspect of fault tolerance is to maintain the integrity of the system in any damage.\r\n\r\nA system can be made fault tolerant by:\r\n\r\n    Replication: providing identical instances of same systems or subsystems. Thus by directing task to all of them in parallel a correct result can be chosen from the quorum.\r\n    Redundancy: Providing identical instances of same system or subsystems and then switching over to other in case of failure of one system.\r\n    Diversity: Providing multiple different implementations of the same specification, and using them like replicated systems to cope with errors in a specific implementation.\r\n\r\nLoad balancing:\r\n\r\nLoad balancing is an act of distributing work so as to achieve optimal resource utilization, increase throughput, reduce response time and avoid overload. It is important for networks where it is difficult to predict the number of request that will be issued to the system. If one system starts to get swamped, requests are forwarded to another system with more capacity. Because load balancing distributes the request based on the actual load at each node, it is excellent for ensuring availability and defending against denial of service attacks.\r\n', 2011, 9),
('Intrusion Detection System In Cloud Environment', 6, '\r\n\r\nToday, on-demand is the buzzword in the IT sector and the latest in that space is the concept of the cloud. The cloud, essentially, is a way of provisioning of computational resources like data, software, etc, via a computer network, rather than from a local computer. Cloud computing makes extensive use of computer networks and existing network topologies for efficient Internet based service provision. Hence, computer networks is the backbone for Cloud computing. Each technology or software may have some bugs or vulnerabilities, which make them an easy target for attackers to get into the system. Without a suitable security mechanism, Cloud users and provider may not be able to ensure that the service is thoroughly secure which may, in turn, affect the users trust on the provider. In order to secure these transactions from intruders, network must be made secure and reliable and also it should be safeguarded from intrusion attempts.\r\n\r\nThere are two approaches to network security in cloud: signature based and anomaly based. Signature based intrusion detection systems monitor network traffic and matches it with the dataset of predefined attack patterns to detect the attacks. However, signature-based technique fails to detect the unknown or new attacks whose signatures are not defined so far or are not included into the dataset of signatures. The anomaly-based technique is another technique to detect intrusion which can solve the problem of detecting unknown attacks. It defines the general pattern of usage and detects any event that deviates from this pattern as an attack. The limitation of this technique is a large number of false positives.\r\n\r\nIn this project, we attempt to design network intrusion detection system based on both signatures as well as anomaly based approaches. Also, attempt has been made to improve the performance\r\n', 2011, 10),
('Torrentzshare Search And Recommendation', 10, '\r\n\r\nBittorrent networking is the most popular form of P2P (peer-to-peer) file sharing. Since 2006, bittorrent sharing has been the primary means for users to trade software, music, movies, and digital books online. Bittorrents (also known as œtorrents) work by downloading small bits of files from many different web sources at the same time. Torrents strive to screen out dummy and corrupt files, are mostly free of adware/spyware, and achieve amazing download speeds. These characteristics have resulted in torrents garnering worldwide popularity on the Internet today.\r\n\r\nThe scenario as of today is that whenever the user searches for a torrent he/she has to narrow down the torrents according to the relevance, the seeds and peers, quality, reviews and ratings, date of upload of the torrent, and the size of the file. The whole process becomes tiresome as lots of websites have to be visited in order to find the best torrent.\r\n\r\nFor this we have proposed an application œtorrentzshare which will enable user to search for the desired torrent and return more accurate results. We have proposed our own search algorithm that will in addition to œkeyword match will also consider all the above mentioned parameters.\r\n\r\nAlso, our application will provide the user with accurate recommendations of torrent files that he/she may be interested in. For making torrent recommendations, we propose an algorithm which will give relevance to a particular torrent based on the user™s previous searches, interests, number of seeds and peers, age of torrent, or an uploader preferred by the user, torrents suggested or being used by Ã«friendsÃ­ of the user etc. Each torrent can be rated by the user and his friends. This feedback plays an important role for making a recommendation.\r\n\r\nAlso, our application will provide the user with friend recommendations that will help the user in expanding his/her network. This provides more room for the user to explore peers sharing similar interests thus having a strong impact on discovering valuable content without any hassles.\r\n\r\nA web crawler will form the basis on which our application will search through different bittorrent websites and find relevant torrents.In addition, the web crawler will also gather information about the various bittorrent websites it has visited. The information collected by the web crawler will then be used for supporting torrent searches by the user and also recommending appropriate torrents to the user.\r\n\r\nOur project will also include a study of various other softwares/client applications which are based on similar lines as our software. We will review their advantages and disadvantages and also evaluate them versus our own software. Thus trying to create an application software which is more efficient than the others.\r\n', 2011, 11),
(' e-Informer', 6, 'e-Informer is application where you point phone''s camera at somebody''s face and their social network information will magically appear. You have to join e-Informer and upload a photo to the database in order to be recognized, so it works with people whoï¿½ve decided that yes, if you take head-on picture of them its OK for you to see their social networking information. The application works by matching most distinctive features of face: the eyes nose and chin. Face recognition software creates a 3-D model of the personï¿½s face and sends it across a server where itï¿½s matched with an identity in the database based on which all the information of identified person which they have made available will be on display to the person who clicked the photo.', 2012, 12),
('Scalability in Cloud Computing', 6, '\r\n\r\nLet™s say you™re an executive at a large corporation. Your particular responsibilities include making sure that all of your employees have the right hardware and software they need to do their jobs. Buying computers for everyone isn™t enough “ you also have to purchase software or software licenses to give employees the tools they require. Whenever you have a new hire, you have to buy more software or make sure your current software license allows another user. It™s so stressful that you find it difficult to go to sleep on your huge pile of money every night.\r\n\r\nSoon, there may be an alternative for executives like you. Instead of installing a suite of software for each computer, you™d only have to load one application. That application would allow workers to log into a Web-based service which hosts all the programs the user who need for his job .Remote machines owned by another company would run everything from e-mail to word processing to complex data analysis programs. It™s called cloud computing, and it could change the entire computer industry.\r\n\r\nLoad balancing is a term that describes a method to distribute incoming socket connections to different servers. It™s not distributed computing, where jobs are broken up into a series of sub-jobs, so each server does a fraction of the overall work. It™s not that at all. Rather, incoming socket connections are spread out to different servers. Each incoming connection will communicate with the node it was delegated to, and the entire interaction will occur there. Each node is not aware of the other nodes existence.\r\nScalability:\r\n\r\nIf your application becomes busy, resource limits, such as bandwidth, cpu, memory, disk space, disk I/O, and more may reach its limits. In order to remedy such problem, you have two options: scale up, or scale out. Load balancing is a scale out technique. Rather than increasing server resources, you add cost effective, commodity servers, creating a œcluster of servers that perform the same task.\r\n\r\nIn this a node or a cluster can be added or removed remotely. If an extra node is added then load balancing of instances will take place. If any is deleted from a cluster then there will be live migration of instances from the deleted node to all other nodes in a cluster.\r\nScope:\r\n\r\n    In this project we will use some frontend graphical user interface which will help to add or delete a node from cluster and at back the script will run which will actually do the scaling in cloud computing environment.\r\n    In normal condition of additional resource request you need to restart the cloud service in order to scale it but our solution will provide the resource request facility on the fly i.e. dynamically.\r\n\r\n', 2011, 13),
(' INT TEST: An application to gauge mental acuity', 6, '"Int Test - An App to Gauge Mental Acuity" as we have named it is an application for Android 4.0 (Ice Cream Sandwich) to develop logic skills, mathematical abilities and vocabulary. Our aim is to build an integrated suite of challenges on diverse topics. Individuals can also use it for various competitive examinations and test their mental skills.	Like the muscles in your body, your mind strengthens with daily stimulation and can lead to atrophy if neglected and not used. This is why it is important to use the mind and continually challenge it. Being involved in leisure activities that help stimulate your brain can help you live a longer and happier life. Studies have shown that time spent stimulating the brain can help prevent dementia and Alzheimer''s disease.	What if we have something handy which can give our brain a quick workout session? Something like a mobile application which we can access at all times?	Something which provides sessions of exercises to boost reasoning skills, memory and mental processing speed staved off mental decline in middle-aged and elderly people in the first definitive study to show that honing intellectual skills can bolster the mind in the same way that physical exercise protects and strengthens the body? ï¿½Int Test - An App to Gauge Mental Acuityï¿½ comes with a solution to all these queries.', 2012, 14),
('The Design And Implementation Of Multimedia Terminal System On 3G Mobile Phones', 3, '\r\n\r\nThe development of economics and the globalization of trade make cross-region communication increasingly frequent. Traditional communication methods, such as telephone, fax and Email cannot satisfy people™s daily demands. Multimedia conference system supports people in different places share audio, video and documents to achieve real-time communication. The IP multimedia subsystem is a key component of 3G networks. Nowadays, multimedia conference system is mostly accessed via conference television and SIP terminal but lacks mobility support. The IP multimedia subsystem is a key component of 3G networks. It enables the seamless provision of multimedia services to end users. Multimedia conferencing is an important category of multimedia services in IMS, serving as the basis for a wide range of applications including audio/video conferencing, multiparty gaming and distance learning. Floor control is an advanced feature of multimedia conferencing which manages the access to shared resources such as audio and video channels.\r\n\r\nOur project extends the 3GPP architecture to include the design and implementation of advanced multimedia conferencing applications. It separates the floor control server and the media resource function processor, and it also opens up advanced conferencing capabilities. A SIP-based floor server control markup language and a multi-level abstraction API are proposed. An important finding is that tools and technologies for the design and implementation of multimedia conferencing applications in IMS are relatively immature.\r\n', 2011, 15),
(' Optimized Multiplayer Gaming using GO', 2, 'Today, computers are enormously quicker but software development is not faster. In currently used languages, there are many issues like, dependency management, garbage collection and parallel computation, management in multi-core system. o we believe it''s worth trying a new language which is concurrent, garbage-collected language with fast compilation.Some features for betterment such as modelling for software construction for dependency analysis, avoiding much of overhead inclusion of files and libraries, no inheritance, garbage-collection, for concurrent execution are implemented in GO.Our project is implementation of a web application called ï¿½Chess-o-maniaï¿½ which hosts Chess games. It will be implemented in GO and HTML5 to study various above mentioned features of GO. The Chess game follows the basic rules of chess, and all the chess pieces only move according to valid moves for that piece. Our implementation of Chess is for two players (no Artificial Intelligence). It is played on an 8x8 checked board. We intend to make our Chess program user-friendly game for two players. We also propose to implement the same algorithm in Java to study the comparison of GO and Java.', 2012, 16),
('Distributed Streaming', 8, '\r\n\r\nThere has been an increasing trend in designing the Video-on-Demand systems using parallel server-based architectures to meet the client demands for online streaming of videos.We use a distributed architecture for effective Buffer management which efficiently utilizes various server resources like the disk Bandwidth, memory utilization and provides clients with VCR like functionalities.\r\n\r\nTo choose the best caching technique for VOD, one should note that traditional caching algorithms, such as LRU, do not work well for video servers due to the large size of video objects and result in low cache hit ratio. One alternative to traditional caching algorithms is interval caching policy. We have proposed a new Cooperative Interval Caching Algorithm which implements caching in distributed systems and allows distributed caches in the cluster to cooperate together to produce a high cache performance close to that of one large cache with the same size as their integral size, while minimizing the probing overhead by intelligently directing a request to the most appropriate server which has the cached stream.\r\n\r\nWe also propose a unique On-line dynamic buffer replacement policy that does not base its results solely on a single parameter such as video popularity but calculates a cumulative popularity based on several other factors and hence chooses the video with the least popularity thus calculated.\r\n\r\nIt is necessary to batch requests to minimize the bandwidth requirement, to reduce I/O demand, improve throughput and increase the number of customers served by using less number of streams. We propose a Modified Adaptive Batching Policy for batching requests arriving at a server. This is different from the existing adaptive batching techniques which batch a user request into a multicast stream by increasing the speed of transmission in the unicast stream and hence batching it. Here, instead we compare the user arrival with a threshold batch time and according to the difference we assign users to different streams classified as multicast queue, submulticast queue and unicast queue.\r\n\r\nWe also propose to implement distributed streaming of videos to clients using the Real Time Streaming protocol (RTSP) which being a control protocol is designed to add more VCR like functionalities to the streaming process. To enable the implementation of the RTSP protocol, we intend to develop a customized portal which supports the RTSP protocol.\r\n', 2011, 17),
(' Crime Analysis using Data Mining Techniques', 2, 'Crime is constantly evolving and sophisticating itself. Since the number of criminal incidents increase over the years the amount of data collected by police forces and other such law enforcement agencies has also exponentially increased. Storing this data sequentially makes it extremely difficult to extract knowledge from it for use in future investigations. Thus rendering all past data practically redundant. Researchers world over have been working on creating tools for analysis and systematic storage of this data. We aim to design a system which stores this data intelligently and uses it to derive conclusions, detect patterns and make predictions. The system will process crime reports and extract relevant data from them. It will also process missing fields by estimating a suitable value for them. Following this the system will use clustering, classification and association techniques to partition data into groups based on various similarity measures. The system can be queried through a user-friendly interface to obtain the required case specific information. Such a system will allow law enforcement agencies to use past data in an intelligent and efficient manner so as to optimize their investigation. The user interface will be designed in a way that any law enforcement officer with basic technical knowhow will be able to use the system as per his needs. The user interface will also allow for visualization of data that is retrieved. This system will be a valuable tool for law enforcement agencies world over to make better sense of the data they have and make faster and more intelligent decisions.', 2012, 18),
('Neural Network Model for employee performance evaluation', 4, '\r\n\r\nIn information and knowledge economy era, the competition among enterprises has been extended in space and strengthened in time. In order to obtain long-term survival and sustainable development, the enterprises must pay more attention to their employees. Therefore, enterprises need to construct a whole set of comprehensive and objective overall performance evaluation system.\r\n\r\nEmployee is the masses foundation and impetus for organization development and expansion, through understanding and evaluating employee correctly, the enterprise not only can impel employee effectively, but also can guarantee the organization rapid and sustainable development.\r\n\r\nThere are many successful applications of Backpropagation (BP) for training multilayer neural networks. Performance Evaluation is one of the application of Backpropagation. However, it has many shortcomings. Learning often takes long time to converge, and it may fall into local minima. One of the possible remedies to escape from local minima is by using a very small learning rate, which slows down the learning process.\r\n\r\nWe propose to implement the above application i.e Employee Performance Evaluation using Radial Basis Neural networks containing radial basis functions can be used in many of the same situations in which back-propagation networks are used.\r\n', 2011, 19),
(' Air-Draw: A Drawing Tool Using Hand Gesture Recognition', 2, 'Since the introduction of the most common input computer devices, not a lot has changed. This is probably because the existing devices are adequate. It is also now that computers have been so tightly integrated with everyday life, that new applications and hardware are constantly introduced. The means of communicating with computers at the moment are limited to keyboards, mice, light pen, trackball, keypads etc. These devices have grown to be familiar but inherently limit the speed and naturalness with which we interact with the computer. Vision based interfaces are feasible and at the present moment the computer is able to ï¿½seeï¿½. We understand this ability of the computers to see and comprehend. Using this, we aim at enabling a rich and user friendly human computer interaction. The project Air-Draw will be a drawing tool; but not just any drawing tool. It will aim at drawing on a computer using hand gestures which will be recognized by the webcam. It will help the artists to make a digital painting without using the mouse in a natural way and express themselves freely, thus enhancing Human Computer interaction. The main goal of applicable algorithms is to measure the hand configuration at each time instant. Our application will use images from a low cost web camera placed in front of the work area, see where the recognized gestures act as the input for a drawing tool and comprehend them accordingly. Thus, the user, rather than pressing buttons, must use different gestures that our application should recognize. The system will recognize various hand gestures predefined to do a specific task like painting, changing the drawing tool, et al. Steps involved are: hand segmentation, hand tracking and gesture recognition from hand features and then based on that, giving input to the drawing module which will display the result.The social impact of this application is also very effectual. It will help the physically disabled who find it difficult in operating a mouse to draw on a computer. It will take art to a whole new level of digitization. Make Life Better.\r\n\r\nOur project will tackle this issue for enterprises in terms of cost and security. We focus on research and development in the area of data security in a cloud. We propose a solution that deals with providing redundancy and error detection and correction of data that is stored on the cloud.', 2012, 20),
('Visual Cryptography', 2, '\r\n\r\nTraditional cryptographic schemes require end users to employ complex operations for encryption as well as decryption. An alternative to this is visual cryptography, where the decryption is completely performed by the human visual system.\r\n\r\nVisual cryptography (VC) schemes hide the secret image into two or more images which are called shares. The secret image can be recovered simply by stacking the shares together without any complex computation involved. The shares are very safe because separately they reveal nothing about the secret image. Earlier, visual cryptographic schemes were restricted to binary images and the shares generated were made of random black and white pixels. Recent efforts have resulted in extended visual cryptography schemes where the secret image is hidden behind two cover images. We have proposed a new extended visual cryptography scheme utilizing error diffusion halftone technique and coding tables which will result in a better quality decrypted image.\r\n', 2011, 21),
(' Messaging World', 4, 'SMS, or text messaging, has replaced talking on the phone for a new "thumb generation" of texters. SMS i.e. short messaging systems have now become an important means for communicating with friends and colleagues.By combining the features of Android, a lot of applications have been developed based on the sms system , such as WhatsApp Messenger that enables you to chat with your friends or group chat for free provided you have net enabled on your phone, AutoReply which auto replyï¿½s your friend with a specified message on the specified date and time, eBuddy Messenger allows you to stay connected with all your friends and family on MSN (Windows Live Messenger or WLM), Facebook, Yahoo!, MySpace, Gtalk (Orkut), etc. So a lot of work any be done easily with the help of sms in your phones. And hence we are going to develop an app named ï¿½Messaging Worldï¿½ for Android based smart phones where in if you forget your cell phone, you will be able to retrieve your contacts from it easily.In this application,to retrieve the contact a person has to send the required contact name along with the verification code. The app will verify the code and then compare the contact list with the name sent in request message. The reply message will be sent back. It will contain the required contact number.', 2012, 22),
('Implementation of data security model in cloud computing', 8, '\r\n\r\nCloud Computing is a model for enabling convenient, on-demand network access to shared pool of configurable computing resources. It comes with minimal management effort or service provider interaction. One of the advantages of cloud is that it provides a way to increase capacity or add capabilities without investing in new infrastructure, or licensing new software. Cloud provides computation, software, data access, and storage services that do not require end-user knowledge of the physical location and configuration of the system that delivers the services. Cloud computing providers deliver applications via the internet, which are accessed from a Web browser, while the business software and data are stored on servers at a remote location.\r\n\r\nSince cloud stores and acts upon huge amount of data, security issues are of utmost importance, in order to ensure privacy. The relative security of cloud computing services is a contentious issue that may be delaying its adoption. Security issues have been categorized into sensitive data access, data segregation, privacy, bug exploitation, recovery, accountability and account control issues. Data security includes properly segregating data, so as to avoid data leaks or exposure of sensitive information to third parties. Data must be accessed by authorized users and digital identities and credentials must be protected as should any data that the provider collects or produces about customer activity in the cloud.\r\n\r\nOur project aims to implement a data security model in cloud computing. We intend to build a system in which: The user is allowed to store data in an encrypted form using a symmetric cryptic key. This key is then encrypted using an asymmetric key, so that only he will have access to it using his private key.\r\n', 2011, 23),
('Security enhancement using advanced AES DES algorithm', 4, 'DES (Data Encryption Standard) is a cryptographic standard. However, the applications of it are limited because of the small key space. Based on irrational numbers, an improved scheme that enhances the randomness of sub-Key is proposed, in which the permutation is controlled by irrational number which is considered as false chaos. Moreover, the permutation controlled by data can be performed at high speed in generic CPU. It is shown that this scheme can expand the key space without costing any more time to run. The plaintext is encrypted to cipher text by the key with a length of 64 bits, in which 56 bits are used for encryption, and the others are employed for parity test. Encryption and decryption use the same algorithm as well as the key. It is no longer a question to attack the 56-bit key with the development of computer technology. The attacker can decipher DES within 20 hours through exhaustive key search.An improved scheme is proposed here to control the transposition by irrational number in order to overcome the defects mentioned above. Irrational numbers, which are similar to datasequences generated form chaotic system, are infinite and non-cycle. However the former is not sensitive to the initial condition. Therefore, using irrational numbers to encrypt will provide higher security without strict chaos synchronization.The improved scheme prevents the key from being directly involved in the production of sub-keys, and increases the transposition controlled by irrational number on the basis of DES, which has higher security, great non-crack ability and wide applicability.	To further improve non-crack ability we integrate AES(Advanced Encryption Standard) with Improved DES with Irrational Numbers. We will integrate AES in between all rounds of DES encryption. The result of EX-OR of fiestal function and left 32-bit part of 64-bit data will be encrypted with AES and left 32-bit of original 64-bit data will also be encrypted with AES. These two AES encrypted 32-bit blocks will be used in further rounds. Only authorized users can use the application.', 2012, 24),
('Design of a Private Cloud', 8, '\r\n\r\nToday cloud is an emerging field of research.By design of private cloud we mean designing a cloud that is proprietary of computer department of SPIT. In this project, we want to provide a service to students/professors using which they can work efficiently.\r\n\r\nWhile doing our projects and labs for lower semesters,we faced a problem of slow/not responding PC this problem mainly arises because of two reasons load on processor due to large number of processes running and due to low memory and viruses.\r\n\r\nBy proposing our system we are trying to solve first problem using cloud computing. by mere observation also, we can conclude that there is no point of having all the softwares as we don™t use them simultaneously.Instead having all the software™s installed slows down the performance. So, in our system user will be requesting the resources required for him to do labs and projects. Whatever work he will be doing will be maintained at cloud, so in this way we try eliminating first problem in a œcloudy way. In our system , we will be providing platform as a service and software as a service.\r\n', 2011, 25),
(' Recording and Reproduction of Sound', 4, 'This paper presents a ?rst of its kind approach to reproduce sound by viewing the vibrations of the source with the help of a camera. This computer vision based approach for recording and reproducing sound overcomes every obstacle faced when using microphones to record sound. The major drawbacks of recording sound with microphone are attenuation of the signal and distortion of signal caused by noise. These problems are not encountered when using the computer vision based approach. Thus, the sound produced is clearer much more accurate. The limitations imposed by the camera are the only issues that affect the reproduced sound. The approach is the best when there are many sources of sound situated close to each other, and we are interested in the sound from a particular source. The approach can be applied in any solid, liquid and gaseous medium without the need for making new equipments according to the medium. In this paper, we propose an algorithm for detection and reproduction of sound using Computer Vision. We show how the existing algorithms for Object Tracking, Detection of Vibrations, Sub-pixel Registration and Production of Bezier Curves can be integrated into the proposed algorithm. Techniques to improve the accuracy of the sound are described. We outline the bene?ts that this approach offers to researchers interested in recording sound in different media. The limitations of the approach are described and ways to overcome these limitations are suggested.', 2011, 26),
('Utility pack under Android environment', 6, '\r\n\r\nThe Smart Android Application Package is a group of applications that are developed under android environment. The package consists of two applications Quick Search and smart utilities.\r\n\r\nQuick Search is an application that provides location based services where a user using the application is able to gain information about nearby important emergency locations and services like hospitals, blood banks and police stations. All the information is stored on server. All that users need to do is to enter the name of the city and based on that application provides all information about that service. The information required will be displayed on the Google map as the canvas. Also a user has privilege to send SMS or email to the hospital or police station in case of emergency. That is the main advantage of this application.\r\n\r\nSmart Utilities application is an integration of utilities. Health Service utility describes the health information of user. Numbers is the utility storing important numbers like passport number, PAN card number, ADHAR card number. Password store is a utility providing facility to store password details of various accounts which itself is protected by a password. Electronic gadget maintenance utility provides detailed information of user™s automobile like automobile name, number, license details, last date of maintenance.\r\n\r\nThe entire package makes use of android platform which has got many advantages. It makes use of Linux kernel. The application is to be developed in Eclipse IDE with an Android plug-in, Android SDK 2.2 for Windows and WAMP server backend support (database).\r\n', 2011, 27),
('Text Extraction From Image', 4, 'The world we are living today is called as Information age. Knowledge comes from information. And, knowledge is power. This precisely brings out the importance of information in today™s world. The need of the hour is to efficiently process this information to achieve greater knowledge and improved decision making. The extraction of text in an image is a classical problem in the computer vision. Extraction involves detection, localization, tracking, extraction, enhancement and recognition of the text from the given image. However variation of text due to difference in size, style, orientation, alignment, low image contrast and complex background make the problem of automatic text extraction extremely challenging. Text within a camera grabbed image can contain a huge amount of meta data about that scene. Such meta data can be useful for identification, indexing and retrieval purposes.Here we will develop the system which will be able to recognize the pattern of the text data from the image file. Then we will extract that data and convert it into the text file. As memory used by image data is more than the text data having same text information. So this idea will save the memory used to store image file as compared to text file and will also allow user to edit, delete and add the extracted text of the image.', 2011, 29);
INSERT INTO `projects` (`topic`, `faculty_id`, `content`, `year`, `id`) VALUES
('ANFIS Based SPAM Filtering Model For Social Networking Sites', 1, 'Spam is flooding the Internet with many copies of the same message, in an attempt to force the message on people who would not otherwise choose to receive it.There are various types of spam such as email spam, forum spam, online classified ads spam, attachment spam, social networking spam etc.We would like to concentrate more on social networking spam(SNS). Social networking spam is when unwanted messages or posts are sent to people in bulk, or when a single click of a seemingly harmless link reposts the link on other profiles,thus spreading the spam like a virus. We plan to use an adaptive neuro fuzzy inference system (ANFIS) that incorporates the advantages of both the neural networking concepts and fuzzy logic to identify the spam messages on such websites.', 2011, 30),
('Media Download Application For Retail Kiosk with touchscreen interfaces', 10, 'In this project report, we propose a new stand-alone software application for a retail kiosk which would be used for accessing media files as well as to download them. Originally, a kiosk is used by customers to play certain media files and then buy them explicitly. Thus, having the customer directly download their chosen media files from the kiosk itself is imperative. Therefore, the main purpose of a media download application is to allow the user to download media files directly from the kiosk to the customer™s external hard drive. In this report, we delineate the procedure of downloading media files into the external storage device and the type of architecture suitable for such an application i.e. the Model View Controller (MVC) architecture. We also illustrate the MVC design aspects, database design and normalization aspects used to implement such a unique application. Last but not the least we demonstrate how the Java Swing technology is used to create such a highly interactive GUI efficiently.', 2011, 32),
('Software based online digital media store for personal computers', 10, 'Nowadays, efficacious accessibility of data stored in large databases is a major aspect of any system which needs to access data stored in such databases. In this project report, we propose a service oriented framework for supporting a voluminous database. This database is a large media database which would provide the client with media files using these framework services. The main purpose of this system is to provide accessibility to media files efficiently and only to the authorized users. One of the major quirks in this proposed system is its search Application Programming Interface. In this report, we demonstrate the services provided by the system and the type of architecture suitable for such a system i.e. the Layered architecture. We also illustrate the critical aspects required to speed up the process of efficient retrieval of data, database design and normalization aspects used to implement such a system. Lastly, we purport the requirement of various technologies used to create such an efficient system.', 2011, 34),
('Active Learning For Interactive Image Retrieval', 5, 'Image classification and retrieval is the application of computer vision techniques to the image retrieval problem, that is, the problem of searching for digital images in large databases. Content based image retrieval is opposed to concept based approaches."Content-based" means that the search will analyze the actual contents of the image and classify them rather than the metadata such as keywords, tags, and/or descriptions associated with the image. The term ''content'' in this context might refer to colors, shapes, textures, or any other information that can be derived from the image itself. Our project aims at classifying images and retrieving them based on the image content. In this project the user selects an image so as to retrieve similar images to the one he queried. Images are classified as per the content. The retrieval process is two step. The images are compared with the existing images and then ranked using a similarity algorithm. The first few similar images are displayed to the user.', 2012, 36),
('Mobile Application Development (IPhone OS)', 5, '\r\n\r\nThe aim of the project is to build an iPhone application to play and download song tracks. This iPhone application is built using Objective C programming language, which is a special programming language used by Apple Inc. for its trademarked iPhone and iPad applications.\r\n\r\nThe iPhone application is a front-end which is integrated by us with an already existing backend provided to us by the company. The application uses HTTP Request to send a query to the database which returns XML data. An XML parser is designed to interpret this data and present it in a more user-readable way. Based on this data the user selects the songs which he wants to play or download.\r\n', 2011, 37),
(' Online Project Management Tool', 5, 'Online Project Management Tool is tool to manage various projects online. Clients will approach us for the project and will handle it online. Each project will have Project Manager, Team Leader, Team Member. Project manager will divide the teams and will assign tasks to team. Each team will have certain timeline for completion of tasks. Team leader will assign task to team members and each member will update status of tasks (Completed, Not Completed and Not Yet Started). It is team leaderï¿½s responsibility to report to the project manager about the task status and request for resources requested by team member.', 2012, 38),
('Analytics Dashboard for Video File trans-coding from content owners', 3, 'An important part of Mime360²s activities includes ingesting new data as and when received from clients. The Data has to be Normalized, Sanity Checked, Catalogued and finally Indexed before it can be delivered to the various clients. With Data coming through various sources at different times, it is imperative to track and analyze the progress of data at various stages in order to accurately predict timelines and deliverables. Analytics tool for data ingestion in video transcoding is web application which will guide to user of system to analyze each of the four phases in transcoding process. This gives information about which video files are in which stage of process. Along with this, it also provides information about amount of time required to process certain file and time of processing remaining. The system also shows total time remaining for all files to be processed. The system allows the user to select the formats to which he wants to convert his video files. These files are queued up and processed according to availability of resources. These files to be processed are selected form the system itself which will trigger backend to do the processing.', 2011, 39),
('GUI And Tracking Of Audio Conversion And Log Files For Various Audio File Types', 3, 'Mime360.com is a Digital Media Distribution Company that performs activities of ingesting new data as and when received from clients. The Data has to be Normalized, Sanity Checked, Catalogued and finally Indexed before it can be delivered to the various clients. With Data coming in through various sources at different times, it is imperative to track and analyze the progress of data at various stages in order to accurately predict timelines and deliverables. To track the progress made by each stage, which data is being processed, how many more files are queued up for undergoing the process, total time required for process to complete were some of the challenges faced. To overcome these problems Analytics tool for data ingestion was proposed to be built. This analytics tool will guide the user and provide him with the details of each stage. This report includes brief knowledge of how analytics software will be developed in accordance with the requirements of a Digital Media Distribution company Mime360.com. Report also includes implementation details and functionalities included which will benefit the company to track their work progress and prioritize their work.', 2011, 40),
('Mobile Android Financial Portfolio Management', 6, '\r\n\r\nOur system aims at providing an all-at-one-place mobile based investment management solution. The user of the system will be able to view all his investment details without the hassles of visiting various websites to know the current share prices and the Net Asset Value (NAV) of his/her mutual funds. The system also has a unique reminder feature, owing to which, the user will not have to remember the insurance policy due dates, and also the maturity dates of his fixed deposits. Hence, with the help of this system, the user will be able to effectively manage all his investments. The application is built on android operating system and can run on various android devices like mobile phones, and tablet PCs. The application allows the user to enter details of various investments made by him/her. Each user will have to enter a username and password at the time of sign-up and subsequent login will require the user to enter the correct username and password. The application helps manage the following investment options:\r\n\r\n    Fixed Deposits\r\n    Insurance Policies\r\n    Mutual Funds\r\n    Public Provident Fund\r\n    Stock Market Shares\r\n    Miscellaneous Investments\r\n\r\nThe system displays details of all the above investments. It also sends reminders to the user, one week prior to the due-date, pertaining to his Fixed Deposits maturity date, insurance premium due dates, and all other necessary notifications. As an added feature, the application automatically syncs the current share prices for stocks, and the current Net Asset Value for mutual funds from the internet, and displays the same. The system is provided with adequate level of security. Each user will have to undergo at least one authentication check before being able to modify or view the investment profile.\r\n', 2011, 41),
('Intelligent Mobile Application on Cloud-Hangout', 5, 'There are many web applications available today.The types of web applications are Document Centric (Static homepage,web radio, company web site),Interactive ( Virtual exhibition, news site, travel planning ),Transactional ( online banking, shopping, booking system ),Workflow based ( E government, B2B solution ),Collaborative ( chat room, E learning plateform, P2P-services ), Portal oriented ( community portal, online shopping mall, business portal ),Ubiquitous (customized services, location aware services, Multi plateform delivery ),Semantic (Knowledge management). Our application is a social web application. We aim to use the APIï¿½s provided by Google and Zomato(a restaurant reviewing website). The application will allow the registered people to interact with each and share their respective Latitude location and help them to decide a place where they can ï¿½hangoutï¿½ with friends.The application makes the intelligent decision based on the experiences, reviews , location and number of times the restaurant is visited. The application also helps the user to make decision in travelling by helping to reach his/her destination by providing alternative least traffic routes.The system collects the data of the registered user travelling on a particular route from google latitude API and applies an algorithm to find the time required to travel on that route at that particular time and stores that in the database so that other users using the application can get the least time of travelled route i.e traffic free route on that source-destination at that particular time.\r\n\r\nOur project will tackle this issue for enterprises in terms of cost and security. We focus on research and development in the area of data security in a cloud. We propose a solution that deals with providing redundancy and error detection and correction of data that is stored on the cloud.', 2012, 42),
('Ninja Mouse', 1, '\r\n\r\nThe use of the mouse and keyboard has long been our only sense of interaction with the computer. What remains unexplored is the infinite possibility of an intuitive 1:1 human interaction.\r\n\r\nWe investigate the use of a webcam that understands and recognizes our gestures that can take us a level up in the field of Human Computer Interaction. Similar to the famous Sixth Sense Mouse, however, without the use of specialized marked gloves, but with bare hands.\r\n\r\nLargely accurate, our product will allow you to take control of an on-screen cursor, replacing conventional use of the mouse and keyboard, by simple hand gestures. While this may seem cumbersome, such products have found support from the more artistic community, while being a favourite in the gaming world.\r\n\r\nApplications of such a product are myriad. Intuitive gestures like swiping your hand to change a powerpoint slide or an artist who may want to use his hands to work on softwares like Photoshop for a more realistic, comfortable and accurate motion, are a few of them. Often, the feel of reality is lost, and NinjaMouse, brings back exactly that.\r\n\r\nMade famous by the Microsoft™s Kinect, this experience is till date, a costly affair for most people, with the product being expensive. NinjaMouse will use a simple webcam and a high end image processing software to form a layer at the OS level, allowing users to have this experience for any software.\r\n\r\nWe have also kept in mind, scalability and seamless integration as one of our prime goals. Our project is going to be developed in Python, thus making it easily usable on Linux and Windows systems. The social impact of such a product cannot be ignored at any stage. This system can be extended to the use of one™s head gestures to do almost the same functions, thus enabling physically challenged or injured people to use the computer without any external help.\r\n', 2011, 43),
(' Scalability in financial Application on Cloud', 8, 'Cloud computing is a pay-per-use model for enabling available, convenient, on-demand network access to a shared pool of configurable computing resources (e.g. network, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction. Cloud computing offers on-demand software service to customers without having the customer know the inner details of the software or IT infrastructure used for their ser-vice. Businesses outsource their computing needs to the Cloud to avoid investing in infrastructure and maintenance.Scalability refers to the ability to service a theoretical number of users. Cloud computing can be scalable up to tens of thousands, hundreds of thousands, millions, or even more, simultaneous users. That means that at full capacity, the system can handle that many users without failure to any user or without crashing as a whole because of resource exhaustion. The better an application''s scalability, the more users it can handle simultaneously. One sector that would signi?cantly bene?t from Cloud is the ?nancial sector.Financial services firms often require ad-hoc access to significant computing resources, whether for applications such as risk management, mark-to-market or ï¿½what-ifï¿½ scenario analyses. In our application the resource should be in-scaled at the peak hours and should be out-scaled when they are not required. So keeping this in mind weï¿½ll design an algorithm that satisfies the requirements. The model will be designed keeping in mind that scalability is a prime expectation.', 2012, 44),
('Infrastructure As a Service', 8, '\r\n\r\nNowadays Cloud Computing is the most recent buzzword in the field of computer science. Many technology giants like Google, Microsoft, Amazon, etc. are doing pioneering work in this field. Cloud computing has many applications in the fast growing economy where companies can scale their infrastructure requirements as and when they require and pay only for what they use. Cloud computing provides a cost effective, scalable and reliable solution to various business needs.\r\n\r\nMany times it has been found that the website go down either due to bottleneck at the server or due to lack of resources (CPU, RAM). Also many companies want to provide their applications as software services to their users so they can use it without having to buy the license for a year when they just need it for few working hours. Also there are clients who would like to hire some infrastructure forfew hours to accomplish their one-time task rather than owning such huge infrastructure which is difficult to manage. The solution to the scenarios mentioned above is Cloud computing which has evolved as a disruptive technology and picked up speed in 2008 and 2009.\r\n\r\nCurrent cloud computing infrastructure offerings are lacking in interoperability. User should not be burdened by vendor lock-in. Our project aims at creating a cross cloud federation environment which will enable the cloud vendors to federate so that they can ask each other for resources when they are exhausted. For e.g. a user request for resources from a home cloud but unfortunately cloud is exhausted of resources so it contacts a foreign cloud to borrow resources for the user and this complete process remains transparent to the user. The user never knows that he/she is using resources of foreign cloud. But for this to be possible the different cloud vendors should co-operate with each other. We propose method to discover available foreign clouds, find out the best and then authenticate home cloud to access resources of foreign cloud. On successful authentication home cloud can create and monitor virtual machines (VMs) on foreign cloud in federated environment.\r\n', 2011, 45),
(' Data Synchronization over Cloud Accounts', 8, 'Data Synchronization over Cloud Accounts is a web based application that attempts to connect technologies, like Google drive and Dropbox , that donï¿½t really get along very well, and make them work as one. It aims to transfer data directly from one cloud to another without having to download them to end userï¿½s computerï¿½s first. The application will establish sessions between the two clouds thus connecting them. It will help to detect redundant data in a mass of storage and provide multiple backup facilities on different cloud services.\r\n\r\nThe application will use our cloud servers to carry out the data transfer. Thus it would be necessary to balance the load on our intermediate servers using divisible load scheduling. Thus, our objective is to develop an effective system using divisible load scheduling theorem to maximize or minimize different performance parameters (throughput, latency for example) for balancing the load on the server at a particular instant.\r\n\r\nThe application will be first of its kind making Data Synchronization over Cloud Accounts possible for users having insufficient memory resources. It aims to address this subtle problem that has marred the usefulness of cloud systems.', 2012, 46),
('Adaptive Batching policy in distributed environment', 8, 'We plan to implement the adaptive batching Technique for storage management for providing data services in the distributed environment. Due to the higher cost of fetching data from disk than from RAM, most database management systems (DBMSs) use a main-memory area as a buffer to reduce disk accesses. In a distributed database system, database is spread among several sites over a computer network. It mainly focuses on the storage and efficient utilization of the available bandwidth to provide continuous streaming of the requested data without any staggering in it. Multiple client requests for the same type of data can be served with a single disk I/O stream by sending (multicasting) the same data blocks to multiple clients (with the multicast facility, if present in the system). This is achieved by batching (grouping) requests for the same request that arrive within a short time. We explore the role of customer waiting time and reneging behavior in selecting the data to be multicast. The performance of the batching policy depends highly on the selection of the batching time. If the batching time is too long for the popular data (high arrival rate), more streams are required. On the other hand, if the batching time is too short for the unpopular data (low arrival rate), it reduces the batching effect and lowers the system performance. Thus, finding the suitable batching time for each data is a crucial issue. In general, having watched the data, the customers rarely watch it again. The popularity of the data decreases with time. It is very difficult to accurately predict a dataï¿½s popularity and set the corresponding batching time.', 2012, 47),
('Travel Guide Information Using AI', 3, '\r\n\r\nA chatterbot is a computer program designed to simulate an intelligent conversation with one or more human users via auditory or textual methods, primarily for engaging in small talk. They are developed for variety of reasons like online help, personalized service, as a part of interactive games, website e-commerce agents, etc. They are simple to use in which you chat textually with the bot over a computer screen. The bot usually introduces itself. You respond with a statement or question. The bot consults its knowledge base or programming languages and replies. The conversation continues as long as it is interesting or useful for human.\r\n\r\nOur chatbot is acting as a travel guide. It has been given specific task to provide guidance to the users about The users can ask queries to the chatbot related to traveling to which our chatbot will provide him with appropriate traveling information that is stored in the knowledge base. So it provides us basic human-to-human communication abstracting mechanical aspects of it. The learning module will give the bot the capability of learning new facts and expanding its knowledge base. We visualize this character as a personalization of someone who has knowledge about something but is constantly learning everything that is said to it. This chatbot with time will expand his/her knowledge and future conversations will hopefully be more meaningful, useful and entertaining.\r\n', 2011, 48),
('Offline Handwriting Recognition System', 2, 'Handwriting recognition is the ability of a computer to receive and interpret intelligible handwritten input from sources such as paper documents, photographs, touch-screens and other devices. The image of the written text may be sensed œoff line from a piece of paper by optical scanning (optical character recognition) or intelligent word recognition. Alternatively, the movements of the pen tip may be sensed œon line, for example by a pen-based computer screen surface. Off-line handwriting recognition involves the automatic conversion of text in an image into letter codes which are usable within computer and text processing applications. The data obtained by this form is regarded as a static representation of handwriting. Offline handwriting recognition?he transcription of images of handwritten text?s an interesting task, in that it combines computer vision with sequence learning. Our project aims to create offline handwriting recognition system using Optical Character Recognition techniques.', 2011, 49),
('Video Stegnography', 2, '\r\n\r\nEssentially, steganography is the art of concealing private or sensitive information within a carrier that for all intents and purposes, appears innocuous. steganography relies on the sender and receiver agreeing upon the method by which the information will be and therefore some means of prior communication is essential for steganography to be of any use. The science of concealing information was later to be known as œsteganography and the current technology of œDigital Watermarking has taken its root from it. The term œwatermark in terms of digital data was taken from the concept of watermarks used to prevent faking of currency notes. Steganography is sometimes confused with cryptography. Although the two can co-exist as discussed later in this document, they are not the same. Both are used to protect information but steganography is concerned with concealing information thereby making it unseen while cryptography is concerned with encrypting information thereby making it unreadable. Different categories of Steganography.\r\n\r\nAlmost all digital file formats can be used for steganography, but the formats that are more suitable are those with a high degree of redundancy. Redundancy can be defined as the bits of an object that provide accuracy far greater than necessary for the object™s use and display. The redundant bits of an object are those bits that can be altered without the alteration being detected easily.\r\n\r\nImage and audio files especially comply with this requirement, while research has also uncovered other file formats that can be used for information hiding. The figure below shows four main categories of file formats that can be used for steganography.\r\n', 2011, 50),
('Web based Complaints Registration', 10, 'Mumbai is becoming one of the unsafe city to stay as the crime rate is increasing in the city.The growing rate of unemployment, poverty, other socio-political conditions are some of the factors that contribute to the increasing rate of thefts, chain snatching, bag snatching, robbery, murders, pick pocketing. With the increasing number of crimes in the city the minor crimes such as pick pocketing, snatching, and bribery go undetected.\r\n\r\nIn spite of going to the police station some police officers refuse to take complaints. No crime should go undetected. Keeping this view in mind the Online Police Complaint is meant to serve as a means to receive complaints against such police, domestic complaints, cyber crime violation of traffic rules etc and to further research, develop, and refer the criminal complaints to the police station near the crime location to carry investigation.\r\n\r\nThis application will provide the user to easily navigate through the pages. The Online Police complaint gives the victims of crime a convenient and easy-to-use reporting mechanism that alerts authorities of suspected criminal or civil violations.\r\n\r\nIt will also help police authority to manage all the police records such as all online solved and unsolved cases, missing and wanted person information. This application will allow an authorized user to get the information of the cases online. It will allow the authorized person to fetch the information on the basis of date, place, or name and hence save time to sort out different cases on the basis of previous case.', 2011, 51),
('ISSAC - Intelligent System for Streamlined Accelerometric Control', 1, 'We now live in a world surrounded by a large number of extremely smart tiny electronic devices. These devices are now capable enough to continuously communicate with each other forming a Ã¢â‚¬Å“Internet of thingsÃ¢â‚¬Â[1] and most of them can be considered motion capture devices in disguise owing to the sheer number and quality of sensors they house. The devices are capable of providing data like linear acceleration, angular velocity, gravitational acceleration, compass heading, pressure and temperature with an update rate that is just enough to be interesting and useful, yet be cheap and easy to acquire.\r\n\r\nWe are further seeing incredible innovation in motion tracking chips present in cell phone that provide better and always-on data with minimal impact on battery life. Innovations like the Motorola X8 architecture and the Apple M7 chip are now the torch-bearers for the next era in motion tracking.\r\n\r\nIt would be a waste if all this real-time data is only used by the cellphone or the smartwatch that generates it. There is a whole host of amazing applications that can be conceptualized if this data is used to interact and control our computers and other devices. The aim of this project is to make this dream a reality. The objective of this project is to investigate ways to conveniently send and process this data to create innovative controls for PC, especially for gaming.\r\n\r\nHowever, there are various challenges we have to deal with when using such sensors. These include: high level of noise in the sensor readings, time delay in sending sensor data from phone/watch to the computer, recognizing the start and end of a gesture, detecting and ignoring unintentional actions. As our work progresses on ISSAC, we aim to find practical solutions to these problems.\r\n\r\nThe successful implementation of this projects depends on efforts from different domains of Computer Science like Mobile Computing, Networks, Human Computer Interaction, Neural Networks and Discrete Signal Processing.', 2013, 52),
('Gesture Recognition And Voice Recognition Story Telling Application', 1, 'Learning begins at an early age. A child is assessed on his/her literary skills right from kindergarten.\r\n\r\nNowadays, various innovative pedagogical techniques are used for literacy development. Story telling is one such technique, which helps improving comprehension and enunciation. Our application aims at strengthening this technique by making it an interactive experience.\r\n\r\nWe use voice recognition in order to emulate various scenarios on the screen. For example if the story teller wants a background depicting night, the screen will emulate such a scenario. We have a predefined list of scenarios which the story teller can use.  \r\n\r\nAlso, there will be various characters provided to enhance the story telling experience. The story teller can select any of those characters and change their positions while narrating the story using gestures.\r\n\r\nUsing this application, the story teller can create his/her own story on the fly. Thus it will make every kid an imaginer and also will be very helpful for teachers in order to provide their students an enjoyable learning experience.', 2013, 53),
('Application Of Spatial Database (Municipal Database)', 4, 'Geographic Information Systems (GIS) are information management systems which are used to store and analyze spatial data observations and relationships. GIS is used as an analytical tool in academic and professional applications ranging from historical analysis of human settlement patterns to predictive modeling of environmental phenomena. Spatial Data refers to information related to a location anywhere on the earth''s surface, and allows users to look at an area or geographic feature in relation to other areas (in relation to changes over time and in relation to various factors).\r\n\r\nSpatial Data describes both the location of a geographic feature and its attributes (non-locational information about a feature), usually stored as coordinates and topology. A feature''s attributes may be viewed as descriptive information that is used to classify and/or describe a particular feature.\r\n\r\nSpatial Data exists in many forms including digital maps and printed maps, aerial photography and digital satellite images and can be manipulated in desktop mapping or GiS programs such as ArcView, Mapinfo, or Intergraph. Common data formats include vector, raster and tabular.\r\n', 2013, 54),
('Market Segmentation in the cellular industry', 4, 'The advancement of technology has enabled businesses to automate their business functions, increase their productivity and reduce their costs. And more recently, technology has also entered customer relationship management sector of large businesses. One of the noted impacts is in the telecommunication sector.\r\n\r\nDue to regulations put forward by the TRAI regarding mobile number portability, the ease with which a customer can now change from one service provider to another while retaining its number has increased. It has thus become essential that all cellular service providers to have some sort of mechanism in place where in this churn can be curbed, while increasing the incentive of each customer to stick to its current service provider by suggesting plans that are more economical. Keeping this problem in mind, we have decided to create a system to segment the cellular market based on relevant data. This analysis can then be used to create meaningful groups or segments of customers. Through this analysis we aim to provide the cellular companies with the tools to help them enhance their customer relationships and also create plans suited for particular segments.', 2013, 55),
('Mental Health Monitor using Reality Mining', 4, 'Reality mining is the collection and analysis of machine-sensed environmental data pertaining to human social behavior, with the goal of identifying these predictable patterns of behavior. Many signs and symptoms of psychiatric disorders explicitly or implicitly relate to an individual™s physical movement and activity patterns and communicative behavior, usually with reference to particular temporal periods or cycles. Our project aims at identifying these early symptoms of mental illness by assessment of these patterns and behaviors. The dataset for this project will be built through usage characteristics of smartphones over a specified period coupled with timely self-reports. This data will be then transformed, filtered and analyzed using appropriate data mining techniques.', 2013, 56),
('Intelligent detection of phishing website using fuzzy datamining', 3, 'Recently there has been an increase in the number of phished websites. Numerous approaches are adopted by phishers to conduct a well-planned phished attack. The victims of the phishing attacks, are mainly on-line banking consumers and payment service providers. They have to face substantial financial loss which eventually results in lack of trust in Internet-based services. In order to overcome these loses there is an urgent need to find solutions to combat phishing attacks.\r\n\r\n\r\n\r\nDetecting phishing website is an extremely complex task which requires significant expert knowledge and experience. So far, numerous solutions have been proposed and developed to address these problems. Most of these approaches are not able to make a decision dynamically on whether the site is phished or not, giving rise to a large number of false positives.\r\n\r\n\r\n\r\nIn our project we investigated and we will develop the application of an intelligent fuzzy-based classification system for e-banking phishing website detection. The main aim of the proposed system is to provide protection to users from phishers deception tricks, giving them the ability to detect the legitimacy of the websites.\r\n', 2013, 57),
('Simulation Of Optimal Multimode Path Considering Real Time Traffic Condition', 6, 'The multi-modal route is defined as a route that involves two or more different modes among which the traveller has to make a transfer .\r\n\r\nThe purpose of multi-modal route planning is to provide the traveler with optimal, feasible and personalized route between origin and destination, which may involve public and private transportation modes. While providing information about multi-mode transport it is also important to consider traffic condition. We are planning to develop a website which will suggest user multiple routes that user can follow to reach his desired destination considering real time traffic condition.To represent routes in multi-modal travel environment, we have proposed an algorithm that will calculate optimal multimode path.\r\n', 2013, 58),
('Adaptive Screen brightness for desktops and laptops', 2, 'With the rampant growth in the types and numbers of technological devices, the consumer base for the same is increasing exponentially. The applications and the power of these devices too have increased, empowering the user to do a lot more than what they could before. Thus it is expected from the user to do a lot more, especially from a user working in a software organization. Because of this, the average time spent by the user in front of the screen, has also increased. Usually, the user continues to exert his or her eyes for long durations at a stretch. This can lead to a condition called as the Computer Vision Syndrome describes a group of eye and vision-related problems that result from prolonged computer use. Many individuals experience eye discomfort and vision problems when viewing a computer screen for extended periods. The level of discomfort appears to increase with the amount of computer use.\r\n\r\nAs a direct solution to this problem, we propose a unique way of adjusting the brightness of the monitor by gauging the amount of redness in the eyes of the user. Our algorithm will be as follows :\r\n\r\n1) Take real time images from the webcam and locate the eyes of the user.\r\n\r\n2) Analyze intensity of red coloured pixels by inspecting the images.\r\n\r\n3) Adjust the brightness of the screen as per the determined value of the intensity of red.\r\n\r\nOur algorithm works under the basic assumption that, red eyes would be a direct and most clear indicator of tired eyes, and therefore a tired user must be protected by further harm. And so, the brightness level will be adjusted.\r\n\r\nAutomatic brightness control features are prevalent in such as mobile phones and MP3 players. However, our application aims to implement it for desktops. The proposed tool will adjust the screen brightness not only on the basis of the surrounding light but also the level of tiredness of the user™s eyes (gauged by checking the redness). This idea intends to minimize the strain caused to the eyes and other ailments due to excessive light emerging from the computer screen. The most charismatic feature of this application is that it needs no extra additional hardware other than a webcam found in all desktops today. It provides a novel solution to computer vision syndrome- one of the most prevalent problems in the generation today\r\n', 2013, 59),
('Credit Risk Evaluation using Data Mining', 8, 'Credit Risk Evaluation is an application which employs data mining to build a model using historical records related to credit.Using the model the credit risk associated with new applicants can be evaluated based on the parameters on which the model is built.\r\nE.g. Salary, Age etc. Thus the model will serve as a classifier for evaluating the credit risk associated with a borrower.\r\n', 2013, 60),
('Natural Language Processing on Web Feeds', 6, 'The aim of our project is to create a ˜smart™ RSS reader that will provide features like automatic summarization and multi-document summarization on the fly. The RSS reader will also provide recommendations to a user about other feeds which might be interesting to the user based on his past history.Automatic summarization uses principles from Natural Language Processing. Natural Language Processing is a sub-field of Artificial Intelligence that is concerned with the interactions between human languages and computers. Natural Language Processing builds upon text mining by applying rules of grammar, vocabulary etc.Recommender systems also come under Artificial Intelligence and utilize data mining and machine learning principles to recommend books, articles, or feeds to users.In our project, we integrate the functionalities of both these systems to create the final product.\r\n', 2013, 61),
('Offline Aptitude Android Application', 3, 'Mobile application development is one of the recent trends in computing Industry. Among several existing platforms for mobile, Android is one of the largest platforms in the world that runs in several smart phones and tablets from various manufacturers like Google, Samsung, HTC, Sony, etc.\r\n\r\nMany a times we surf around internet for some test for practice, before we appear for the actual test. Various constraints like slow network or no network or many a times web server overload cannot satisfy people™s daily demands. Even if there are numerous application and websites available, none provides all features together in one application. Just for such reasons we cannot afford to stop practicing or delay it. One solution to this is to have an application in mobile for practicing test offline.\r\n\r\nAndroid Offline Aptitude App is collection of 1500+ quantitative aptitude questions and word problems frequently asked in competitive examination and placement papers. It will be designed as a preparation tool for job aspirants  and various aptitude tests. Our project covers various sections like Mathematics , different programming languages like C, C++, Java, Verbal and Logical and puzzles.', 2013, 63),
('Android Application For Courier Delivery', 2, 'We are emphasizing on developing this project that will help the customers to choose the suitable courier service provider easily. For this, we are implementing software œMobile based Courier delivery based on Android Technology. \r\n\r\nWe are inspired to work on MCD from the existing website Coupan Duniya. MCD will help customers to take the decision of sending their courier to desire destination with their desired time (Fast, Normal) and within budget using Smart-phone. Customer will get information of all service provider providing services of courier within city and also help them to choose best provider according to the need. Customer will need to register MCD for gaining the free services. Once registered customer will login and choose the destination and mode of delivery. Based on preferences the list of service provider will be displayed along with the delivery charges. Customer can then choose the provider and know the nearby address from current location. Also a token number will be generated along with the service provider which is referred by customer to do the transaction. \r\n\r\nThis will reduce the time of queue not only in peak hours but also in festivals. The project would include Location detection using GPS. Also email would be send to the customer, notifying them of their deliveries. Thus, the project will ease the workload of the customer by providing them the platform wherein they will be able to select the mode of transfer and mode of payment.\r\n', 2013, 64),
('Virtual Supercomputer on campus', 8, 'A lot of organizations today are involved in developing projects that are compute intensive. For this purpose buying a high end computer may not be the most feasible solution to meet their computing requirements. Also, consider a computer lab. A lot of resources belonging to the computers in the lab are not utilized optimally. Our project aims at utilizing these resources in order to cater to the requirements of the projects that require compute intensive resources. We have employed the idea of constructing a cluster of commodity PCs (Lab PCs in our case) that will help execute parallel program that require high end computer resources. A typical cluster will then use the client-server model where in the server(Master node) will deal with partitioning the program into tasks and scheduling to clients (Slave nodes).\r\n', 2013, 65),
('Automatic Subtitle Generation for Videos in Devnagari Script', 10, 'Generating subtitles in Devanagari will help people with auditory problems as well as for people who have difficulty in understanding the video.   \r\n\r\nHence, this is an attempt to develop a software that will automatically generate subtitles in Devanagari script. This project mainly focuses on generating subtitles on run time and with minimum lag in time synchronization.\r\n\r\nOur report is intended to provide a very brief introduction about generating subtitles on run time with minimum efforts. It also illustrates the concept of Speech Recognition, Audio Extraction and finally Subtitle Generation.\r\n', 2013, 66),
  ('Social Educational Forum', 5,
   'An internet forum/ discussion board is an online site for sharing information and ideas through the internet. \r\n\r\nIt allows numerous people around the globe to speak up about their knowledge, experience, expertise, information, suggestions, etc. with each other. There are various computer forums in the internet currently available. \r\n\r\nOnline forum is an online communication medium between multiple users. Usually the communication is done through text-style communication, but there are other techniques like video or voice conferencing or chatting. A forum discussion board not only helps users for internet communication and association about almost every issue of life, but also provides opportunity to trade information and product/service for personal or business purpose. \r\n\r\nThis is an excellent way of collecting information by every user for each and every useful topic.\r\n\r\nThe topic chosen by us is educational forum as its need is increasing day by day. Our project includes following features in an education forum.\r\n\r\n¢	User will create his own account containing a unique username and password, which he only can   access.\r\n¢	Functions like add as friend.\r\n¢	Live chatting module.\r\n¢	Latest news related to exams.\r\n¢	Information regarding different exams.\r\n¢	Answer to queries asked by users.\r\n¢	Exam related material.\r\n¢	College search up.\r\n¢	Posting status on wall and commenting on them.\r\n¢	Job related information.\r\n¢	Companies™ information.',
   2013, 67),
  ('', 1, '', 2013, 68),
  ('ryhrthtr', 1, 'etgret5e ergeg', 2013, 69),
  ('ertyju', 1, 'werty', 2013, 70),
  ('iuuu', 1, 'qa', 2009, 71);

-- --------------------------------------------------------

--
-- Table structure for table `research_grant_project`
--

CREATE TABLE IF NOT EXISTS `research_grant_project` (
  `description` text NOT NULL,
  `faculty_id` int(11) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research_grant_project`
--

INSERT INTO `research_grant_project` (`description`, `faculty_id`) VALUES
('Research Grant from National Science & Technology Entrepreneurship Development Board (NSTEDB), Department of Science &†Technology (DST), Govt. of India, New Delhi for the project titled ZIUS.', 1),
('Worked as Principal Investigator @ University of Mumbai Minor-Research Grant for the year 2010-2011 for the project titled STACsys: SMS based Taxi and Auto Rickshaw Complaint System.\r\n\r\nStatus:Project completed and report is submitted to Mumbai University.', 1),
('Worked as a co-investigator @ University of Mumbai Minor-Research Grant for the year 2011-2012 for the project titled BMX : Bluetooth Mouse controller for LCD projector and desktop PC:\r\n\r\nStatus:Project completed and report is submitted to Mumbai University.', 1),
('Worked as a Principal Investigator for  University of Mumbai Minor-Research Grant for the year 2013-2014 for the project titled "Sign Language Translator for an Android phone".\r\n\r\n\r\n\r\nStatus:Project completed and report is submitted to Mumbai University. ', 1),
('Mumbai University Minor Research Grant during financial year 2014-15', 3),
('Research Grant from IEDC during financial year 2014-15', 3);

-- --------------------------------------------------------

--
-- Table structure for table `research_publication`
--

CREATE TABLE IF NOT EXISTS `research_publication` (
  `title` varchar(10000) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `type` varchar(1) NOT NULL,
  KEY `faculty_id` (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research_publication`
--

INSERT INTO `research_publication` (`title`, `faculty_id`, `type`) VALUES
('TPM "Total productive Maintenance": A case study in Manufacturing  Industry"  Authors:- D.R.Kalbande, Dr.G.T.Thampi, S.Sawalekar   Published in IIIE (National Journal of Indian Institute of Industrial Engineering) Journal   Oct .2010.', 1, 'N'),
('"Multi-attribute and Multi-criteria Decision Making Model for technology  selection using fuzzy logic", published in Technia - International Journal of Computing   Science and Communication Technologies, IJCSCT Vol. 2, Issue 1, July 2009.', 1, 'I'),
('"Incidence Handling & Response System", published in International  Journal of Computer Science & Information Security IJCSIS, Vol. 2, June 2009, ISSN   1947-5500.( IMPACT FACTOR: 0.423)', 1, 'I'),
('"Algorithm to access Office Files on Mobile Phones, published in  Proceedings of International Journal of Computer Application (IJCA), ISSN: 0975 8887,   Number 3, Article -4 March 2011 and also Published by Foundation of Computer   Science. BibTeX', 1, 'I'),
('An Advanced Technology Selection Model using Neuro Fuzzy Algorithm  for Electronic Toll Collection System published in (IJACSA) International Journal of   Advanced Computer Science and Applications, Vol. 2, No. 4, 2011 ,pp.97-104.', 1, 'I'),
('ANFIS Based SPAM Filtering Model for Social Networking Websites,  published in International Journal of Computer Applications 44(11):32-36, April 2012.   Published by Foundation of Computer Science, New York, USA.,DOI : 10.5120/6310-8635', 1, 'I'),
('Smart Card based Android Application for Public Transport Ticketing  System published in International Journal of Computer Applications 60(11):29-32,   December 2012. Published by Foundation of Computer Science, New York, USA,DOI:   10.5120/9738-4289', 1, 'I'),
('"Intrusion Detection System in Distributed Computing Environment", International Journal of Cloud Computing, Accepted in June-2011', 8, 'I'),
('An efficient Recommender System Using Collaborative Filtering Method with K-Means Approach published in International Journal of Engineering Research and Applications (IJERA) ISSN: 2248-9622,Page no. 30-35.', 1, 'I'),
('Mobile Cloud based Compiler : A Novel Framework For Academia published in International Journal of Advancements in Research & Technology,Volume 2,Issue4,April 2013 Page 445 ISSN 2278 7763', 1, 'I'),
('"Wireless File Transfer Using Webserver on an Android Device", International Journal of Innovative and Emerging Research in Engineering, Volume 2, Issue 2, 2015, available online at www.ijiere.com', 3, 'I'),
('"Students Guide: Offline Android Aptitude Application", International Journal of Computer Applications(0975-8887) Volume 94-No 11, May 2014.', 3, 'I'),
('"Intelligent Detection of Phishing E-banking Website Using Fuzzy Datamining", International Journal of Innovative Research in Computer Science & Technology(IJIRCST) ISSN:2347-5552, Volume-2, Issue-3, May-2014.', 3, 'I'),
('"Malware Detection Module using Machine Learning Algorithms to Assist in Centralizedty in Enterprise Networks", International Journal of Network Security & Its Applications (IJNSA), Vol.4, No.1, January 2012. ', 3, 'I'),
('"A Hybrid System for Multimedia Conferencing on Android Technology", International Journal of Computer Applications (0975 ? 8887) Volume 44 No11, April 2012.', 3, 'I'),
('Mr. Sunil Ghane, Mrs. Sujata Kolhe, œFacilitating Document Annotation for Efficient User  Relevant Search&quot;, been published in International Journal MECIT-2015, at Jawaharlal Nehru  University, New Delhi.', 13, 'I');

-- --------------------------------------------------------

--
-- Table structure for table `seminar_workshop`
--

CREATE TABLE IF NOT EXISTS `seminar_workshop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text,
  `date` date NOT NULL,
  `content` text,
  `committee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `seminar_workshop`
--

INSERT INTO `seminar_workshop` (`id`, `image`, `date`, `content`, `committee_id`) VALUES
(10, NULL, '0000-00-00', 'effwef+1', 0),
(11, NULL, '2012-12-01', '12+1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `startup`
--

CREATE TABLE IF NOT EXISTS `startup` (
  `name` varchar(10000) NOT NULL,
  `logo` varchar(10000) NOT NULL,
  `students_involved` varchar(10000) NOT NULL,
  `url` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `startup`
--

INSERT INTO `startup` (`name`, `logo`, `students_involved`, `url`) VALUES
('one', 'img/up16.php', 'three', 'two'),
('fghjkolp;', '', '3fnjrjfinernggirenfrn  ', 'fghjkol'),
('fgjkl', '', 'fgi', 'ftyuio'),
  ('aaghj', '', 'ghjk', 'fgh'),
  ('7op[', '', 'tyuiop[', 'tyuiop[');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text NOT NULL,
  `project_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`student_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 198;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `first_name`, `project_id`) VALUES
  (1, 'Priyanka Kulkarni', 1),
  (2, 'Uma Nagarsekar', 1),
  (3, 'Aditi Mhapserkar', 1),
  (4, 'Kinjal Nandu', 2),
  (5, 'Urvi Parikh', 2),
  (6, 'Bhavana Patil', 2),
  (7, 'Shreya Agrawal', 3),
  (8, 'Gaurav Hedaoo', 3),
  (9, 'Bhushan Patil', 3),
  (10, 'Subhashree Chowdhury', 4),
  (11, 'Alifiya Bhanpurwala', 4),
  (12, 'Aishwarya Venkatesh', 4),
  (13, 'Manali Raut', 5),
  (14, 'Shwetlana Singh', 5),
  (15, 'Vijaya Nale', 5),
  (16, 'Ashwin Agrawal', 6),
  (17, 'Amay Ghadigaonkar', 6),
  (18, 'Rakesh Baingolkar', 6),
  (19, 'Aditya Agarwal', 7),
  (20, 'Sanket Kothari', 7),
  (21, 'Ruchi Shah', 7),
  (22, 'Apurva Nigam', 8),
  (23, 'Priti Kothawade', 8),
  (24, 'Chaitali Shah', 8),
  (25, 'Ashwini Patil', 9),
  (26, 'Sheetal Gaikwad', 9),
  (27, 'Ankit Shah', 9),
  (28, 'Rahul Bhandari', 10),
  (29, 'Rupeshkumar Kadam', 10),
  (30, 'Omkar Nagvekar', 10),
  (31, 'Ritesh Mittal', 11),
  (32, 'Damini Kapse', 11),
  (33, 'Manali Jiwtode', 11),
  (34, 'Bhavik Shah', 12),
  (35, 'Viral Patel', 12),
  (36, 'Pratik Naik', 12),
  (37, 'Mahesh Karambalkar', 13),
  (38, 'Swapnil Katale', 13),
  (39, 'Abhishek Lingwal', 13),
  (40, 'Mihir Jayavant', 14),
  (41, 'Soham Aurangabadkar', 14),
  (42, 'Viren Italia', 14),
  (43, 'Manas Churi', 15),
  (44, 'Shruti Desai', 15),
  (45, 'Neelam Gulrajani', 15),
  (46, 'Jinav Shah', 16),
  (47, 'Vinayak Namye', 16),
  (48, 'Parth Shah', 16),
  (49, 'Snehal Sakpal', 17),
  (50, 'Manisha Prakash', 17),
  (51, 'Vinit Shah', 17),
  (52, 'Nupur Bansal', 18),
  (53, 'Daksha Asrani', 18),
  (54, 'Aditya Shetty', 18),
  (55, 'Shaikh Afrin', 19),
  (56, 'Shaikh Arshad', 19),
  (57, 'Shailesh Pujari', 19),
  (58, 'Pooja Pai', 20),
  (59, 'Saurabh Netravalkar', 20),
  (60, ' Ronak Parpani', 20),
  (61, 'Meera Kamath', 21),
  (62, 'Arpita Parab', 21),
  (63, 'Aarti Salyankar', 21),
  (64, 'Bhagyashree Kakirde', 22),
  (65, ' Prachee Gidh', 22),
  (66, ' Heenal Bhavsar', 22),
  (67, 'Geetika Bangera', 23),
  (68, 'Arbans DÃ­cruz', 23),
  (69, 'Alpesh Jain', 23),
  (70, 'Rohan Singh Negi', 24),
  (71, 'Sushant Kaul', 24),
  (72, 'Nishit savla', 24),
  (73, 'Mahesh Singh', 25),
  (74, 'Pralhad Surve', 25),
  (75, 'Omkar Yadav', 25),
  (76, 'Sheetal Pandrekar', 26),
  (77, ' Aaswad Satpute', 26),
  (78, 'Ruchita Bhatane', 26),
  (79, 'Chandan Patil', 27),
  (80, 'Tejashree Nipane', 27),
  (81, 'Shweta Kapgate', 27),
  (82, 'Tejas Khairnar', 28),
  (83, 'Rohil Pathak', 28),
  (84, 'Harsh Bhartiya', 28),
  (85, 'Mihir Shah', 29),
  (86, 'Pankaj Pinjarkar', 29),
  (87, 'Rugved Thakur', 29),
  (88, 'Preeti Ramaraj', 30),
  (89, 'Nisha Swaminathan', 30),
  (90, 'Harsh Panchal', 30),
  (91, 'Bhavin Patel', 31),
  (92, ' Dakshay Nerkar', 31),
  (93, ' Ronak Shah', 31),
  (94, 'Yash Rasania', 32),
  (95, 'Vipul Mishra', 32),
  (96, 'Santosh Kolekar', 33),
  (97, ' Nitesh Gupta', 33),
  (98, ' Pratik Shah', 33),
  (99, 'Ninad Limaye', 34),
  (100, 'Parth Satra', 34),
  (101, 'Gaffney Mendonsa', 35),
  (102, 'Rucha Shintre', 35),
  (103, 'Harshal Khandare', 36),
  (104, ' Karan Pradhan', 36),
  (105, ' Aniket Kamat', 36),
  (106, 'Amay Sankhe', 37),
  (107, 'Gandhali Karnik', 37),
  (108, 'Rohan Pawar', 38),
  (109, ' Nikhil Urkude', 38),
  (110, ' Chinmay Sonawane', 38),
  (111, 'Aashish Mittal', 39),
  (112, 'Srinath Warrior', 39),
  (113, 'Aadish Kotwal', 40),
  (114, 'Gautam Nichalani', 40),
  (115, 'Bhinav Sura', 41),
  (116, 'C.Ramasubramanian', 41),
  (117, 'Anshika Maheshwari', 41),
  (118, 'Nimesh Jain', 42),
  (119, ' Punit Shetty', 42),
  (120, ' Umang Kandoi', 42),
  (121, 'Priyank Singhal', 43),
  (122, 'Sumiran Shah', 43),
  (123, 'Femin Gala', 44),
  (124, ' Raj Mehta', 44),
  (125, ' Gaurav Kaushik', 44),
  (126, 'Adarsh Jain', 45),
  (127, 'Swati Iyer', 45),
  (128, 'Andrea Dsouza', 45),
  (129, 'Aliraza Punjani', 46),
  (130, ' Vijendra Singh', 46),
  (131, ' Ankit Vasa', 46),
  (132, 'Janhavi Mahajan', 47),
  (133, ' Sneha Shah', 47),
  (134, ' Snehal Satpute', 47),
  (135, 'Rutuja Gadre', 48),
  (136, 'Priyadarshini Jadhav', 48),
  (137, 'Kainesh Patel', 49),
  (138, 'Ujwal Patil', 49),
  (139, 'Prashant Vishe', 49),
  (140, 'Tejas Kawitke', 50),
  (141, 'Sagar Patel', 50),
  (142, 'Daniel yierang aku', 50),
  (143, 'Tulika Chamdia', 51),
  (144, 'Prajakta Kore', 51),
  (145, 'Sumit Gouthaman', 52),
  (146, 'Omkar Karande', 52),
  (147, 'Atman Pandya', 52),
  (148, 'Kanchan Chandnani', 53),
  (149, 'Deepti Chavan', 53),
  (150, 'Pratiti Desai', 53),
  (151, 'Manda Ghuge', 54),
  (152, 'Kriti Gupta', 54),
  (153, 'Karishma Kharat', 54),
  (154, 'Sonja Colaco', 55),
  (155, 'Harshita Maheshwari', 55),
  (156, 'Aditya Narula', 55),
  (157, 'Jigesh Ashok Mehta', 56),
  (158, 'Madhura Waman Mestry', 56),
  (159, 'Ajit Bramhadeo Mishra', 56),
  (160, 'Khushbu Nehita', 57),
  (161, 'Pooja Kolhe', 57),
  (162, 'Chinmayee Vaidya', 57),
  (163, 'Nikhil Nar', 58),
  (164, 'Prachi Patole', 58),
  (165, 'Priyanka Pendurkar', 58),
  (166, 'Kavya Agarwal', 59),
  (167, 'Ajay Kotian', 59),
  (168, 'Siddharth Raikar', 59),
  (169, 'Rishabh Jain', 60),
  (170, 'Ankit Bharadiya', 60),
  (171, 'Kasim Waleed Mehvy', 60),
  (172, 'Ashwini Nene', 61),
  (173, 'Darshan Savalia', 61),
  (174, 'Aneeka Ansari', 61),
  (175, 'Ashish Gimekar', 62),
  (176, 'Ashish Jain', 62),
  (177, 'Abhiraj Kakani', 62),
  (178, 'Jinal Shah', 63),
  (179, 'Ruchika Shah', 63),
  (180, 'Priti Sane', 63),
  (181, 'Pradeep Chaudhary', 64),
  (182, 'Pravin Prajapati ', 64),
  (183, 'Ravi Prajapati ', 64),
  (184, 'Sahil Monga', 65),
  (185, 'Shivaji Vidhale', 65),
  (186, 'Siddharth Wagle', 65),
  (187, 'Hitesh Baid', 66),
  (188, 'Ligin Thiya', 66),
  (189, 'Ashish Upadhyay', 66),
  (190, 'Sneha Thasal', 67),
  (191, 'Shiv Nandrajog', 67),
  (192, 'srfgh', 68),
  (193, 'dfgb', 68),
  (194, 'dfgregreg', 69),
  (195, 'rthtrhrthrth', 69),
  (196, 'ertyu', 70),
  (197, 'q', 71);

-- --------------------------------------------------------

--
-- Table structure for table `student_achievements`
--

CREATE TABLE IF NOT EXISTS `student_achievements` (
  `id`      INT(11)    NOT NULL AUTO_INCREMENT,
  `title`   TEXT       NOT NULL,
  `content` TEXT       NOT NULL,
  `year`    INT(11)    NOT NULL,
  `batch`   VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 5;

--
-- Dumping data for table `student_achievements`
--

INSERT INTO `student_achievements` (`id`, `title`, `content`, `year`, `batch`) VALUES
  (1, 'Automated emergency service for car accidents',
   'IBM TGMC Project CompetitionProject-Mobile Banking  went till  the last face 2 face evaluation round     Sept 2011 – June 2012',
   2013, ''),
  (2, 'Performance monitoring and improvement for cloud based virtual computing lab',
   'Received funding of Rs. 1 lakh from the Innovation and Entrepreneurship Development Cell of Government of India (2010-2011).',
   2012, ''),
  (3, 'wefijw ', 'wefwegwg', 2016, ''),
  (4, 'fghujiop', 'uwef ewjfewfj fgj gjnug w4ng wu4ingf', 2014, '');

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE IF NOT EXISTS `team_members` (
  `name`                    TEXT    NOT NULL,
  `students_achievement_id` INT(11) NOT NULL,
  `student_id`              INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`student_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 8;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`name`, `students_achievement_id`, `student_id`) VALUES
  ('asnfijadbf', 2, 1),
  ('rohit', 2, 2),
  ('Tejas Shah', 1, 3),
  ('Jimish Shah', 1, 4),
  ('mfef', 3, 5),
  ('ghuiop', 4, 6),
  ('fgyuiop', 4, 7);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `area_of_interest`
--
ALTER TABLE `area_of_interest`
  ADD CONSTRAINT `area_of_interest_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `conference`
--
ALTER TABLE `conference`
  ADD CONSTRAINT `conference_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `others`
--
ALTER TABLE `others`
  ADD CONSTRAINT `others_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `professional_affiliation`
--
ALTER TABLE `professional_affiliation`
  ADD CONSTRAINT `professional_affiliation_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `research_grant_project`
--
ALTER TABLE `research_grant_project`
  ADD CONSTRAINT `research_grant_project_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `research_publication`
--
ALTER TABLE `research_publication`
  ADD CONSTRAINT `research_publication_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
