<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <link href="css/bootstrap.min.css" rel="stylesheet">
	  <!-- Include scripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
	<script type="text/javascript" src="responsiveHeader/headerjs/responsivemultimenu.js"></script>

	<!-- Include styles -->
	<link rel="stylesheet" href="responsiveHeader/headercss/responsivemultimenu.css" type="text/css"/>

	<!-- Include media queries -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Vision&Mission </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/imageslider.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    <style>
        body {
            overflow-x: hidden;

        }

       
h1{
            color: black;
            font-family: Georgia, Times, "Times New Roman", serif;
            font-size:250%;

        }
       
        .thumbnails li {
            margin: 2px;
        }

        .thumbnails a {
            text-decoration: none !important;
        }

        .thumbnail li {
            list-style-type: square;
        align-content: center;
        }

         .hr {
            display: block;
            margin: 0.5em auto;
            border: 2px inset #FF4500;
        }
    </style>
    <script>
        $(document).ready(function () {
            //Examples of how to assign the Colorbox event to elements
            $(".group1").colorbox({rel: 'group1', height: '80%'});


        });
    </script>
</head>
<body>
       <?php include 'responsiveHeader/header.html'; ?><br><br>

<h1 style="text-align:center;">PHD</h1>
<hr width="30%" class="hr">
<div class="container">
    <div class="row">
        <ul class="thumbnails" style="list-style: none">

        <li class="span5">
         <div class="thumbnail">
<ul>
    <li>
        <a href="pdf/Comp_PhD_students.pdf" target="_blank">List of Ph.D. Students in Computer Engineering</a></li><br><br>
        <li><a href="pdf/A -  PH_D_Course_work_Rules and regulations - All Engg Disciplines -App C and D.pdf"
               target="_blank">PHD Course work Rules and regulations.pdf</a></li><br><br>
        <li><a href="pdf/PHD Course work Time Table.pdf" target="_blank">PHD Course work Time Table</a></li>
</ul>
            </div>
            </ul>    
    </div>
</div>
<br>
<br>

<?php include 'includes/footer.php'; ?>

</body>
</html>