<?php

require_once './includes/session.php';
require_once './includes/database.php';


$sql = "select * FROM iic_company";
$result = $database -> query($sql);
$row = $database->fetch_array($result);
?>

<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title> IIC </title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/imageslider.css">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<style>
		body {
			overflow-x: hidden;

		}
		.space-top-lg{padding-top: 50px;}
		.space-bottom-lg{padding-bottom: 50px;}
		.space-top-sm {padding-top: 20px;}
		.space-bottom-sm {padding-bottom: 20px;}

        .space-top-md {padding-top: 30px}
		.imgAbout {
			width: 100%;
			height: auto;
			border:2px solid #fff;

		}
		.about {
			text-align: center;
			border: 2px solid;
			border-radius: 10px;
		}
		#imgDescription {
			font-style: italic;
			font-size: 1.2em;
		}
		.internships {
			text-align: center;
			border: 2px solid;
			border-radius: 10px;

		}
		.companies {

			text-align: center;
			border: 2px solid;
			border-radius: 10px;
		}

		.companiesImages {
			width: 80px;
			height: 80px;
			border: 1px solid;
			border-radius: 40px;

		}

        .company-dark {
			padding-top: 10px;
			padding-bottom: 10px;
			background-color: #e5e5e5;
		}
		.company-light {
			padding-top: 10px;
			padding-bottom: 10px;
			background-color: #fff;
		}
		.company-head {
			color: #000;
		}
		a:hover {
			color: #000;
		}



	</style>
</head>
<body>
<?php include 'includes/header.php'; ?>
<div class="container col-lg-6 space-top-lg" style="background-color: #fff;">
	<div class="about space-top-sm space-bottom-sm">
		<h2 class="aboutHead">About IIC</h2>
		<div class="space-top-sm">
			<img class="imgAbout" src="">
			<p id="imgDescription">Student Co-ordinators 2013-14</p>
		</div>
		<h3 class="space-top-sm">IIC - Industry Institute Interaction Cell</h3>
		<p class="aboutContents" style="font-size: 1.2em; font-weight: bold;">Address: IIC, Room No. 604, 6th Floor, Department of Computer Engineering</p>
		<h4 class="space-top-sm">IIC Chair - IIC</h4>
		<p class="aboutContents"> Dr. D. R. Kalbande</p>
		<h4 class="space-top-sm">Faculty Coordinator</h4>
		<p class="aboutContents"> Prof. Radha Shankarmani (Head of IT department)</p>
		<h4 class="space-top-sm">Student Coordinators</h4>
		<p class="aboutContents">Mr. Rushabh Shah ,TE Computer, Sem V</p>
		<p class="aboutContents">Mr. Jay Bhatt ,SE Computer, Sem III</p>
		<p class="aboutContents">Ms. Rheya Vithlani ,SE Computer, Sem III</p>
		<p class="aboutContents">Ms. Shivani Inamdar ,SE Computer, Sem III</p>
		<p class="aboutContents">Ms. Trisha Surve ,SE Computer, Sem III</p>
		<div class="obj">
			<h4 class="space-top-md">Objectives</h4>
			<ul>
				<li>
					<p>To enhance the employability of the Computer Engineering branch students</p>
				</li>
				<li>
					<p>To guide and motivate the students for undertaking Industry oriented projects</p>
				</li>
				<li>
					<p>To provide internship to students of Computer Engineering</p>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="container col-lg-6  space-top-lg" style="background-color: #fff;">
	<div class="internships space-top-sm space-bottom-sm">
        <?php $sql = "select * FROM iic_company";
        $int_result = $database -> query($sql);
        if ($int_result->num_rows > 0) {?>
            <h2 class="internshipsHead">Industry Institute Interaction Cell - Internships</h2>
        <?php
        // output data of each row
        while($row = $int_result->fetch_assoc()) {?>
		<div class="space-top-md">
			<ul style="alignment: left">
				<li>
					<p><?php echo "{$row['description']}" ?></p>
				</li>

			</ul>
		</div>
        <?php
        }
        }
        ?>
	</div>
</div>
<div class="container col-lg-6 space-top-lg space-bottom-lg">
	<div class="companies space-top-sm space-bottom-sm" style="background-color: #fff;">
		<h2 class="companiesHead">Industry Institute Interaction Cell - Company Tie-ups</h2>
		<div class="space-top-sm">
            <?php
            $classname = "company-dark";
            global $decider;
            $decider = true;
            function change($decider){
                if($decider==true){
             $classname = "company-dark";
             }
             else{
             $classname ="company-light";
            }
            return $classname;
            }
            ?>
            <?php
            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
            ?>
            <div class="<?php echo "$classname" ?>">

                <img src="<?php echo "{$row['logo']}" ?>"
                class="companiesImages">
                <a href="#">
                    <h3 class="company-head"><?php echo "{$row['name']}" ?></h3>
                </a>
                <p><?php echo "{$row['description']}" ?></p>
            </div>
            <?php
            $decider=!$decider;
            $classname=change($decider);
            }}
            ?>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>