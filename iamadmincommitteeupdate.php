<?php
require_once 'includes/database.php';
require_once 'includes/session.php';
require_once 'includes/uploading_image.php';

if (isset($_SESSION['aid'])) {

    if(isset($_SESSION['ssz'])&&isset($_POST['committee'])){
        //echo 'hua';
        $details="";
        foreach ($_POST['students'] as $key => $value) {
            $details.=$value."<br/>";
        }
        $details .= "<br/><br/>Description:<br/>";
        $sql="UPDATE committee SET details='{$database->escape_value($details)}{$database->escape_value($_POST['description'])}',cordi_id={$_POST['cordi_id']},logo='{$database->escape_value($_SESSION['ssz'])}' WHERE id={$_POST['committee']}";
        echo $sql;
        //echo $sql;
        $database->query($sql);
    }else if(isset($_POST['committee'])){
        //echo 'zale';
        $details="";
        foreach ($_POST['students'] as $key => $value) {
            $details.=$value."<br/>";
        }
        $sql="UPDATE committee SET details='{$database->escape_value($details)}{$database->escape_value($_POST['description'])}',cordi_id={$_POST['cordi_id']}WHERE id={$_POST['committee']}";
               echo $sql;

        $database->query($sql);
    }
    //print_r($_SESSION);
    //echo '<br/> files';
    //print_r($_FILES);
    $sql="select * from faculty";
    $faculty_result=$database->query($sql);
    $sql="select * from committee";
    $committee_resultset=$database->query($sql);
    //print_r($_POST);
    $database->close_connection();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Department of Computer Engineering | Faculty</title>

    <script src="js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap_admin.css" rel="stylesheet">
    <link href="css/admin_template.css" rel="stylesheet">
    <link href="css/admin_faculty.css" rel="stylesheet">
    <link href="css/faculty_need.css" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php include('includes/control_header.php'); ?>

<div class="container">


    <div class="row row-offcanvas row-offcanvas-right">

        <?php include('includes/admin_sidebar.php'); ?>

        <div class="col-xs-12 col-sm-9">
            <div class="jumbotron" id="addFaculty">    <!-- for adding a new faculty -->

                <div class="error"><?php if (isset($message)) {
                        echo $message;
                        unset($message);
                    } ?></div>    <!-- print error here -->    <!-- print error here -->

                <h3>Add Committee Information Update</h3>
                <ul class="nav">
                    <form method="POST" action="" enctype="multipart/form-data">

                        <select class="form-control" name="committee">
                            <!-- Manually specify committee name if needed in options -->
                            <?php
                            while ($committee = $database->fetch_array($committee_resultset)) {
                                ?>
                                <option
                                    value="<?php echo "{$committee['id']}"; ?>"><?php echo $committee['name']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <select class="form-control" name="cordi_id">
                            <?php
                            while ($faculty = $database->fetch_array($faculty_result)) {
                                ?>
                                <option value="<?php echo $faculty['id']; ?>"><?php echo $faculty['fname']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <textarea class="form-control" placeholder="Description" name="description"></textarea>
                        <input type="file" class="form-control" style="height:auto" name="image">
                        <!-- logo, if not needed remove -->
                        <div id="cstudents">
                            <input type="text" class="form-control" placeholder="Student Name" name="students[]">
                        </div>
                        <div class="btn btn-sm btn-primary btn-block addMore" onclick="committee_addmore()">Add More
                        </div>
                        <input type="submit" class="btn btn-sm btn-primary btn-block faculty-account" value="Post">

                    </form>
                </ul>
            </div>    <!--/jumbotron for adding a new faculty -->

        </div><!--/span-->

    </div><!--/row-->

    <hr>

    <footer style="font-size:12px; text-align:center;">
        <p>Copyright &copy; 2017 Bharatiya Vidya Bhavan's Sardar Patel Institute of Technology. All Rights Reserved.</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/addmore.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/admin_faculty.js"></script>
</body>
</html>
<?php
unset($_SESSION['error_message']);
?>
